<?php
/**
 * The template for displaying Author archive pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php global $authordata; $authordata = get_user_by('ID', $wp_query->query_vars['author']) ?>
<?php $author_url = get_author_posts_url($authordata->ID, $authordata->user_nicename) ?>
<?php ub_count_user($authordata->ID) ?>

<section id="user-section" class="content-area">
	<div id="content" class="site-content" role="main">

		<div class='user-header' style='background-image: url(<?= ub_user_background($authordata) ?>)'>
			<div class="profile-avatar"><?= get_avatar($authordata->ID, 150, '', get_the_author_meta('display_name'), ['extra_attr' => sprintf('title="%s"', get_the_author_meta('display_name'))]) ?></div>

			<header class="archive-header">
				<h1 class="archive-title"><?= $authordata->display_name ?></h1>
				<div class="authordata-description"><?= $authordata->description ?></div>
				<div class="center author-buttons">
					<?= do_shortcode('[subscribe-author-button]') ?>

					<?php if (is_user_logged_in() && get_current_user_id() != $authordata->ID): ?>
						<a href="<?= msg_thread_link($authordata->ID) ?>" title="Message" class="btn btn-default"><i class="fa fa-envelope"></i></a>
					<?php endif ?>

<?php if ($authordata->user_url): ?>
	<a href="<?= $authordata->user_url ?>" class="btn btn-default ban-profile-url" target="_blank" title="Home page"><i class="fa fa-link"></i></a>
<?php endif ?>

<button type="button" class="btn btn-default btn-cal" data-toggle="modal" data-target="#myCalModal" title="<?= $authordata->display_name ?>'s activity calendar">
 <i class="fa fa-calendar-plus-o"></i>
</button>

<a href="<?= get_author_feed_link($authordata->ID) ?>" class="btn btn-default btn-rss" title="<?= $authordata->display_name ?>'s RSS feed"><i class="fa fa-rss"></i></a>



					<button type="button" class="btn btn-default btn-report" id="btnReport"  title="Report <?= $authordata->display_name ?>"><i class="fa fa-flag"></i></button>

					<span title="Views"><?= get_all_posts_views($authordata->ID) ?> <i class="fa fa-eye"></i></span>
				</div>

				<div class="user-stats row center">
					<div class='col-md-4 col-sm-4 col-xs-4'> 
						<div class='updates'>
							<a href="<?= $author_url ?>"><span class='lg'><?= ub_count_post($authordata->ID) ?></span> Updates</a>
						</div>
					</div>
					<div class='col-md-4 col-sm-4 col-xs-4'> 
						<div class='following'>
							<a href='<?= $author_url ?>/following'><span class='lg'><?= ub_following_cnt($authordata->ID) ?></span> Following</a>
						</div>
					</div>

					<div class='col-md-4 col-sm-4 col-xs-4'> 
						<div class='follows'>
							<a href='<?= $author_url ?>/followers'>
								<span class='lg'><?= ub_followers_cnt($authordata->ID) ?></span> Followers
							</a>
						</div>
					</div>
				</div>


<!-- Modal -->
<div class="modal fade" id="myCalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Download a Calendar</h4>
      </div>
      <div class="modal-body modal-cal">
<p class="lead">Download the calendar for your device:</p>
     <a href="<?= admin_url('admin-ajax.php') ?>?action=ub_cal&amp;type=ics&amp;user_id=<?= $authordata->ID ?>" class="btn btn-default"><i class="fa fa-calander"></i> Google </a>
     <a href="<?= admin_url('admin-ajax.php') ?>?action=ub_cal&amp;type=ics&amp;user_id=<?= $authordata->ID ?>" class="btn btn-default"><i class="fa fa-calander"></i> Apple</a>
     <a href="<?= admin_url('admin-ajax.php') ?>?action=ub_cal&amp;type=csv&amp;user_id=<?= $authordata->ID ?>" class="btn btn-default"><i class="fa fa-calander"></i> CSV</a>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



			</header>
		</div>
	</div>

	<div class="container">
		<?php // show followers/following ?>
		<?php if (array_key_exists('following', $wp_query->query_vars)): ?>
				<h1 class="center"><?= $authordata->display_name ?> is following</h1>
				<div class="infinite-scroll"><?= ub_user_following() ?></div>
		<?php elseif (array_key_exists('followers', $wp_query->query_vars)): ?>
				<h1 class="center"><?= $authordata->display_name ?>'s Followers</h1>
				<div class="infinite-scroll"><?= ub_user_followers() ?></div>

		<?php else: // show author's posts ?>

			<?php if (have_posts()): ?>
				<?php if (function_exists('order_btn') && count_user_posts($authordata->ID)) echo order_btn() ?>
				<div class="infinite-scroll">
					<div class="row">
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="col-md-4">
							<?php get_template_part( 'content', get_post_format() ) ?>
							</div>

						<?php endwhile ?>
						<span class="pager next"><?php next_posts_link('Next') ?></span>
					</div>
				</div>
			<?php else: ?>
				<div class="alert alert-info" role="alert">No activities listed yet. Why not add one?</div>
			<?php endif ?>

		<?php endif ?>
	</div>
</section>

<script>
	jQuery(function($){
		$('#btnReport').on('click', function(e) {
			ub_report('user', <?= $authordata->ID ?>, $(this));
		});
	});
</script>

<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();

