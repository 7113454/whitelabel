<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-top">
		<div class="post-author">
			<div class="post-avatar"><?php echo get_avatar( $post->post_author, 46 ); ?></div>
			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a> 
		</div>
		<div class="post-time">
			<?php printf( _x( '%s ago', '%s = human-readable time difference', 'your-text-domain' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
		</div>
		<div class="clear">	</div>
	</div>

	<a href="<?= get_permalink() ?>" title="<?php the_title_attribute() ?>" class="fav-thumb"><?php the_post_thumbnail('medium') ?></a>

	<header class="entry-header">
		<a href="<?php the_permalink() ?>" class="title" title="<?php the_title_attribute() ?>"><?php the_title() ?></a>

		<!--favorite-->
		<?php // function_exists('wpfp_link') && wpfp_link(); ?>
	</header>

	<footer class="entry-meta">
		<span class="tag-links"><?php the_tags('', '', '') ?>&nbsp;</span>
		<?= ub_favorite() ?>
	</footer>
</article><!-- #post-## -->

