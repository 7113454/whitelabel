<?php 
/*
YARPP Template: Simple
Author: mitcho (Michael Yoshitaka Erlewine)
Description: A simple example YARPP template.
*/
?>
<?php if (have_posts()):?>
<div class="row">
	<?php while (have_posts()) : the_post(); ?>
		<div class="col-md-4"><?php get_template_part('content', get_post_format()) ?></div>
	<?php endwhile; ?>
</div>
<?php else: ?>
<p>No related posts.</p>
<?php endif; ?>
