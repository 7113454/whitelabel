<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

	</div><!-- #main -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="foot-menu">
			<a href="/about" class="btn btn-default">About</a>
			<a href="/contact" class="btn btn-default">Contact</a>
		</div>
		<div>

		<?php if (get_option('APP_BANNERS_apple_id') || get_option('APP_BANNERS_android_id')): ?>
			<div class="foot-app-store">
				<?php if (get_option('APP_BANNERS_apple_id')): ?>
					<a href="https://itunes.apple.com/app/id<?= get_option('APP_BANNERS_apple_id') ?>" class="store-foot"><img src="<?= get_theme_file_uri('images/apple-app-store.png') ?>" alt="Download on the App Store button" title="Download on the App Store"></a>
				<?php endif ?>
				<?php if (get_option('APP_BANNERS_android_id')): ?>
				<a href="https://play.google.com/store/apps/details?id=<?= get_option('APP_BANNERS_android_id') ?>" class="store-foot"><img src="<?= get_theme_file_uri('images/google-play.png') ?>" alt="Download on Google Play button" title="Download on Google Play"></a>
				<?php endif ?>
			</div>
		<?php endif ?>

<?php
$app_cfg = get_option('app_config');
if ( ! empty($app_cfg['facebook_url']))
printf('<a href="%s" class="btn btn-default btn-soc-foot"><i class="fa fa-facebook"></i></a>', $app_cfg['facebook_url']);
if ( ! empty($app_cfg['instagram_url']))
printf('<a href="%s" class="btn btn-default btn-soc-foot"><i class="fa fa-instagram"></i></a>', $app_cfg['instagram_url']);
if ( ! empty($app_cfg['twitter_url']))
printf('<a href="%s" class="btn btn-default btn-soc-foot"><i class="fa fa-twitter"></i></a>', $app_cfg['twitter_url']);
if ( ! empty($app_cfg['youtube_url']))
printf('<a href="%s" class="btn btn-default btn-soc-foot"><i class="fa fa-youtube"></i></a>', $app_cfg['youtube_url']);
?>

		</div>
		<div class="site-info">
			<a href="/privacy">Privacy</a> &amp; <a href="/terms">Terms</a>
			<br />
			&copy; 2018	<a href="/" class="foot-home"><?php bloginfo('name') ?></a>
		</div>

	</footer>
</div>

<?php if (is_user_logged_in()): ?>
	<div class="modal fade" id="addListingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><img src="/wp-content/uploads/2016/01/ub-300x300.png" class="modal-logo" />&nbsp;&nbsp;Add Listing</h4>
				</div>
				<div class="modal-body">
					<?= do_shortcode('[wpuf_form id="6900"]') ?>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>

<?php wp_footer(); ?>

<script>
	var base_url = "<?= site_url() ?>";
	var ajaxurl = "<?= admin_url("admin-ajax.php") ?>";
	var logged_in = <?= is_user_logged_in() ? "true" : "false" ?>;
</script>

</body>
</html>
