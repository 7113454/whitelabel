<?php
global $current_user;

$now = time();
$status = next_start_date($post) < $now && next_end_date($post) > $now ? 'going' : (next_end_date($post) < $now ? 'past' : 'future');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('row '. $status); ?>>
	<header class="entry-header col-md-12">
	

<div class="post-top-box">

<div class="post-avatar-top">
	<a href="<?= get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><?= get_avatar($post->post_author, 40, '', get_the_author_meta('display_name'), ['extra_attr' => sprintf('title="%s"', get_the_author_meta('display_name'))]) ?></a>
	<?= follow_user_btn() ?>
</div>

<div class="post-top-title">
<h1 class="entry-title"><?php the_title() ?></h1>
</div>
<div class="clear"></div>


</div>



	</header>

	<?php if (next_end_date($post) < $now && get_the_author_meta('ID') == get_current_user_id()): ?>
		<div class="col-md-12">
			<div class="alert alert-warning">
				This event is in the past would you like to <a href="#" id="btnRelist" onclick="return edit_activity(20357)">relist</a> this activity?
			</div>
		</div>
	<?php endif ?>

	<div class="col-md-9">
		<p class="fav-thumb">
			<?php the_post_thumbnail('large', array('title' => get_the_title(), 'alt' => get_the_title())) ?>
			<?php if ($post->youtube): ?>
				<span class="playVideoBnt" data-youtube-id="<?= youtube_id($post->youtube) ?>" title="Watch Video"><i class="fa fa-play-circle"></i></span>
			<?php endif ?>
		</p>


	<div class="post-share">
		<?= attend_btn($post) ?>
		<?= ub_favorite(get_the_ID()) ?>

		<a href="#" class="btn btn-default ub-share" data-url="<?= get_the_permalink() ?>" data-text="<?= get_the_title() ?>" data-image="<?= get_the_post_thumbnail_url(null, 'full') ?>" title="Share">
			<i class="fa fa-share"></i>
		</a>

		<button type="button" class="btn btn-default pull-right" id="btnReport"><i class="fa fa-flag-o"></i></button>
		<span class="activity-views"> <?= $post->views ? $post->views : 1 ?> views</span>

		<?php if (get_the_author_meta('ID') == $current_user->ID): ?>
			<button class="btn btn-default" type="button" onclick="edit_activity(<?= get_the_ID() ?>)"><i class="fa fa-pencil-square-o"></i> Edit</button>
			<button class="btn btn-danger btn-delete-listing"><i class="fa fa-times"></i> Delete</button>

			<?php if ('featured' != $post->status): ?>
				<a href="/pay?item=activity-<?= get_the_ID() ?>" class="btn btn-default">Feature</a>
			<?php endif ?>
		<?php endif ?>
	</div>


	<div id="panel-desc" class="panel panel-default <?= panel_state('panel-desc') ?>">
		<div class="panel-heading">
			<?php the_title() ?> Details
			<a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a>
		</div>
		<div class="panel-body">
			<?php the_content() ?>

			<?php if ($post->info_url): ?>
				<div><strong>More Info:</strong> <a href="<?= $post->info_url ?>"><?= $post->info_url ?></a></div>
			<?php endif ?>
		</div>
	</div>

	<?php if (comments_open() || get_comments_number()): ?>
		<div id="panel-comments" class="panel panel-default <?= panel_state('panel-comments') ?>">
			<div class="panel-heading">Comments <a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a></div>
			<div class="panel-body">
				<?php comments_template() ?>
			</div>
		</div>
	<?php endif ?>

	<div id="panel-feed" class="panel panel-default <?= panel_state('panel-feed') ?>">
		<div class="panel-heading">
			Feed
			<a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a>
		</div>
		<div class="panel-body">
			<div class="feed-item">
				<a href="<?= get_author_posts_url(get_the_author_meta( 'ID' )) ?>">
					<?= get_avatar($post->post_author, 40, '', get_the_author_meta('display_name'), ['extra_attr' => sprintf('title="%s"', get_the_author_meta('display_name'))]) ?>
					<?= get_the_author_meta('display_name') ?>
				</a>
				published - <span class="feed-date"><?= human_time_diff(strtotime($post->post_date)) ?> ago</span>.
			</div>

			<?= get_post_feed() ?>
		</div>
	</div>

	</div>
	<div class="col-md-3">
		<div id="panel-details" class="panel panel-default <?= panel_state('panel-details') ?>">
			<div class="panel-heading">
				Details
				<a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a>
			</div>
			<div class="panel-body">
				<div class="btn btn-default btn-block" id="date-type">
					<p><strong>Start:</strong> <span class="date" data-time="<?= next_start_date($post) ?>"><?= next_start_date($post, 'm/d/Y H:i') ?></span></p>
					<p><strong>End:</strong> <span class="date" data-time="<?= next_end_date($post) ?>"><?= next_end_date($post, 'm/d/Y H:i') ?></span></p>
					<p><strong>Timezone:</strong> <span class="timezone">UTC</span></p>
				</div>
				<div class="cat-side"><?php the_category(', '); ?></div>

				<span class="tag-links"><?php the_tags('', '', '') ?>&nbsp;</span>
			</div>
		</div>



	<div id="panel-location" class="panel panel-default <?= panel_state('panel-location') ?>">
		<div class="panel-heading">
			Where
			<a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a>
		</div>
		<div class="panel-body">
			<?php if ($post->location): ?>
				<div id="activity-location"></div>
				<div id="activity-address">
					<a href="<?= site_url('/map') ?>?lat=<?= $post->location['lat'] . '&amp;lng=' . $post->location['lng'] ?>"><?= $post->location['address'] ?></a>
				</div>
			<?php else: ?>
				<span class="online-only">
					<?php if ($post->info_url): ?> <a href="<?= $post->info_url ?>">Online only</a>
					<?php else: ?> Online only
					<?php endif ?>
				</span>
			<?php endif ?>
		</div>
	</div>

	<?php if (function_exists('get_transport_near') && get_transport_near()): ?>
		<div id="panel-transit" class="panel panel-default <?= panel_state('panel-transit') ?>">
			<div class="panel-heading">
				Public Transport
				<a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a>
			</div>
			<div class="panel-body">
				<table>
				<?php foreach (get_transport_near() as $t): ?>
					<?php // if ( ! in_array($t['type'], ['train_station', 'subway_station', 'bus_station'])) continue; ?>
					<tr class="<?= $t['type'] ?>">
						<td>
							<?php if ('train_station' == $t['type']): ?>
								<i class="fa fa-train fa-2x" title="Train"></i>
							<?php elseif ('subway_station' == $t['type'] || 'light_rail_station' == $t['type']): ?>
								<i class="fa fa-subway fa-2x" title="Subway"></i>
							<?php else: ?>
								<i class="fa fa-bus fa-2x" title="Bus"></i>
							<?php endif ?>
						</td>
						<td>
							<div class="name"><?= $t['name'] ?></div>
							<div class="dist"><?= round($t['dist'], 1) ?> miles</div>
						</td>
					</tr>
				<?php endforeach ?>
				</table>
			</div>
		</div>
	<?php endif ?>

	<?php if (isset($post->age) || isset($post->fitness_level) || isset($post->disabled_facilities) || isset($post->food_available)): ?>
		<div id="panel-options" class="panel panel-default <?= panel_state('panel-options') ?>">
			<div class="panel-heading">
				Options
				<a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a>
			</div>
			<div class="panel-body">
				<?php if (isset($post->age)): ?>
					<div><strong>Age:</strong> <?= $post->age ?></div>
				<?php endif ?>
				<?php if (isset($post->fitness_level)): ?>
					<div><strong>Fitness level:</strong> <?= $post->fitness_level ?></div>
				<?php endif ?>
				<?php if (isset($post->disabled_facilities)): ?>
					<div><strong>Disabled facilities:</strong> <?= $post->disabled_facilities ?></div>
				<?php endif ?>
				<?php if (isset($post->food_available)): ?>
					<div><strong>Food available:</strong> <?= $post->food_available ?></div>
				<?php endif ?>
			</div>
		</div>
	<?php endif ?>


		<div id="panel-author" class="panel panel-default <?= panel_state('panel-author') ?>">
			<div class="panel-heading">Publisher <a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a></div>
			<div class="panel-body text-center">

				<div class="published-on">Published on <?php echo get_the_date(); ?> by</div>

				<div class="author-avatar">
					<a href="<?= get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><?= get_avatar($post->post_author, 96, '', get_the_author_meta('display_name'), ['extra_attr' => sprintf('title="%s"', get_the_author_meta('display_name'))]) ?></a>
					<?= follow_user_btn() ?>
				</div>
				<p><a href="<?= get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><strong><?= get_the_author() ?></strong></a></p>

				<p class="since">Member since: <?= date(get_option('date_format'), strtotime(get_the_author_meta( 'user_registered'))) ?></p>
				<?php /*
				<p class="center"><?= do_shortcode('[subscribe-author-button]') ?></p>
				*/ ?>
			</div>
		</div>
	</div>
</article>

<?php /*if( ! empty($post->hashtag)): ?>
	<div class="panel panel-default panel-desc similar-activities">
	<div class="panel-heading">Twitter <?= $post->hashtag ?></div>
	<div class="panel-body">
		<?= eh_tweets() ?>
	</div>
</div>
<?php endif*/ ?>

<div id="panel-similar" class="panel panel-default panel-desc <?= panel_state('panel-similar') ?>">
	<div class="panel-heading">Similar Activities <a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a></div>
	<div class="panel-body">
		<?= related_activities() ?>
	</div>
</div>

<?php if (have_recently_viewed()): ?>
	<div id="recently-viewed" class="panel panel-default panel-desc <?= panel_state('panel-similar') ?>">
		<div class="panel-heading">Recently viewed <a href="#" class="btn-collapse-panel pull-right" title="Toggle Panel"></a></div>
		<div class="panel-body">
			<div class="row"><?= show_recently_viewed() ?></div>
		</div>
	</div>
<?php endif ?>

<?php if (current_user_can('manage_options')): ?>
	<?php edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' ); ?>
<?php endif ?>

<?php function_exists('ub_count') && ub_count() ?>

<script>
	var loc = <?= json_encode($post->location) ?>;
	var post_id = <?php the_ID() ?>;
	var event_tz = '<?= get_event_timezone() ?>';

	function initMap() {
		var myLatLng = {lat: parseFloat(loc.lat), lng: parseFloat(loc.lng)};

		var map = new google.maps.Map(document.getElementById('activity-location'), {
			zoom: 17,
			disableDefaultUI: true,
			center: myLatLng
		});

		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: loc.address
		});

		var infowindow = new google.maps.InfoWindow({
			content: loc.address
		});

		marker.addListener('click', function() { infowindow.open(map, marker); });
//		infowindow.open(map, marker);
	}

	jQuery(function($) {
		if (loc) initMap();

		$('.btn-delete-listing').on('click', function() {
			if ( ! confirm('Are you sure?')) return false;

			var form = $('<form method="post" action="" class="hidden"><input type="hidden" name="action" value="remove_post" /></form>');
			$('body').append(form);
			form.trigger('submit');
		});

		$('#btnReport').on('click', function(e) {
			ub_report('post', post_id, $(this));
		});
	});
</script>
