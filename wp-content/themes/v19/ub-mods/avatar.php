<?php

add_action('show_user_profile', 'ub_form_avatar');
function ub_form_avatar($profileuser) {
	?>
	<fieldset>
		<legend>Avatar</legend>
		<table class="wpuf-table ub-background">
			<tr>
				<th><img id="avatar_image_preview" src="<?= get_avatar_url($profileuser->ID, ['size' => 150]) ?>"></th>
			</tr>
			<tr>
				<th>
					<input type="hidden" name="avatar_coords" id="avatar_coords">
					<input type="file" class="hidden" name="ub_avatar" id="ub_avatar">
					<button type="button" class="btn btn-default" onclick="document.getElementById('ub_avatar').click()">Select Image...</button>
				</th>
			</tr>
		</table>
	</fieldset>

	<link rel="stylesheet" href="<?= includes_url('/js/jcrop/jquery.Jcrop.min.css') ?>" />
	<script src="<?= includes_url('/js/jcrop/jquery.Jcrop.min.js') ?>"></script>
	<script>
	jQuery(function($) {
		var jcrop_api;

		$('#ub_avatar').on('change', function() {
			var files = !!this.files ? this.files : [];
			if (!files.length || !window.FileReader) return; // Check if File is selected, or no FileReader support
			if (/^image/.test( files[0].type)) { //  Allow only image upload
				var ReaderObj = new FileReader();
				ReaderObj.readAsDataURL(files[0]);
				ReaderObj.onloadend = function() {
					if (jcrop_api) jcrop_api.destroy();
					var img = $("#avatar_image_preview");
					img.on('load', function() {
						var trueSize = [img[0].naturalWidth, img[0].naturalHeight]; // fix for big images
						var max = trueSize[0] < trueSize[1] ? trueSize[0] : trueSize[1]; // initial position for crop

						var settings = {
							aspectRatio: 1,
							onChange: setCoords,
							onSelect: setCoords,
							trueSize: trueSize,
							setSelect: [0, 0, max, max]
						};
						img.Jcrop(settings, function() { jcrop_api = this; });
					});
					img.attr({'src': this.result, 'style': '', 'width': '100%'});
				}
			}
		});

		$('#your-profile').attr('enctype', 'multipart/form-data');
		$('#email').attr('type', 'email');
	});

	// update avatar crop coordinates
	function setCoords(coords) {
		var str = JSON.stringify(coords);
		jQuery('#avatar_coords').val(str);
	}
	</script>
	<?php
}

add_action('personal_options_update', 'ub_update_avatar');
function ub_update_avatar($user_id) {
	if (isset($_FILES['ub_avatar'])) {
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/media.php');

		$crop = json_decode(stripslashes_deep($_POST['avatar_coords']));
		if ($crop->w && $crop->h) {
			$img = wp_get_image_editor($_FILES['ub_avatar']['tmp_name']);
			if ( ! is_wp_error($img)) {
				$img->crop($crop->x, $crop->y, $crop->w, $crop->h);
				$result = $img->save();
				rename($result['path'], $_FILES['ub_avatar']['tmp_name']);
			}
		}

		$id = media_handle_upload('ub_avatar', 0);

		if ( ! is_wp_error($id)) {
			avatar_delete(); // delete old avatar
			update_user_meta($user_id, 'wp_user_avatar', $id);
		}
	}
}

add_filter('get_avatar_url', 'ub_avatar_url', 10, 3);
function ub_avatar_url($url, $id_or_email, $args) {
	$size_arr = is_numeric($args['size']) ? array($args['size'],$args['size']) : $args['size'];
	$url_out = '';

	if (isset($id_or_email->comment_author_email)) {
		$user = get_user_by('email', $id_or_email->comment_author_email);
	}
	else {
		$user = is_numeric($id_or_email) ? get_user_by('id', $id_or_email) : get_user_by('email', $id_or_email);
	}

	$image_id = $user ? get_user_meta($user->ID, 'wp_user_avatar', true) : false;
	if ($image_id) {
		$src = wp_get_attachment_image_src($image_id, $size_arr);
		$url_out = $src ? $src[0] : '';
	}

	if ( ! $url_out) {
		$src = wp_get_attachment_image_src(get_option('default_avatar_id'), $size_arr);
		$url_out = $src ? $src[0] : '';
	}

	return $url_out;
}

function avatar_delete() {
	$user_id = get_current_user_id();
	$media_id = get_user_meta($user_id, 'wp_user_avatar', true);
	wp_delete_attachment($media_id);
	delete_user_meta($user_id, 'wp_user_avatar');
}
