<div class="panel panel-default">
	<div class="panel-heading">Profile views</div>
	<div class="panel-body">
		<div id="user_views"></div>
	</div>
</div>

<?php if ($query->have_posts()): ?>
<div class="panel panel-default">
	<div class="panel-heading">Activities</div>
	<div class="panel-body">
		<table id="activity-stats" class="table">
			<thead>
				<tr>
					<th>Title</th>
					<th>Views</th>
					<th>Favorite</th>
				</tr>
			</thead>
			<tbody>
			<?php while ($query->have_posts()): $query->the_post(); ?>
				<tr>
					<td><a href="<?php the_permalink() ?>"><?php the_title() ?></a></td>
					<td><?php printf('%d', $query->post->views) ?></td>
					<td><?= intval($query->post->_like) ?></td>
				</tr>
			<?php endwhile ?>

			<tr>
				<td colspan="3">
					<div class="pull-right">
						<?php previous_posts_link() ?>
						&nbsp;&nbsp;
						<?php next_posts_link(null, $query->max_num_pages) ?>
					</div>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<?php else: ?>
	<div class="alert alert-info" role="alert">You have no activities yet.</div>
<?php endif ?>

<script src="https://www.gstatic.com/charts/loader.js"></script>
<script src="<?= get_template_directory_uri() ?>/js/tablesort.min.js"></script>
<script>
	if (document.getElementById('activity-stats')) {
		new Tablesort(document.getElementById('activity-stats'));
	}

	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(drawUserViews);

	function drawUserViews() {
		var u_views = <?= json_encode(get_user_views($user_id), JSON_NUMERIC_CHECK) ?>;
		var data = google.visualization.arrayToDataTable(u_views);

		var options = {
			legend: {position: 'none'},
			vAxis: {format: '0'}
        };
		var chart = new google.visualization.AreaChart(document.getElementById('user_views'));
        chart.draw(data, options);
	}
</script>
