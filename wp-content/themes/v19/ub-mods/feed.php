<?php
/*
-- TABLE STRUCTURE
CREATE TABLE `wp_feed` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `action` enum('like','attend') NOT NULL DEFAULT 'like',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/

add_action('ub_like', 'like_save');
function like_save($data) {
	global $wpdb;
	if ( ! $user_id = get_current_user_id()) return;

	$tbl = $wpdb->prefix . 'feed';
//	$wpdb->delete($tbl, ['post_id' => $post_id]); // delete previous records
	$wpdb->insert($tbl, ['action' => 'like', 'post_id' => $data['post_id'], 'user_id' => $data['user_id']]);
}

add_action('ub_unlike', 'like_remove');
function like_remove($data) {
	global $wpdb;
	$tbl = $wpdb->prefix . 'feed';
	$wpdb->delete($tbl, ['post_id' => $data['post_id'], 'user_id' => $data['user_id'], 'action' => 'like']);
}

add_action('attending_add', 'attend_save');
function attend_save($post_id) {
	global $wpdb;
	$tbl = $wpdb->prefix . 'feed';
	// $wpdb->delete($tbl, ['post_id' => $post_id]); // delete previous records
	$wpdb->insert($tbl, ['action' => 'attend', 'post_id' => $post_id, 'user_id' => get_current_user_id()]);
}

add_action('attending_remove', 'attend_remove');
function attend_remove($post_id) {
	global $wpdb;
	$tbl = $wpdb->prefix . 'feed';
	$wpdb->delete($tbl, ['post_id' => $post_id, 'user_id' => get_current_user_id(), 'action' => 'attend']);
}

add_action('rest_api_init', 'rest_register_feed');
function rest_register_feed() {
	register_rest_field('post', 'feed2', array('get_callback' => 'rest_post_feed'));
}

function rest_post_feed($object) {
	global $wpdb;
	$feed = array();

	$sql = "SELECT user_id, display_name, date
		FROM {$wpdb->prefix}feed AS f 
		INNER JOIN {$wpdb->users} as u ON f.user_id = u.ID
		WHERE post_id = {$object['id']} AND action = 'like'
		ORDER BY date DESC
		LIMIT 3";
	$feed['like'] = $wpdb->get_results($sql, ARRAY_A);
	$last_like = isset($feed['like'][0]) ? strtotime($feed['like'][0]['date']) : 0;

	foreach ($feed['like'] as $k => $f) {
		unset($feed['like'][$k]['date']);
		$feed['like'][$k]['avatar'] = rest_get_avatar_urls($f['user_id']);
	}

	$sql = "SELECT user_id, display_name, date
		FROM {$wpdb->prefix}feed AS f 
		INNER JOIN {$wpdb->users} as u ON f.user_id = u.ID
		WHERE post_id = {$object['id']} AND action = 'attend'
		ORDER BY date DESC
		LIMIT 3";
	$feed['attend'] = $wpdb->get_results($sql, ARRAY_A);
	$last_attend = isset($feed['attend'][0]) ? strtotime($feed['attend'][0]['date']) : 0;

	foreach ($feed['attend'] as $k => $f) {
		unset($feed['attend'][$k]['date']);
		$feed['attend'][$k]['avatar'] = rest_get_avatar_urls($f['user_id']);
	}

	$sql = "SELECT action, COUNT(*) AS 'count' FROM {$wpdb->prefix}feed WHERE post_id = {$object['id']} GROUP BY action";
	$total = $wpdb->get_results($sql, OBJECT_K);

	$feed['total_like'] = isset($total['like']) ? $total['like']->count : 0;
	$feed['total_attend'] = isset($total['attend']) ? $total['attend']->count : 0;
	$feed['last_action'] = $last_attend > $last_like ? 'attend' : 'like';

	return $feed;
}

function get_post_feed($post = null, $page = 1) {
	global $wpdb;
	$post = get_post($post);
	$html = '';

	$limit = 25;
	$start = ($page - 1) * $limit;

	$sql = "SELECT action, UNIX_TIMESTAMP(`date`) AS 'date', user_id, user_nicename, display_name
		FROM {$wpdb->prefix}feed
		JOIN {$wpdb->users} ON user_id = {$wpdb->users}.ID
		WHERE post_id = %d
		ORDER BY date ASC
		LIMIT $start,$limit";
	$sql = $wpdb->prepare($sql, $post->ID);
	$result = $wpdb->get_results($sql);

	foreach ($result as $f) {
		$a = 'like' == $f->action ? 'liked this' : 'is attending';
		$ava = get_avatar($f->user_id, 40, '', $f->display_name, ['extra_attr' => sprintf('title="%s"', $f->display_name)]);
		$html .= '<div class="feed-item">';
		$html .= sprintf('<a href="%s">%s %s</a> %s - <span class="feed-date">%s ago</span>.', get_author_posts_url($f->user_id, $f->user_nicename), $ava, $f->display_name, $a, human_time_diff($f->date)); 
		$html .= '</div>';
	}

	if ($result) {
		$url = admin_url("admin-ajax.php")."?action=post_feed&amp;post_id={$post->ID}&amp;page=".($page+1);
		$html .= '<button type="button" class="btn btn-default btn-block btn-more" data-target="'.$url.'">More...</button>';
	}
	return $html;
}

add_action('wp_ajax_post_feed', 'ajax_post_feed');
add_action('wp_ajax_nopriv_post_feed', 'ajax_post_feed');
function ajax_post_feed() {
	$out = array('html' => get_post_feed($_GET['post_id'], $_GET['page']));
	wp_send_json($out);
}

function panel_state($id) {
	$closed = isset($_COOKIE['closed']) ? explode(',', $_COOKIE['closed']) : array();
	$status = '';
	if (in_array($id, $closed)) {
		$status = 'panel-closed';
	}
	return $status;
}
