<?php

// add "switchable" field to "User Edit" page
// add_action('show_user_profile', 'switchable_field');
add_action('edit_user_profile', 'switchable_field');
function switchable_field($user) {
	$checked = get_the_author_meta('_switchable', $user->ID) ? 'checked' : '';
	echo '<table class="form-table"><tr>';
	echo '<th>Switchable</th>';
	echo '<td><label><input name="_switchable" type="checkbox" '.$checked.'> Allow admins switch to this account</label></td>';
	echo '</tr></table>';
}

add_action('personal_options_update', 'update_switchable_field');
add_action('edit_user_profile_update', 'update_switchable_field');
function update_switchable_field($user_id) {
	if ( ! current_user_can('edit_user', $user_id)) return;

	if (empty($_POST['_switchable'])) {
		delete_user_meta($user_id, '_switchable');
	}
	else {
		update_user_meta($user_id, '_switchable', 1);
	}
}

//add_filter('determine_current_user', 'switch_logged_in_cookie', 15);
add_action('init', 'switch_logged_in_cookie');
function switch_logged_in_cookie() {
	if (empty($_POST['switch_to']) || ! is_switched_admin()) return;

	// backup LOGGED_IN_COOKIE
	$cookie_name = '~' . LOGGED_IN_COOKIE;
	if (empty($_COOKIE[$cookie_name])) {
		setcookie($cookie_name, $_COOKIE[LOGGED_IN_COOKIE], 0, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), true);
	}

	wp_set_auth_cookie($_POST['switch_to']);

	wp_redirect($_SERVER['REQUEST_URI']);
	exit;
}

add_action('clear_auth_cookie', 'clear_switched_cookie');
function clear_switched_cookie() {
	setcookie('~' . LOGGED_IN_COOKIE, ' ', time() - YEAR_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN);
}

// is the user admin or was him?
function is_switched_admin() {
	if ( ! defined('SWITCHED_ADMIN_ID')) {
		$cookie_name = '~' . LOGGED_IN_COOKIE;
		$cookie = isset($_COOKIE[$cookie_name]) ? $_COOKIE[$cookie_name] : '';
		$user_id = $cookie ? wp_validate_auth_cookie($cookie, 'logged_in') : 0;

		if ( ! $user_id) {
			$user_id = is_super_admin() ? get_current_user_id() : 0;
		}

		define('SWITCHED_ADMIN_ID', $user_id);
	}

	return SWITCHED_ADMIN_ID;
}

function account_switch_box() {
	global $wpdb;

	if ( ! is_switched_admin()) return;

	$sql = "SELECT ID, user_login
		FROM {$wpdb->usermeta}
		JOIN {$wpdb->users} ON user_id = ID
		WHERE meta_key = '_switchable' OR ID = %d
		ORDER BY user_login ";
	$sql = $wpdb->prepare($sql, is_switched_admin());

	$users = $wpdb->get_results($sql, OBJECT_K);

	$out = '<form method="post" action="" id="form_account_switch">';
	$out .= '<select id="account_switch" name="switch_to" class="form-control" onchange="this.form.submit()">';

	$current_uid = get_current_user_id();
	foreach ($users as $u) {
		$sel = $u->ID == $current_uid ? 'selected' : '';
		$out .= sprintf('<option value="%d" %s>%s</option>', $u->ID, $sel, $u->user_login);
	}

	$out .= '</select></form>';

	return $out;
}

add_filter('user_row_actions', 'ub_login_action', 10, 2);
function ub_login_action($actions, $user_object) {
	if (is_super_admin()) {
		$actions['login'] = '<a href="#" class="ub_switch_btn">Login</a>';
	}
	return $actions;
}

add_action('in_admin_footer', 'ub_switch_js');
function ub_switch_js() {
	if ( ! is_super_admin()) return;
	?>
	<script>
		jQuery('.ub_switch_btn').on('click', function(e) {
		e.preventDefault();

		var id = jQuery(this).closest('tr').attr('id').split('-');
		var form = jQuery('<form method="post" action="/"><input type="hidden" name="switch_to" value="'+id[1]+'"></form>');
		form.appendTo('body').trigger('submit');
		});
	</script>
	<?php
}
