<?php

add_shortcode('ub_map', 'ub_activities_map');
function ub_activities_map() {
	ob_start();
?>
	<div id="activities-map-buttons">
		<div class="btn-group" data-toggle="buttons">
			<label class="btn btn-default active">
				<input type="radio" value="upcoming" autocomplete="off" checked> Upcoming
			</label>
			<label class="btn btn-default">
				<input type="radio" value="past" autocomplete="off"> Past
			</label>
			<!--
			<button type="button" class="btn btn-default btn-upcoming active">Upcoming</button>
			<button type="button" class="btn btn-default btn-past">Past</button>
			-->
		</div>
	</div>
	<div id="activities-map"></div>
	<script src="<?= get_template_directory_uri() ?>/js/markerclusterer.js"></script>
	<script>
		var activities = {};
		jQuery(function($){
			$.getJSON(base_url+'/posts.json', function(r) {
				activities = r;
				ub_activities_map('upcoming');
			});
		});
	</script>
<?php
return ob_get_clean();
}

add_action('ub_cron_daily', 'build_posts_json');
function build_posts_json() {
	$fname = ABSPATH . 'posts.json';

	$now = time();
	$activities = array('upcoming' => array(), 'past' => array());

	$qry = array(
		'nopaging' => true,
		'meta_query' => array(
			array('key' => 'location', 'value' => '', 'compare' => '!='),
			array('key' => 'date_end', 'value' => $now, 'compare' => '>', 'type' => 'NUMERIC'),
		)
	);

	$q = new WP_Query($qry);
	while ( $q->have_posts() ) {
		$q->the_post();

		$item = array(
			'title' => get_the_title(),
			'location' => $q->post->location,
			'url' => get_permalink(),
			'author' => get_the_author(),
			'author_url' => get_author_posts_url($q->post->post_author),
			'img' => get_the_post_thumbnail_url(null, 'thumb-small'),
			'date' => $q->post->date,
//			'text' => wp_trim_words(strip_tags(get_the_content()), 15),
			'featured' => 'featured' == $q->post->post_status,
		);

		if ($q->post->date_end > 0 && $q->post->date_end > $now) {
			$activities['upcoming'][] = $item;
		}
	}
	wp_reset_postdata();

	// remove base url to reduce json size
	$activities = str_replace(str_replace('/', '\/', site_url()), '', json_encode($activities));
	file_put_contents($fname, $activities);
}

function remove_invalid_locations() {
	global $wpdb;

	$cnt = 0;
	$all = $wpdb->get_results("SELECT post_id, meta_value FROM {$wpdb->postmeta} WHERE meta_key = 'location'");
	foreach ($all as $a) {
		$location = unserialize($a->meta_value);

		if ( ! empty($location['address']) && is_numeric($location['lat']) && is_numeric($location['lng'])) continue;

		delete_post_meta($a->post_id, 'location');
		wp_set_object_terms($a->post_id, null, 'city');
		$cnt++;
	}

	return $cnt;
}