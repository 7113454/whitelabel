<div class="author author-preview" style='background-image: url(<?= ub_user_background($user) ?>)'>
	<?php $author_url = get_author_posts_url( $user->ID ) ?>
	<div class="authorInfo">
		<div class="authorAvatar"><a href="<?= $author_url ?>"><?= get_avatar( $user->user_email, '128' ) ?></a></div>
	</div>
	<div class="clearfix">
		<h2 class="authorName"><a href="<?= $author_url ?>"><?= $user->display_name ?></a></h2>
		<?= do_shortcode('[subscribe-author-button]') ?>
	</div>
	<p class="authorDescrption"><?php echo get_user_meta($user->ID, 'description', true); ?></p>

	<ul class="socialIcons">
		<?php
			$website = $user->user_url;
			if($user->user_url != '')
			{
				printf('<li><a href="%s" class="btn btn-default">%s</a></li>', $user->user_url, '<i class="fa fa-link"></i>');
			}

			$twitter = get_user_meta($user->ID, 'twitter_profile', true);
			if($twitter != '')
			{
				printf('<li><a href="%s">%s</a></li>', $twitter, 'Twitter');
			}

			$facebook = get_user_meta($user->ID, 'facebook_profile', true);
			if($facebook != '')
			{
				printf('<li><a href="%s">%s</a></li>', $facebook, 'Facebook');
			}

			$google = get_user_meta($user->ID, 'google_profile', true);
			if($google != '')
			{
				printf('<li><a href="%s">%s</a></li>', $google, 'Google');
			}

			$linkedin = get_user_meta($user->ID, 'linkedin_profile', true);
			if($linkedin != '')
			{
				printf('<li><a href="%s">%s</a></li>', $linkedin, 'LinkedIn');
			}
		?>
	</ul>
</div>
