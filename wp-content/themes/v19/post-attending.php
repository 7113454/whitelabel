<?php get_header() ?>
<div class="container">
	<?php
	$q = attending_members($post);
	$cnt = count($q->results);
	?>

	<h1><?php the_title() ?></h1>
	<h2><?= $cnt ?> members going to attend</h2>

	<?php if ( ! empty( $q->results)): ?>
		<div class="attending-members">
			<?php foreach ($q->results as $k => $user): ?>
				<div class="member">
					<a class="avatar" href="<?= get_author_posts_url($user->ID) ?>" alt="<?= $user->display_name ?>"><?= get_avatar($user->ID) ?></a>
					<a class="name" href="<?= get_author_posts_url($user->ID) ?>"><?= $user->display_name ?></a>
				</div>
			<?php endforeach ?>
		</div>
	<?php endif ?>
</div>
<?php get_footer() ?>
