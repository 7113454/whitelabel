<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content container" role="main">

			<?php if ( have_posts() ) : ?>
				<header class="page-header">
					<h6 class="page-title"><?php printf( __( 'Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h6>
				</header><!-- .page-header -->
			
				<div class="infinite-scroll">
					<div class="row search-results">
						<?php while ( have_posts() ) : the_post() ?>
							<div class="col-md-4">
								<?php get_template_part( 'content', get_post_format() ) ?>
							</div>
						<?php endwhile ?>

						<span class="pager next"><?php next_posts_link('Next') ?> </span>
					</div>
				</div>

			<?php else: ?>
					<?php get_template_part( 'content', 'none' ) ?>
			<?php endif ?>

		</div><!-- #content -->
	</section><!-- #primary -->

<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();
