<?php
/*
Template Name: Display Authors
*/

// Get all users order by amount of posts
$allUsers = get_users('orderby=post_count&order=DESC&number=10');

$users = array();

// Remove subscribers from the list as they won't write any articles
foreach($allUsers as $currentUser)
{
	if(!in_array( 'subscriber', $currentUser->roles ))
	{
		$users[] = $currentUser;
	}
}

?>

<?php get_header(); ?>



				<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="leader-menu center">

<h1><?php echo get_query_var('name'); ?>

</h1>
<a href='/hot' class='btn btn-default'><i class='fa fa-arrow-up'></i>  Popular Activities</a> <a href='/users' class='btn btn-default disabled'><i class='fa fa-group'></i>  Top Users</a>  <!--<a href='/gifting' class='btn btn-default'><i class='fa fa-gift'></i> Gifting</a>--> <!--<a href='/vendor' class='btn btn-default'><i class='fa fa-building'></i>  Vendors</a>-->  <!--<a href='/map' class='btn btn-default'><i class='fa fa-map-marker'></i>  Map</a>--> 
</div>


<section class="content" role="main">



	<?php
		printf('<h1>%s</h1>', the_title());

		foreach($users as $user)
		{
			include(get_stylesheet_directory() . '/content-user.php');
		}
	?>
</section>

	</div>
			</div>


<?php get_footer(); ?>
