<?php
/**
 * Template Name: No Edge, No Sidebar
 *
 * @package WordPress
 * @subpackage V1
 * @since V1
 */

get_header(); ?>
	<div class="page-trim"></div>
	

	<div id="edge" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
	
<?php get_footer(); ?>