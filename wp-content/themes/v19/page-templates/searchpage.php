<?php
/*
Template Name: Search Page
*/

<?php get_header(); ?>


				<div class="row">
				<div class="col-md-10 col-md-offset-1">
				<div class="leader-menu center">

<a href='/leaderboard' class='btn btn-default'><i class='fa fa-arrow-up'></i>  Top</a> <a href='/users' class='btn btn-default'><i class='fa fa-list'></i>  Latest</a>  <!--<a href='/gifting' class='btn btn-default'><i class='fa fa-gift'></i> Gifting</a>--> <!--<a href='/vendor' class='btn btn-default'><i class='fa fa-building'></i>  Vendors</a>-->  <!--<a href='/map' class='btn btn-default'><i class='fa fa-map-marker'></i>  Map</a>--> 
</div>


<section class="content" role="main">



<?php get_search_form(); ?>

</section>

	</div>
			</div>


<?php get_footer(); ?>