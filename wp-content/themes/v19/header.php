<!DOCTYPE html>
<?php
$current_user = wp_get_current_user();
$unread_msg = msg_unread_cnt() ? 'unread-msg' : '';
$app_cfg = get_option('app_config');
?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<link rel="manifest" href="/manifest.json">
</head>
<body <?php body_class($unread_msg) ?>>

<div class="top-menu">
	<div class="container">

		<div class="pull-left">
			<?php if (is_user_logged_in()): ?>
				<a href='/latest'>
					<img src="<?= get_logo_url() ?>" class='logo-in' alt="<?php bloginfo('name') ?>" title="<?php bloginfo('name') ?>">
					<?php if ($current_user->unread): ?>
						<span class="notification" title="New posts since last login"><?= count($current_user->unread) ?></span>
					<?php endif ?>
				</a>
			<?php else: ?>
				<a href='/'><img src="<?= get_logo_url() ?>" class='logo' alt="<?php bloginfo('name') ?> logo" title="<?php bloginfo('name') ?>"></a>
			<?php endif ?>


			<a href='/hot'  class='btn btn-default btn-hot'>
				<img src="<?= get_theme_file_uri('images/flame.png') ?>" class="flame" alt="" />
				<div class='hot-txt'>Hot</div>
			</a>

			<a class="btn btn-default search-toggle <?= empty($_COOKIE['showSearch']) ? '' : 'active' ?>" href="#"><i class='fa fa-search'></i></a>
		</div>
		<div class="pull-right top-right-menu">

			<?php if (is_user_logged_in()): ?>
				<button type='button' class='btn btn-default btn-top-add' data-toggle='modal' data-target='#addListingModal' title='Submit Activity'><i class='fa fa-plus'></i></button>
				<?php if (can_submit_status()): ?>
					<a href="<?= site_url('add-status') ?>" class="btn btn-default" title="Submit Status"><i class="fa fa-comment"></i></a>
				<?php endif ?>

				<div class='dropdown'>
					<a href='/profile' class='btn btn-default collapsed' data-toggle='dropdown'>
						<?= get_avatar( $current_user->ID, 46 ); ?>
						<i class="fa fa-envelope msg-notif" title="New message"></i>
						<span class='vanish'><?= $current_user->display_name ?></span>
					</a>

					<ul class='dropdown-menu' role='menu'>
						<li><a href="<?= messages_page_url() ?>"><i class="fa fa-envelope"></i> Messages</a></li>
						<li><a href="<?= get_author_posts_url($current_user->ID, $current_user->user_nicename) ?>"><i class='fa fa-user'></i> Public profile</a></li>
						<li><a href='/edit'><i class='fa fa-pencil'></i> Edit profile</a></li>		

						<li><a href='/analytics'><i class='fa fa-line-chart'></i> Analytics</a></li>							
						<li><a href="<?= site_url('notifications') ?>"><i class='fa fa-info-circle'></i> Notifications</a></li>							
					
						<!--<li><a href='/attending'>6 Attending</a></li>-->							
						<!--<li><a href='/wishlist'><i class='fa fa-gift'></i> Wishlist</a></li>-->							
						<!--<li><a href='/upgrade'><i class='fa fa-line-chart'></i> Upgrade</a></li>-->
						<?= account_switch_box() ?>
						<li><a href='/help'><i class='fa fa-support'></i> Help</a></li>							
						<!--<li><a href='/support'><i class='fa fa-support'></i> Support</a></li>-->							
						<li><a href="<?= wp_logout_url(home_url()) ?>"><i class="fa fa-unlock"></i> Logout</a></li>
					</ul>
				</div>

			<?php else: ?>
				<a href="<?= wp_login_url() ?>" class="btn btn-default btn-top-login">Login</a>
				<a href="<?= wp_registration_url() ?>" class="btn btn-default btn-top-reg">Signup</a>
			<?php endif ?>
		</div>
	</div>
</div>

<div class="search-bar" style="<?= empty($_COOKIE['showSearch']) ? 'display:none;' : '' ?>" id="collapsedSearch">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<?php if (function_exists('ub_search_form')) echo ub_search_form() ?>
		</div>
	</div>
	<a class="btn btn-default btn-block btn-close-search search-toggle" href="#">...</a>
</div>

<div id="page" class="hfeed site">
	<div id="main" class="site-main">

