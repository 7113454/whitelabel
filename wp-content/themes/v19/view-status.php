<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<!--favorite-->
		<?php // function_exists('wpfp_link') && wpfp_link(); ?>

		<?php the_title( '<h1 class="entry-title">', '</h1>') ?>
	</header>

<div class="status-author"><a href="<?= get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><?= get_avatar($post->post_author, 40, '', get_the_author_meta('display_name'), ['extra_attr' => sprintf('title="%s"', get_the_author_meta('display_name'))]) ?></a></p>
				<p><a href="<?= get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><strong><?= get_the_author() ?></strong></a></p>
</div>

	<div class="post-share">
		<a href="#" class="btn btn-default ub-share" data-url="<?= get_the_permalink() ?>" data-text="<?= get_the_title() ?>" data-image="<?= get_the_post_thumbnail_url(null, 'full') ?>" title="Share">
			<i class="fa fa-share"></i>
		</a>

		<?= ub_favorite(get_the_ID()) ?>
	</div>

	<?php the_post_thumbnail('full', array('title' => get_the_title(), 'alt' => get_the_title())) ?>

	<div class="post-content"><?php the_content() ?></div>


	<footer class="entry-meta">
		<span class="tag-links"><?php the_tags('', '', '') ?>&nbsp;</span>
	</footer>
</article>

<?php if (comments_open() || get_comments_number()): ?>
	<div class="panel panel-default">
		<div class="panel-heading">Comments</div>
		<div class="panel-body">
			<?php comments_template() ?>
		</div>
	</div>
<?php endif ?>

<?php if (current_user_can('manage_options')): ?>
	<?php edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' ); ?>
<?php endif ?>
<?php function_exists('ub_count') && ub_count() ?>

