<?php

add_action('show_user_profile', 'ub_form_bg');
add_action('personal_options_update', 'ub_update_bg');
add_shortcode('ub_favorite_posts', 'ub_favorite_posts');

remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rest_output_link_wp_head', 10);

add_image_size('thumb-small', 40, 40, true); // for Apple Watch

// google map conflict in "User frontend" and "Custom fields"
add_action('wp_print_scripts', 'ub_dequeue_script', 100);
function ub_dequeue_script() {
	wp_dequeue_script('google-maps');
}

add_action('wp_enqueue_scripts', 'ub_scripts');
function ub_scripts() {
	wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/bs.css', array(), '3.0.0');
	wp_enqueue_style('twentyfourteen-style', get_stylesheet_uri(), array(), '111117');

	wp_enqueue_script('momentjs', get_template_directory_uri() . '/js/moment.min.js', array(), '2.14.1', true);
	wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/js/js.min.js', array(), '111117', true);
//	wp_enqueue_script('vu-form', get_template_directory_uri() . '/js/vu-form.js', array( 'jquery' ), '090117', true );
	wp_enqueue_script('twentyfourteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '111117', true );
	if ( ! empty(GOOGLE_API_KEY)) {
		wp_enqueue_script('google-maps-ub', 'https://maps.google.com/maps/api/js?libraries=places&key=' . GOOGLE_API_KEY, array(), null);
	}
}

// form for background image
function ub_form_bg($profileuser) {
	?>
	<fieldset>
		<legend>Background</legend>
		<table class="wpuf-table ub-background">
			<tr>
				<th><div style="background-image:url(<?= ub_user_background($profileuser) ?>);" id="bg_image_preview"></div></th>
			</tr>
			<tr>
				<th>
					<input type="file" class="hidden" name="ub_background" id="ub_background">
					<button type="button" class="btn btn-default" onclick="document.getElementById('ub_background').click()">Select Image...</button>
				</th>
			</tr>
		</table>
	</fieldset>
	<?php
}

// save user background
function ub_update_bg($user_id) {
	if (isset($_FILES['ub_background'])) {
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/media.php');
		$id = media_handle_upload('ub_background', 0);

		if ( ! is_wp_error($id)) {
//			$attach = wp_get_attachment_image_src($id, 'full');
//			$src = str_replace(site_url(), '', $attach[0]);
			update_user_meta($user_id, 'ub_background', $id);
		}
	}
}

// delete current member's background image
function ub_delete_bg(WP_User $user) {
	$media_id = get_user_meta($user->ID, 'ub_background', true);

	wp_delete_attachment($media_id);
	delete_user_meta($user->ID, 'ub_background');
}

// create user background image from base64 encoded string
function ub_create_image($base64_image) {
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/media.php');

	$allowed = array('gif' => 'image/gif', 'png' => 'image/png', 'jpg' => 'image/jpeg');
	$dir = wp_upload_dir();
	$file = tempnam($dir['path'], '');
	chmod($file, 0644); // change permissions so web server can read the file

	file_put_contents($file, base64_decode($base64_image));
	$mime = mime_content_type($file);

	if ($ext = array_search($mime, $allowed)) { // add file extension based on mime
		rename($file, $file . '.' . $ext);
		$file .= '.' . $ext;
	}
	else { // invalid mime
		unlink($file);
		return false;
	}

	// register image
	$attachment = array(
		'guid' => $dir['url'] . '/' . basename($file),
		'post_mime_type' => $mime,
		'post_title' => basename($file),
		'post_status' => 'inherit',
		'post_date' => date('Y-m-d H:i:s')
	);
	$attachment_id = wp_insert_attachment($attachment, $file);
	$attachment_data = wp_generate_attachment_metadata($attachment_id, $file);
	wp_update_attachment_metadata($attachment_id, $attachment_data);

	return $attachment_id;
}

function ub_user_background(WP_User $user) {
	if (ctype_digit($user->ub_background)) {
		$url = wp_get_attachment_image_src($user->ub_background, 'large');
		$url = $url ? $url : wp_get_attachment_image_src(get_option('default_bg_id'), 'large');
	}
	else {
		$url = is_file(ABSPATH . $user->ub_background) ? site_url($user->ub_background) : wp_get_attachment_image_src(get_option('default_bg_id'), 'large');
	}
	$url = is_array($url) ? $url[0] : $url;
	return $url;
}

// add_action('wp_login', 'ub_last_login', 10, 2);
// function ub_last_login($login, WP_User $user) {
// 	global $wpdb;

// 	$sql = $wpdb->prepare( "SELECT author_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE subscriber_id = %d AND status = 'active'", $user->ID);
// 	$authors = $user->ID ? $wpdb->get_col($sql) : array();

// 	if ($authors) {
// 		$authors_str = implode(',', $authors);
// 		$sql = "SELECT COUNT(*) FROM {$wpdb->posts} WHERE post_type = 'post' AND post_status = 'publish' "
// 			. "AND post_date > '{$user->last_login}' AND post_author IN ($authors_str)";
// 		$new_posts = $wpdb->get_var($sql);
// 		setcookie('new_posts', $new_posts);
// 	}

// 	update_usermeta($user->ID, 'last_login', date('Y-m-d H:i:s', time()));
// }

add_action('init', 'ub_on_init');
function ub_on_init() {
	// lowercase all URLs
	if (preg_match('/[A-Z]/', $_SERVER['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = strtolower($_SERVER['REQUEST_URI']);
		$_SERVER['PATH_INFO']   = strtolower($_SERVER['PATH_INFO']);
	}

	// it is theme customizer preview, do nothing
	if (isset($_GET['customize_changeset_uuid'])) return;

	$user = wp_get_current_user() ;
	if ('/latest' == $_SERVER["REQUEST_URI"] && $user->unread) {
		update_user_meta($user->ID, 'unread', array());
	}

	if ($user->ID && get_transient('_seen') === false) {
		set_transient('_seen', time(), 60);
		update_user_meta($user->ID, '_seen', time());
	}

	// redirect already logged in user from login page
	if (in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php')) && is_user_logged_in()) {
		$redirect = '/latest/';
		if (isset($_SERVER['HTTP_REFERER']) && 0 === strpos($_SERVER['HTTP_REFERER'], 'http://ub.fitness')) {
			$redirect = str_replace('http://', 'https://', $_SERVER['HTTP_REFERER']);
		}
		wp_redirect($redirect);
	}

	if ('/' == $_SERVER["REQUEST_URI"] && is_user_logged_in()) {
		wp_redirect('latest/');
	}

	// do not include "WP user frontend" scripts for guests
	if ( ! is_user_logged_in() && 0 !== strpos($_SERVER["REQUEST_URI"], '/join')) {
		$wpuf = WP_User_Frontend::init();
		remove_action('wp_enqueue_scripts', array($wpuf, 'enqueue_scripts'));
	}
}

function ub_followers_cnt($user_id) {
	global $wpdb;

	$sql = $wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}wpsa_subscribe_author WHERE author_id = %d", $user_id);
	return $wpdb->get_var($sql);
}

function ub_following_cnt($user_id) {
	global $wpdb;

	$sql = $wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}wpsa_subscribe_author WHERE subscriber_id = %d", $user_id);
	return $wpdb->get_var($sql);
}

add_shortcode('most_followed_users', 'most_followed_list');
function most_followed_list($atts) {
	global $wpdb;
	ob_start();

	$limit = isset($atts['limit']) ? $atts['limit'] : 10;
	$sql = "SELECT author_id FROM {$wpdb->prefix}wpsa_subscribe_author GROUP BY author_id ORDER BY COUNT(*) DESC LIMIT $limit";
	$ids = $wpdb->get_col($sql);

	$users = get_users(['include' => $ids, 'orderby' => 'include']);
	foreach ($users as $user) {
		include (get_template_directory() . '/content-user.php');
	}
	return ob_get_clean();
}

add_shortcode('last_seen_users', 'last_seen_users_list');
function last_seen_users_list($atts) {
	global $wpdb;
	ob_start();

	$limit = isset($atts['limit']) ? $atts['limit'] : 10;
	$sql = "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key = '_seen' ORDER BY meta_value DESC LIMIT $limit";
	$ids = $wpdb->get_col($sql);

	$users = get_users(['include' => $ids, 'orderby' => 'include']);
	foreach ($users as $user) {
		include (get_template_directory() . '/content-user.php');
	}
	return ob_get_clean();
}

function ub_favorite_posts($attr) {
	$favorite_post_ids = get_user_likes();
	ob_start();

	if ($favorite_post_ids) {
		$posts_per_page = isset($attr['posts_per_page']) ? $attr['posts_per_page'] : get_option('posts_per_page');
		$page = intval(get_query_var('paged'));

		$qry = array('post__in' => $favorite_post_ids, 'posts_per_page'=> $posts_per_page, 'orderby' => 'post__in', 'paged' => $page);
		$q = new WP_Query( $qry );

		while ( $q->have_posts() ) {
			$q->the_post();
			get_template_part('content', 'favourite-author-posts');
		}
		echo '<span class="pager next">' . get_next_posts_link('Next', $q->max_num_pages) . '</span>';
		wp_reset_postdata();
	}
	else {
		echo '<div class="alert alert-info" role="alert">You have no favorites yet.</div>';
	}
	return ob_get_clean();
}

// redirect logged in user from login page
add_filter('login_redirect', 'ub_login_redirect', 10, 3);
function ub_login_redirect($redirect_to, $request, $user) {
	global $wpdb;
	$redirect_to = '/latest';
	if ($user instanceof WP_User && ! $user->suggested_users_seen) {
		$featured = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->usermeta} WHERE meta_key='wp_capabilities' AND meta_value LIKE '%featured%'");
		if ($featured) {
			$redirect_to = '/new';
		}
	}
	return $redirect_to;
}

// add_filter('usp_post_data', 'ub_usp_mod'); // changes to "User Submitted Posts" plugin
// function ub_usp_mod($postData) {
// 	// Edit post
// 	$post = isset($_POST['post_id']) ? get_post($_POST['post_id']) : null;
// 	if ($post && $post->post_author == get_current_user_id()) {
// 		$postData['ID'] = intval($_POST['post_id']);

// 		// Delete old image if new has been provided
// 		if (isset($_FILES['user-submitted-image']) && $_FILES['user-submitted-image']['error'][0] == UPLOAD_ERR_OK) {
// 			$media = get_attached_media('image', $post->ID);
// 			foreach ($media as $k => $v) {
// 				wp_delete_attachment($k);
// 			}
// 		}
// 	}
// 	return $postData;
// }

// handle custom fields
add_action('wp_insert_post', 'ub_custom_fields', 10, 3);
function ub_custom_fields($post_ID, $post, $update) {
	global $wpdb;
	// in admin panel the Advanced Custom Fields manages the fields
	if (is_admin() && isset($_POST['acf_nonce'])) return false;
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
	if (wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce')) return;

	if ('post' == $post->post_type || 'status' == $post->post_type) {
		$startDate = isset($_POST['date_start']) ? strtotime($_POST['date_start']) : 0;
		$endDate = isset($_POST['date_end']) ? strtotime($_POST['date_end']) : 0;

		// every post must have date fields for sorting
		update_post_meta($post_ID, 'date', $startDate);
		update_post_meta($post_ID, 'date_end', $endDate);

		// $dates = empty($_POST['dates'])
		// 	? json_encode(array(array('start' => $startDate, 'end' => $endDate, 'rrule' => '')))
		// 	: $_POST['dates'];
		// update_post_meta($post_ID, 'dates', $dates);

		// delete future dates and recount
		// $sql = "DELETE FROM {$wpdb->prefix}dates WHERE post_id = {$post_ID} AND start > NOW()";
		// $wpdb->query($sql);

		// $sql = "INSERT INTO {$wpdb->prefix}dates (post_id, start, end) VALUES (%d, %s, %s)";
		// $sql = $wpdb->prepare($sql, $post_ID, date('Y-m-d H:i', $startDate), date('Y-m-d H:i', $endDate));
		// $wpdb->query($sql);
	}

	$yt_pattern = '#^https?://(?:www\.)?(?:youtube\.com/watch|youtu\.be/)#';
	if (isset($_POST['youtube']) && preg_match($yt_pattern, $_POST['youtube'])) {
		update_post_meta($post_ID, 'youtube', esc_html($_POST['youtube']));
	}
	elseif ($update) {
		delete_post_meta($post_ID, 'youtube');
	}
	
	$info_url = empty($_POST['info_url']) ? '' : $_POST['info_url'];
	// add "http://" if missed
	if ($info_url && ! preg_match('/^https?:\/\//', $info_url)) {
		$info_url = 'http://' . $info_url;
	}

	if ($update && ! filter_var($info_url, FILTER_VALIDATE_URL)) {
		delete_post_meta($post_ID, 'info_url');
	}
	elseif (filter_var($info_url, FILTER_VALIDATE_URL)) {
		update_post_meta($post_ID, 'info_url', esc_html($info_url));
	}


	if ( ! empty($_POST['location']) && is_numeric($_POST['latitude']) && is_numeric($_POST['longitude'])) {
		$location = array(
			'address' => esc_html($_POST['location']),
			'lat' => esc_html($_POST['latitude']),
			'lng' => esc_html($_POST['longitude'])
		);
		update_post_meta($post_ID, 'location', $location);
	}
	elseif ($update) {
		delete_post_meta($post_ID, 'location');
		wp_set_object_terms($post_ID, null, 'city');
	}
}

// assign "city" taxonomy
add_action('added_post_meta', 'ub_set_post_city', 10, 4);
add_action('updated_post_meta', 'ub_set_post_city', 10, 4);
function ub_set_post_city($meta_id, $object_id, $meta_key, $meta_value) {
	if ('location' != $meta_key) return;

	$addr = ub_reverce_geocode($meta_value['lat'], $meta_value['lng']);
	$tags = tax_city_id($addr['country'], $addr['city']);
	wp_set_post_terms($object_id, $tags, 'city');
}

// modify Advaced Custom Field values to timestamp
add_filter('acf/update_value/name=date', 'acf_filter_date', 10, 3);
add_filter('acf/update_value/name=date_end', 'acf_filter_date', 10, 3);
function acf_filter_date($value, $post_id, $field) {
	$date = ctype_digit($value) ? $value : strtotime($value);
	return $date;
}

// format date for Advanced Custom Fields
add_filter('acf/format_value', 'acf_format_date', 10, 3);
function acf_format_date($value, $post_id, $field) {
	if ($value && ('date_end' == $field['name'] || 'date' == $field['name'])) {
		$value = date('m/d/Y H:i', $value);
	}
	return $value;
}

// check dates in frontend form
add_filter('wpuf_add_post_validate', 'wpuf_validate_dates');
add_filter('wpuf_update_post_validate', 'wpuf_validate_dates');
function wpuf_validate_dates() {
	// handle only Add Listing form (ID=6900)
	if (empty($_POST['form_id']) || 6900 != $_POST['form_id']) return;

	$date_start =  strtotime($_POST['date_start']);
	$date_end = strtotime($_POST['date_end']);
	$max_range = 8035200; // 93 days

	if ( ! $date_start) {
		return 'Invalid start date';
	}

	if ( ! $date_end || $date_end < $date_start || $date_end > ($date_start + $max_range)) {
		return 'Invalid end date';
	}
}

// check height of the image uploaded via WPUF
add_action('admin_init', 'wpuf_check_image');
function wpuf_check_image() {
	if (empty($_POST['action']) || $_POST['action'] != 'wpuf_file_upload') return;
	if ($_POST['form_id'] != 6900) return;
	if (empty($_FILES['wpuf_file']) || $_FILES['wpuf_file']['error']) return;

	$size = getimagesize($_FILES['wpuf_file']['tmp_name']);
	if ($size[0] < 250 || $size[1] < 250) {
		wp_send_json(array('err' => 1, 'msg' => 'Min dimension is 250x250px'));
	}
}

/* START MODIFYING "ADD STATUS" FORM */
// check does user allowed to submit status
add_filter('wpuf_add_post_validate', 'wpuf_validate_status');
function wpuf_validate_status() {
	// handle only Add Status form (ID=18901)
	if (empty($_POST['form_id']) || 18901 != $_POST['form_id']) return;

	if ( ! can_submit_status()) return 'Sorry, you cannot submit status yet.';
}

function can_submit_status() {
	// admin can always submit
	if (is_super_admin()) return true;

	$user = wp_get_current_user();
	$name_set = get_user_meta($user->ID, 'first_name', true) && get_user_meta($user->ID, 'last_name', true);
	$avatar_set = get_user_meta($user->ID, 'wp_user_avatar', true);
	$have_posts = $user->ID && count_user_posts($user->ID) >= get_option('min_posts');

	if ($have_posts && $name_set && $avatar_set) return true;

	return false;
}

add_filter('pre_do_shortcode_tag', 'hide_submit_status_form', 10, 4);
function hide_submit_status_form($return, $tag, $attr, $m) {
	// is it the "Add Status" shortcode?
	if ('wpuf_form' != $tag || empty($attr['id']) || 18901 != $attr['id']) return false;

	if (can_submit_status()) return false;

	$error = '<div class="alert alert-warning">'
		. '<h4>Unfortunately, you can not share a status update yet.</h4>'
		. sprintf('<p>Please fill out your profile & submit at least %d activities & status updates will become available.</p>', get_option('min_posts'))
		. '</div>';
	return $error;
}
/* END */


// register hierarchical taxonomy and return ID's
function tax_city_id($country, $city) {
	$country_term = get_term_by('name', $country, 'city', ARRAY_A);

	if ( ! $country_term) {
		$country_term = wp_insert_term($country, 'city');
	}

	if ( ! is_array($country_term)) return false;

	if ($city) {
		$args = ['taxonomy' => 'city', 'hide_empty' => 0, 'name' => $city, 'child_of' => $country_term['term_id']];
		$terms = get_terms($args);
		$city_term = (array) array_shift($terms);
		if ( ! $city_term) {
			$city_term = wp_insert_term($city, 'city', array('parent' => $country_term['term_id']));
		}
	}
	else {
		$city_term = false;
	}

	return array($country_term['term_id'], $city_term['term_id']);
}

add_action('init', 'ub_register_status');
function ub_register_status() {
	register_post_type('status',
		array(
			'labels' => array( 'name' => __( 'Statuses' ), 'singular_name' => __( 'Status' )),
			'public' => true,
			'has_archive' => true,
			'menu_position' => 5,
			'show_in_rest' => true,
			'supports' => array('title', 'editor', 'author', 'thumbnail', 'comments'),
		)
	);

	register_post_status('user_deleted', array(
		'label' => 'User deleted',
		'public' => false,
		'exclude_from_search' => true,
		'show_in_admin_status_list' => true,
		'show_in_admin_all_list' => true,
		'internal' => false,
		'label_count' => _n_noop( 'User deleted <span class="count">(%s)</span>', 'User deleted <span class="count">(%s)</span>' ),
	));

	register_post_status('featured', array(
		'label' => 'Featured',
		'public' => true,
		'label_count' => _n_noop( 'Featured <span class="count">(%s)</span>', 'Featured <span class="count">(%s)</span>' ),
	));

	register_post_status('spam', array(
		'label' => 'Spam',
		'public' => false,
		'label_count' => _n_noop( 'Spam <span class="count">(%s)</span>', 'Spam <span class="count">(%s)</span>' ),
	));

	$args = array(
		'label' => 'Cities',
		'hierarchical' => true,
		'rewrite' => array('hierarchical' => true, 'slug' => 'place'),
	);
	register_taxonomy('city', 'post', $args);
}

// posts of the authors member subscribed to
add_shortcode('ub_favorite_author_posts', 'ub_favorite_author_posts');
function ub_favorite_author_posts($attr) {
	global $wpdb, $paged;
	$user_id = get_current_user_id();
	$sql = $wpdb->prepare( "SELECT author_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE subscriber_id = %d AND status = 'active'", $user_id);
	$authors = $user_id ? $wpdb->get_col($sql) : array();
	$posts_per_page = isset($attr['posts_per_page']) ? $attr['posts_per_page'] : get_option('posts_per_page');

	ob_start();

	if ($authors) {
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;

		$args = array(
			'author__in' => $authors,
			'post_type' => array('post', 'status'),
			'posts_per_page' => $posts_per_page,
			'paged' => $paged,
			'meta_query' => array(),
		);

		if (isset($attr['time']) && 'future' == $attr['time']) {
			$args['meta_query'][] = array(
				'key' => 'date_end',
				'value' => current_time('timestamp'),
				'type' => 'NUMERIC',
				'compare' => '>',
			);
		}
		elseif (isset($attr['time']) && 'past' == $attr['time']) {
			$args['meta_query'][] = array(
				'key' => 'date_end',
				'value' => current_time('timestamp'),
				'type' => 'NUMERIC',
				'compare' => '<',
			);
		}

		// order by
		if (isset($_GET['ord']) && 'date_start_asc' == $_GET['ord']) {
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = 'date';
			$args['order'] = 'ASC';
			$args['post_type'] = 'post';
		}
		elseif (isset($_GET['ord']) && 'date_end_asc' == $_GET['ord']) {
			$args['orderby'] = 'meta_value_num';
			$args['meta_key'] = 'date_end';
			$args['order'] = 'ASC';
			$args['post_type'] = 'post';
		}

		$q = new WP_Query($args);

		if ($q->have_posts()) {
			while ($q->have_posts()) {
				$q->the_post();
				get_template_part('content', 'favourite-author-posts');
			}

			echo '<span class="pager next">' . get_next_posts_link('Next', $q->max_num_pages) . '</span>';

			wp_reset_postdata();
		}
		else {
			echo 'Sorry, no posts found from your favourite authors.';
		}
	}
	else {
		echo '<div class="alert alert-info" role="alert">You are not following anyone. Follow some <a href="/users">users</a> to see updates.</div>';
	}

	return ob_get_clean();
}

// add statuses on author archive page
add_action('pre_get_posts', 'ub_custom_post_author_archive');
function ub_custom_post_author_archive(&$query) {
	if ($query->is_author) {
		$query->set('post_type', array('post', 'status'));
	}
    remove_action('pre_get_posts', __FUNCTION__); // run once!
}

// exclude pages from search
add_action('pre_get_posts', 'ub_search_events');
function ub_search_events($query) {
	if ($query->is_search) {
		$query->set('post_type', array('post', 'status'));
	}
}

add_action('pre_get_posts', 'ub_order_posts');
function ub_order_posts($query) {
	if ( ! $query->is_archive) return;

	$order = isset($_GET['ord']) ? $_GET['ord'] : null;
	if ($order && in_array($order, ['date_start_asc', 'date_end_asc'])) {

		if ('date_start_asc' == $order) {
			$query->set('orderby', 'meta_value_num');
			$query->set('meta_key', 'date');
			$query->set('order', 'ASC');
			$query->set('post_type', 'post');
		}
		elseif ('date_end_asc' == $order) {
			$query->set('orderby', 'meta_value_num');
			$query->set('meta_key', 'date_end');
			$query->set('order', 'ASC');
			$query->set('post_type', 'post');
		}
	}
}

add_action('init', 'ub_author_endpoints');
function ub_author_endpoints(){
    add_rewrite_endpoint('following', EP_AUTHORS);
    add_rewrite_endpoint('followers', EP_AUTHORS);
}

function ub_favorite($post_id, $like = null) {
	$class = 'ub-fav btn btn-default';
	$like = null === $like ? do_i_like($post_id) : $like;
	$post = get_post($post_id);
	$cnt = empty($post->_like) ? '' : intval($post->_like);

	if ($like) {
		$title = "Unlike";
		$class .= ' active';
		$action = "remove";
		$icon = '<i class="fa fa-thumbs-up"></i>';
		$html = sprintf('<button type="button" title="%s" class="%s" data-id="%d" data-action="%s">%s %s</button>', $title, $class, $post_id, $action, $icon, $cnt);
	}
	else {
		$title = "Like";
		$action = "add";
		$icon = '<i class="fa fa-thumbs-o-up"></i>';
		$html = sprintf('<button type="button" title="%s" class="%s" data-id="%d" data-action="%s">%s</button>', $title, $class, $post_id, $action, $icon);
	}

	return $html;
}

// hook the WP Favorite Posts plugin response
// add_action('wpfp_after_add', 'ub_after_fav', 100);
// add_action('wpfp_after_remove', 'ub_after_fav', 100);
// function ub_after_fav($post_id) {
// 	$out = array('btn' => ub_favorite($post_id));
// 	wp_send_json($out);
// }

function ub_count_post($user_id) {
	global $wpdb;
	$sql = "SELECT COUNT(*) FROM {$wpdb->posts} WHERE post_author = $user_id AND post_type IN('post', 'status') AND post_status IN('publish', 'featured') ";
	return $wpdb->get_var($sql);
}

// remove slug for "status" post type
add_filter('post_type_link', 'ub_remove_slug', 10, 3);
function ub_remove_slug($post_link, $post, $leavename) {
    if ('status' != $post->post_type || 'publish' != $post->post_status) {
        return $post_link;
    }

    $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);

    return $post_link;
}

add_action('pre_get_posts', 'ub_parse_request');
function ub_parse_request( $query ) {
    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    if ( ! empty( $query->query['name'] ) ) {
        $query->set('post_type', array('post', 'status', 'page'));
    }
}

add_action('wp_footer', 'ub_cookie_law_info');
function ub_cookie_law_info() {
	if (empty($_COOKIE['cookieOK']) && ! is_user_logged_in()) {
		echo '<div id="cookie-law">This website uses cookies to improve your experience. ';
		echo 'We&apos;ll assume you&apos;re ok with this, but you can opt-out if you wish. ';
		echo '<button type="button" id="btnCookieOK" class="btn btn-default btn-xs">Accept</button> ';
		echo '<a href="/privacy/" class="btn btn-default btn-xs">Read More</a></div>';
	}
}

add_action('wp_footer', 'ub_google_analytics');
add_action('login_footer', 'ub_google_analytics');
function ub_google_analytics() {
	if (defined('WP_DEBUG') && WP_DEBUG) return;
	include ABSPATH . 'google-analytics.php';
}

add_action('wp_head', 'smart_banner');
function smart_banner() {
	printf('<meta name="apple-itunes-app" content="app-id=1048911248">'.PHP_EOL);
}

// google searchbox
add_action('wp_head', 'schema_search');
function schema_search() {
	if ( ! is_home()) return;

	$descr = 'Discover new &amp; unusual fitness activities you\'ll love from service providers around the world including football, tennis, golf, running &amp; more.';

	printf('<meta name="Description" content="%s">', $descr);

	echo '<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "WebSite",
		"url": "'. site_url() .'",
		"description": "'.$descr.'",
		"potentialAction": {
			"@type": "SearchAction",
			"target": "'. site_url() .'?s={search_term_string}",
			"query-input": "required name=search_term_string"
		}
	}
	</script>';
	echo PHP_EOL;
}

add_action('wp_head', 'ub_open_graph');
function ub_open_graph() {
	global $post;
	if ( ! is_singular(array('post', 'status'))) return false;

	$excerpt = wp_kses_post( wp_trim_words( $post->post_content, 20 ) );
	$image = get_the_post_thumbnail_url(null, 'full');

	echo '<meta property="og:url" content="'.get_permalink().'" />' . PHP_EOL;
	echo '<meta property="og:type" content="article" />' . PHP_EOL;
	echo '<meta property="og:title" content="'.get_the_title().'" />' . PHP_EOL;
	echo '<meta property="og:description" content="'.$excerpt.'" />' . PHP_EOL;
	echo '<meta property="og:image" content="'.$image.'" />' . PHP_EOL;

	// Structured Data https://developers.google.com/search/docs/guides/intro-structured-data
	// Event requires location, so don't run the code if location is not set
	if ('post' == $post->post_type && $post->location) {
		echo '<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Event",
			"name": "' . get_the_title() . '",
			"description": "' . $excerpt . '",
			"image": "' . $image . '",
			"url": "' . ($post->info_url ? $post->info_url : get_permalink()) . '",
			"startDate": "' . date('c', intval($post->date)) . '",
			"endDate": "' . date('c', intval($post->date_end)) . '",
			"location": {
				"@type": "Place",
				"name": "'. $post->location['address'] .'",
				"address": "'. $post->location['address'] .'",
				"geo": {
					"@type": "GeoCoordinates",
					"latitude": "' . $post->location['lat'] . '",
					"longitude": "' . $post->location['lng'] . '"
				}
			}
		}
		</script>'.PHP_EOL;
	}
}

add_theme_support('admin-bar', array( 'callback' => '__return_false' ));

add_action('wp', 'ub_remove_post');
function ub_remove_post() {
	$user = wp_get_current_user();
	$obj = get_queried_object();
	if (isset($_POST['action']) && 'remove_post' == $_POST['action']
		&& 'post' == $obj->post_type && get_current_user_id() == $obj->post_author) {
		$obj->post_status = 'user_deleted';
		wp_update_post($obj);
		wp_redirect('/');
	}

	if (isset($_POST['action']) && 'report' == $_POST['action']) {
		$item = in_array($_POST['item'], array('user', 'post')) ? $_POST['item'] : 'post';
		$attr = array(
			'user_id' => $user->ID,
			'item_type' => $item,
			'item_id' => $_POST['id'],
			'subject' => $_POST['subject'],
			'message' => $_POST['message'],
			'agent' => $_SERVER['HTTP_USER_AGENT'],
			'date' => date('Y-m-d H:i:s'),
			'ip' => $_SERVER['REMOTE_ADDR'],
		);
		ub_create_complaint($attr);
	}

	if (isset($_POST['action']) && 'suggested_users_seen' == $_POST['action']) {
		update_user_meta($user->ID, 'suggested_users_seen', 1);
	}
}

function ubMakePostFeatured($post) {
	$upd = array(
		'ID' => $post,
		'post_status' => 'featured',
	);
	wp_update_post($upd);
}

// remove_action('init', 'yarpp_init'); // disable embedding YARPP into event content
// add_filter('the_content', 'ub_filter_content', 1000);
// function ub_filter_content($content) {
// 	// disable the YARPP in post description
// 	$content = '<!--noyarpp-->' . $content;
// 	return $content;
// }

// exclude ended events from related list
add_filter('posts_join', 'yarpp_exclude_ended_join', 20);
function yarpp_exclude_ended_join($arg) {
	global $wpdb;
	if (strpos($arg, 'yarpp')) {
		$arg .= " JOIN {$wpdb->postmeta} AS end ON {$wpdb->posts}.ID = end.post_id AND end.meta_key = 'date_end'";
	}
	return $arg;
}
// exclude ended events from related list
add_filter('posts_where', 'yarpp_exclude_ended_where', 20);
function yarpp_exclude_ended_where($arg) {
	global $wpdb;
	if (strpos($arg, 'yarpp')) {
		$arg .= " AND end.meta_value > UNIX_TIMESTAMP()";
	}
	return $arg;
}

add_shortcode('ub_top_users', 'ub_top_users');
function ub_top_users($atts) {
	global $authordata, $wpdb;
	$old_authordata = $authordata;

	$page = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
	$limit = empty($atts['limit']) ? 10 : $atts['limit'];
	$start = ($page-1) * $limit;

	$sql = "SELECT post_author
		FROM {$wpdb->posts} AS p
		JOIN {$wpdb->postmeta} AS pm ON p.ID = pm.post_id AND meta_key = 'views'
		GROUP BY post_author
		ORDER BY SUM(meta_value) DESC
		LIMIT $start,$limit";
	$ids = $wpdb->get_col($sql);
	$ids = $ids ? $ids : array(0);

	$args = array(
		'include' => $ids,
		'orderby' => 'include',
//		'number' => (isset($atts['number']) ? $atts['number'] : 10),
	);
	$allUsers = get_users($args);

	ob_start();
	foreach($allUsers as $user)
	{
		$authordata = $user;
		include(get_stylesheet_directory() . '/content-user.php');
	}

	if ($allUsers) {
		echo '<span class="pager next"><a href="'.get_permalink().'/page/'.($page+1).'">Next</a></span>';
	}

	$authordata = $old_authordata;
	return ob_get_clean();
}

function ub_user_following($atts = array()) {
	global $wpdb, $wp;
	global $authordata;

	$user_id = isset($atts['user_id']) ? $atts['user_id'] : $authordata->ID;
	$old_authordata = $authordata;

	$page = empty($_GET['page']) ? 1 : intval($_GET['page']);
	$limit = isset($atts['number']) ? $atts['number'] : 10;
	$start = ($page - 1) * $limit;

	$sql = "SELECT author_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE subscriber_id = {$user_id} ORDER BY created_at DESC, id LIMIT $start,$limit";
	$users_id = $wpdb->get_col($sql);

	$total = ub_following_cnt($authordata->ID);

	if ($users_id) {
		ob_start();
		foreach ($users_id as $id) {
			$authordata = $user = get_user_by('id', $id);
			// skip removed or closed accounts
			if ( ! $authordata || in_array('closed', $authordata->roles)) continue;
			include(get_stylesheet_directory() . '/content-user.php');
		}
		$html = ob_get_clean();

		if ($page < ceil($total/$limit)) {
			$url = home_url($wp->request);
			$html .= '<span class="pager next"><a href="' . $url . '?page=' . ($page+1) . '">Next</a></span>';
		}
	}
	else {
		$html = '<div class="alert alert-info">Nothing found.</div>';
	}

	$authordata = $old_authordata;
	return $html;
}

function ub_user_followers($atts = array()) {
	global $wpdb, $wp;
	global $authordata;

	$user_id = isset($atts['user_id']) ? $atts['user_id'] : $authordata->ID;
	$old_authordata = $authordata;

	$page = empty($_GET['page']) ? 1 : intval($_GET['page']);
	$limit = isset($atts['number']) ? $atts['number'] : 10;
	$start = ($page - 1) * $limit;

	$sql = "SELECT subscriber_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE author_id = {$user_id} ORDER BY created_at DESC, id LIMIT $start,$limit";
	$users_id = $wpdb->get_col($sql);

	$total = ub_followers_cnt($authordata->ID);

	if ($users_id) {
		ob_start();
		foreach ($users_id as $id) {
			$authordata = $user = get_user_by('id', $id);
			// skip removed or closed accounts
			if ( ! $authordata || in_array('closed', $authordata->roles)) continue;
			include(get_stylesheet_directory() . '/content-user.php');
		}
		$html = ob_get_clean();

		if ($page < ceil($total/$limit)) {
			$url = home_url($wp->request);
			$html .= '<span class="pager next"><a href="' . $url . '?page=' . ($page+1) . '">Next</a></span>';
		}
	}
	else {
		$html = '<div class="alert alert-info">Nothing found.</div>';
	}

	$authordata = $old_authordata;
	return $html;
}

add_action('wpsa_subscribed', 'ub_notify_author', 10, 2);
function ub_notify_author($author_id, $subscriber_id) {
	$author = get_user_by('ID', $author_id);
	$subscriber = get_user_by('ID', $subscriber_id);

	// author unsubscribed
	if ($author->notifications != '' && empty($author->notifications & 1)) return;

	if ($author->user_email && $subscriber->user_email) {
		$subj = 'New follower!';
		$msg = "Congratulations, you have a new follower!\n\n";
		$msg .= get_author_posts_url($subscriber->ID, $subscriber->user_nicename);

		wp_mail($author->user_email, $subj, $msg);
	}
}

add_shortcode('ub-analytics', 'ub_analytics');
function ub_analytics() {
	$user_id = get_current_user_id();
	$page = get_query_var('paged') ? get_query_var('paged') : 1;
	$per_page = 20;
	$args = array('author' => $user_id, 'posts_per_page' => $per_page, 'paged' => $page);
	$query = new WP_Query( $args );

	ob_start();
	include get_stylesheet_directory() . '/ub-mods/analytics.php';
	wp_reset_postdata();
	return ob_get_clean();
}

add_action('wp_ajax_get_activity', 'ajax_get_activity');
function ajax_get_activity() {
	$out = array();

	$post_id = $_GET['post_id'];
	$post = get_post($post_id);

	if ($post && $post->post_author == get_current_user_id()) {
		$date_start = date('m/d/Y H:i', $post->date);
		$date_end = date('m/d/Y H:i', $post->date_end);
		$out = array(
			'ID' => $post->ID,
			'post_title' => $post->post_title,
			'post_content' => $post->post_content,
			'post_date' => $post->post_date,
			'category' => get_the_category($post_id),
			'tags' => wp_get_post_tags($post_id),
			'date' => $date_start ? $date_start : '',
			'date_end' => $date_end ? $date_end : '',
			'location' => $post->location,
//			'repeat' => $post->repeat,
			'info_url' => $post->info_url,
			'youtube' => $post->youtube,
			'disabled_facilities' => $post->disabled_facilities,
			'food_available' => $post->food_available,
			'hashtag' => $post->hashtag,
		);
	}

	wp_send_json($out);
}

add_shortcode('city-list', 'ub_city_list');
function ub_city_list() {
	global $wp_query;
	$args = array(
		'taxonomy' => 'city',
		'parent' => is_tax('city') ? intval($wp_query->queried_object->term_id) : 0,
	);

	ob_start();
	$terms = get_terms($args);
	if ($terms) {
		echo '<div class="panel panel-default">';
		echo '<div class="panel-body">';
		echo '<div class="row city-list">';
		foreach ($terms as $t) {
			printf ('<div class="col-sm-4"><a href="%s">%s</a> <span>(%d)</span></div>', get_term_link($t), $t->name, $t->count);
		}
		echo '</div>';
		echo '</div></div>';
	}
	return ob_get_clean();
}

/**
 * get coordinates based on address
 *
 * @param string $address
 */
function ub_geocode($address) {
	$url = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . GOOGLE_API_KEY . '&address=' . urlencode($address);
	$json = @file_get_contents($url);
	$data = json_decode($json);


	if($data->status == "OK") {
		$out = array('formatted_address' => $data->results[0]->formatted_address);

 		foreach ($data->results[0]->address_components as $c) {
			if (in_array('locality', $c->types)) {
				$out['city'] = $c->long_name;
			}
			elseif (in_array('country', $c->types)) {
				$out['country'] = $c->long_name;
			}
		}

		$out['lat'] = $data->results[0]->geometry->location->lat;
		$out['lng'] = $data->results[0]->geometry->location->lng;
	}
	else {
		$out = false;
	}

	return $out;
}


function ub_reverce_geocode($lat, $lng) {
	$url = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . GOOGLE_API_KEY . '&latlng='.trim($lat).','.trim($lng);
	$json = @file_get_contents($url);
	$data = json_decode($json);


	if($data->status == "OK") {
		$out = array('formatted_address' => $data->results[0]->formatted_address);

 		foreach ($data->results[0]->address_components as $c) {
			if (in_array('locality', $c->types)) {
				$out['city'] = $c->long_name;
//				$out['city'] = $c->short_name;
			}
			elseif (in_array('country', $c->types)) {
				$out['country'] = $c->long_name;
//				$out['country'] = $c->short_name;
			}
		}
	}
	else {
		$out = false;
	}

	return $out;
}

// save post ID to notification queue
add_action('new_to_publish', 'ub_enqueue_to_notify');
add_action('draft_to_publish', 'ub_enqueue_to_notify');
function ub_enqueue_to_notify($post) {
	$queue = get_option('postsToNotify', array());
	$queue[] = $post->ID;
	update_option('postsToNotify', $queue);
}

// check new posts and notify app users
add_action('ub_cron_hourly', 'update_unread_cnt');
function update_unread_cnt() {
	global $wpdb;
	$queue = get_option('postsToNotify', array());
	$authors = array();

	do_action('before_ub_push', $queue);

	foreach ($queue as $id) {
		$post = get_post($id);
		if ( ! $post) continue;

		$sql = "SELECT subscriber_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE author_id = {$post->post_author}";
		$subscribers = $wpdb->get_col($sql);

		if ( ! $subscribers) continue;

		foreach ($subscribers as $s) {
			$unread = get_user_meta($s, 'unread', true);
			$unread = is_array($unread) ? $unread : array();

			if (count($unread) > 50) continue;

			$unread[] = $post->ID;
			update_user_meta($s, 'unread', $unread);
		}

		// only one notification per author per hour.
		// some importers can create many activities in a seconds, I don't want receive all of them!
		if (in_array($post->post_author, $authors)) continue;
		$authors[] = $post->post_author;

		$sql = "SELECT subscriber_id
			FROM {$wpdb->prefix}wpsa_subscribe_author AS sa
			LEFT JOIN {$wpdb->usermeta} AS um ON subscriber_id = user_id AND meta_key = 'notifications'
			WHERE author_id = {$post->post_author} AND (meta_value IS NULL OR meta_value & 2)";
		$subscribers = $wpdb->get_col($sql);

		if ( ! $subscribers) continue;
		do_action('ub_push', $post, $subscribers);
	}

	update_option('postsToNotify', array());
}

function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit = 'M') {
	$theta = $lon1 - $lon2;

	// deg2rad doesn't like string variables, cast them manually
	$dist = sin(deg2rad($lat1*1)) * sin(deg2rad($lat2*1)) + cos(deg2rad($lat1*1)) * cos(deg2rad($lat2*1)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);

	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);

	if ($unit == "K") {
		return ($miles * 1.609344);
	} else if ($unit == "N") {
		return ($miles * 0.8684);
	} else {
		return $miles;
	}
}

// limit search results for "WP Search Suggest" plugin
add_filter('wpss_search_query_args', 'ub_wpss_mod');
function ub_wpss_mod($args) {
	$args['posts_per_page'] = 8;
	return $args;
}

// cleanup favorites/followers from removed items
add_action('ub_cron_daily', 'ub_clean_removed');
function ub_clean_removed() {
	global $wpdb;

	$sql = "DELETE sa
			FROM {$wpdb->prefix}wpsa_subscribe_author AS sa
			LEFT JOIN {$wpdb->users} AS u ON u.ID = sa.author_id
			WHERE u.ID IS NULL";
	$wpdb->query($sql);

	$sql = "DELETE sa
			FROM {$wpdb->prefix}wpsa_subscribe_author AS sa
			LEFT JOIN {$wpdb->users} AS u ON u.ID = sa.subscriber_id
			WHERE u.ID IS NULL";
	$wpdb->query($sql);
}

add_action('wp_ajax_ub_cal', 'export_calendar');
add_action('wp_ajax_nopriv_ub_cal', 'export_calendar');
function export_calendar() {
	$user = get_user_by('id', $_GET['user_id']);

	if ( ! $user) die('user not exists');

	$args = array('author' => $user->ID, 'posts_per_page' => -1);
	$q = new WP_Query($args);

	switch ($_GET['type']) {
		case 'ics':
			echo export_calendar_ics($q);
			break;
		default:
			echo export_calendar_csv($q);
			break;
	}
	exit;
}

function export_calendar_csv(WP_Query $query) {
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename="downloaded.csv"');

	$out = fopen('php://output', 'w');
	fputcsv($out, array('title', 'description', 'start', 'end', 'location', 'url'));
	while ($query->have_posts()) {
		$query->the_post();
		$addr = $query->post->location ? $query->post->location['address'] : '';
		$post = array(
			$query->post->post_title,
			$query->post->post_content,
			$query->post->date,
			$query->post->date_end,
			$addr,
			get_permalink(),
		);
		fputcsv($out, $post);
	}
	fclose($out);
}

function export_calendar_ics(WP_Query $query) {
	header('Content-type: text/calendar; charset=utf-8');
	header('Content-Disposition: attachment; filename=downloaded.ics');

	echo "BEGIN:VCALENDAR" . PHP_EOL;
	echo "VERSION:2.0" . PHP_EOL;
	echo "PRODID:-//ub.fitness//NONSGML v1.0//EN" . PHP_EOL;
	echo "CALSCALE:GREGORIAN" . PHP_EOL;

	while ($query->have_posts()) {
		$query->the_post();
		echo "BEGIN:VEVENT" . PHP_EOL;
		echo "DTSTART:" . ics_date_format($query->post->date) . PHP_EOL;
		if ($query->post->date_end) {
			echo "DTEND:" . ics_date_format($query->post->date_end) . PHP_EOL;
		}
		echo "DTSTAMP:" . ics_date_format($query->post->post_date_gmt)  . PHP_EOL;
		echo "UID:" . uniqid() .PHP_EOL;
		if ( ! empty($query->post->location)) {
			echo "LOCATION:" . ics_escape_str($query->post->location['address']) . PHP_EOL;
		}
		echo "DESCRIPTION:" . ics_escape_str($query->post->post_content) . PHP_EOL;
//		echo "URL;VALUE=URI:" . esc_url(get_permalink()) . PHP_EOL;
		echo "URL:" . esc_url(get_permalink()) . PHP_EOL;
		echo "SUMMARY:" . ics_escape_str($query->post->post_title) . PHP_EOL;
		echo "END:VEVENT" . PHP_EOL;
	}
	echo "END:VCALENDAR" . PHP_EOL;
}

function ics_escape_str($string) {
	$string = preg_replace('/\R+/', '\n', $string);
	return preg_replace('/([\,;])/','\\\$1', $string);
}

function ics_date_format($date) {
	$time = ctype_digit($date) ? $date : strtotime($date);
	return date('Ymd\THis\Z', $time);
}

// Wp Subscribe Author cannot unsubscribe, fix
add_action('init', 'wpsa_unsubscribe_fix');
function wpsa_unsubscribe_fix() {
	global $wpdb;
	if (isset($_GET['wpsa_unsubscribe']) && isset($_GET['author'])) {
		$sql = $wpdb->prepare("SELECT * FROM {$wpdb->users} WHERE md5(user_email) = %s", $_GET['wpsa_unsubscribe']);
		$user = $wpdb->get_row($sql);

		if ($user) {
			$sql = $wpdb->prepare("DELETE FROM {$wpdb->prefix}wpsa_subscribe_author WHERE author_id = %d AND subscriber_id = %d", $_GET['author'], $user->ID);
			$wpdb->query($sql);
		}
	}
}

add_shortcode('order-btn', 'order_btn');
function order_btn() {
	$opt = array(
		'date_pub_asc' => 'Published date',
		'date_start_asc' => 'Start date',
		'date_end_asc' => 'End date',
	);
	$out = '<form class="form-inline" method="get" action="">';
	$out .= '<div id="orderby" class="form-group"><div class="input-group"><div class="input-group-addon">Order by</div>';
	$out .= '<select name="ord" class="form-control" onchange="this.form.submit()">';
	foreach ($opt as $k => $v) {
		$selected = isset($_GET['ord']) && $k == $_GET['ord'] ? 'selected' : '';
		$out .= "<option value=\"$k\" $selected>$v</option>";
	}
	$out .= '</select></div></div>';
	$out .= '</form>';
	return $out;
}

// START REMOVING AUTHOR BASE
add_action('init', 'ub_author_trick');
function ub_author_trick() {
	preg_match('/^\/([^\/?]+)/', $_SERVER['REQUEST_URI'], $matches);

	if ( ! empty($matches[1])) {
		$user = get_user_by('slug', $matches[1]);

		if ( ! empty( $user ) && $user->ID) {
			$_SERVER['REQUEST_URI'] = '/author' . $_SERVER['REQUEST_URI'];
		}
	}
}

add_filter('author_link', 'no_author_base', 1000, 2);
function no_author_base($link, $author_id) {
    $link_base = trailingslashit(get_option('home'));
    $link = preg_replace("|^{$link_base}author/|", '', $link);
    return $link_base . $link;
}

add_filter('redirect_canonical', 'author_redirect_canonical');
function author_redirect_canonical($redirect_url) {
	return strpos($redirect_url, '/author/') ? str_replace('/author/', '/', $redirect_url) : $redirect_url;
}
// END REMOVING AUTHOR BASE

// START RECEENTLY VIEWED
add_action('wp', 'save_recently_viewed');
function save_recently_viewed() {
	global $post;
	if (is_singular('post')) {
		$arr = isset($_COOKIE['posts_viewed']) ? json_decode($_COOKIE['posts_viewed']) : array();
		array_unshift($arr, $post->ID);
		$arr = array_unique($arr);
		$arr = array_slice($arr, 0, 3);
		setcookie('posts_viewed', json_encode($arr, JSON_NUMERIC_CHECK), time()+7*86400, '/');
	}
}

function show_recently_viewed() {
	$arr = isset($_COOKIE['posts_viewed']) ? json_decode($_COOKIE['posts_viewed']) : false;
	ob_start();
	if ($arr && is_array($arr)) {
		$qry = array('post__in' => $arr, 'orderby' => 'post__in');
		$q = new WP_Query( $qry );

		while ( $q->have_posts() ) {
			$q->the_post();
			get_template_part('content', 'favourite-author-posts');
		}
		wp_reset_postdata();
	}
	return ob_get_clean();
}

function have_recently_viewed() {
	return isset($_COOKIE['posts_viewed']);
}
// END RECEENTLY VIEWED

// remove attached media files
add_action('before_delete_post', 'ub_delete_post');
function ub_delete_post($post_id) {
	global $post_type;

	if ('post' == $post_type) {
		$media = get_attached_media('image', $post_id);
		foreach ($media as $m) {
			wp_delete_attachment($m->ID, true);
		}
	}
}

// default post thumbnail
add_filter('get_post_metadata', 'ub_default_post_thumbnail', 10, 4);
function ub_default_post_thumbnail($value, $object_id, $meta_key, $single) {
	$default_image_id = get_option('default_thumb_id');
	if ('post' != get_post_type() || '_thumbnail_id' != $meta_key) return null;

	$meta_type = 'post';
	$meta_cache = wp_cache_get($object_id, $meta_type . '_meta');

	if ( !$meta_cache ) {
		$meta_cache = update_meta_cache( $meta_type, array( $object_id ) );
		$meta_cache = $meta_cache[$object_id];
	}

	// post HAS thumbnail
	if ( isset($meta_cache[$meta_key]) ) {
		if ( $single )
			return maybe_unserialize( $meta_cache[$meta_key][0] );
		else
			return array_map('maybe_unserialize', $meta_cache[$meta_key]);
	}

	if ($single)
		return $default_image_id;
	else
		return array($default_image_id);
}

function pluralize( $count, $text )
{
	return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
}

function ago( $datetime )
{
	if (empty($datetime)) return;
	if (ctype_digit($datetime) || 'DateTime' != get_class($datetime)) {
		$time = $datetime;
		$datetime = new DateTime();
		$datetime->setTimestamp($time);
	}

	$interval = date_create('now')->diff( $datetime );
	$suffix = ( $interval->invert ? ' ago' : '' );
	if ( $v = $interval->y >= 1 ) return pluralize( $interval->y, 'year' ) . $suffix;
	if ( $v = $interval->m >= 1 ) return pluralize( $interval->m, 'month' ) . $suffix;
	if ( $v = $interval->d >= 1 ) return pluralize( $interval->d, 'day' ) . $suffix;
	if ( $v = $interval->h >= 1 ) return pluralize( $interval->h, 'hour' ) . $suffix;
	if ( $v = $interval->i >= 1 ) return pluralize( $interval->i, 'minute' ) . $suffix;
	return pluralize( $interval->s, 'second' ) . $suffix;
}

// handle custom YouTube field
// add_filter('the_content', 'ub_youtube_field', 6);
// function ub_youtube_field($content) {
// 	global $post;
// 	if ('post' == $post->post_type && $post->youtube) {
// 		$content .= PHP_EOL . $post->youtube;
// 	}
// 	return $content;
// }

function youtube_id($str) {
	$regexp = '#((?:www\.)?(?:youtube\.com\/(?:watch\?v=|embed\/|v\/)|youtu\.be\/|youtube\-nocookie\.com\/embed\/)([a-zA-Z0-9-]*))#i';
	preg_match($regexp, $str, $matches);
	return isset($matches[2]) ? $matches[2] : '';
}

add_shortcode('happening_now', 'happening_now');
function happening_now() {
	$now = current_time('timestamp');

	$page = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
	$args = array(
		'ignore_sticky_posts' => true,
		'paged' => $page,
		'orderby' => 'meta_value_num',
		'meta_key' => 'views',
		'meta_query' => array(
			array('key' => 'date', 'compare' => '<', 'value' => $now, 'type' => 'NUMERIC'),
			array('key' => 'date_end', 'compare' => '>', 'value' => $now, 'type' => 'NUMERIC'),
			array('key' => '_not_hot', 'compare' => 'NOT EXISTS', 'value' => 'any'),
		),
	);

	$q = new WP_Query($args);

	ob_start();
	echo '<div class="row">';
	while ($q->have_posts()) {
		$q->the_post();
		echo '<div class="col-md-4">';
		get_template_part('content', 'post');
		echo '</div>';
	}
	echo '</div>';
	echo '<span class="pager next">' . get_next_posts_link('Next', $q ->max_num_pages) . '</span>';

	wp_reset_postdata();
	return ob_get_clean();
}

function get_event_timezone($post = 0) {
	$post = get_post($post);

	if (empty($post->_tz) && !empty($post->date) && !empty($post->location['lat'])) {
		$url = 'https://maps.googleapis.com/maps/api/timezone/json?key=' . GOOGLE_API_KEY
			. '&timestamp=' . $post->date
			. '&location=' . $post->location['lat'] . ',' . $post->location['lng'];
		$resp = wp_remote_get($url);
		$body = is_array($resp) ? json_decode($resp['body']) : false;

		if (is_wp_error($resp)) {
			trigger_error($resp->get_error_message(), E_USER_WARNING);
			return;
		}
		elseif (is_array($resp) && $body->status != 'OK') {
			trigger_error('GOOGLE: ' . $body->errorMessage, E_USER_WARNING);
			return;
		}
		elseif (isset($body->timeZoneId)) {
			update_post_meta($post->ID, '_tz', $body->timeZoneId);
		}
	}

	return $post->_tz;
}

function related_activities($post = null, $atts = []) {
	global $post;
	$post = get_post($post);
	$cats = get_the_terms($post, 'category');

	$dflt = [
		'cat' => $cats[0]->term_id,
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'meta_query' => [
			['key' => 'date_end', 'value' => current_time('timestamp'), 'type' => 'NUMERIC', 'compare' => '>']
		]
	];

	// exclude current post on details page
	if (is_singular('post')) {
		$dflt['post__not_in'] = [$post->ID];
	}

	ob_start();

	$opt = array_merge($dflt, $atts);
	$q = new WP_Query($opt);

	// search by tags if no active posts in category
	if ( ! $q->have_posts()) {
		$opt['tag__in'] = wp_get_post_terms($post->ID, "post_tag", array("fields" => "ids"));
		unset($opt['cat']);
		$q = new WP_Query($opt);
	}

	if ($q->have_posts()) {
		echo '<div class="row">';
		while ($q->have_posts()) {
			$q->the_post();
			echo '<div class="col-md-4">';
			get_template_part('content', 'post');
			echo '</div>';
		}
		echo '</div>';
	}
	else {
		echo 'No related posts.';
	}

	wp_reset_postdata();
	return ob_get_clean();
}

// change WP_Query on author feed
// add_action('pre_get_posts', 'filter_author_page');
// function filter_author_page($query) {
// 	if ( ! is_feed() || ! $query->get('name')) return;

// 	$query->set('author_name', $query->get('name'));
// 	$query->set('name', null);
// 	$query->set('post_type', ['post', 'status']);
// }

// Add a new interval of a week
// See http://codex.wordpress.org/Plugin_API/Filter_Reference/cron_schedules
add_filter('cron_schedules', 'ub_add_weekly_cron_schedule');
function ub_add_weekly_cron_schedule( $schedules ) {
    $schedules['weekly'] = array(
        'interval' => 604800, // 1 week in seconds
        'display'  => __( 'Once Weekly' ),
    );
 
    return $schedules;
}

// block email to some recipients
add_filter('wp_mail', 'ub_discard_recipient');
function ub_discard_recipient($atts) {
	// do not send emails to facebook.com and ub.fitness
	$blocked = '/@(facebook.com|ub.fitness)$/i';
	if (isset($atts['to']) && preg_match($blocked, $atts['to'])) {
		$atts['to'] = '';
	}
	return $atts;
}

add_shortcode('activities_list', 'activities_list');
function activities_list() {
	global $wpdb;
	$out = '';

	// // get categories
	// $sql = "SELECT t.term_id, name
	// FROM {$wpdb->term_taxonomy} AS tt
	// JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id
	// WHERE tt.taxonomy = 'category' AND `count` > 0";
	// $cats = $wpdb->get_results($sql, OBJECT_K);

	// // get tags
	// $sql = "SELECT t.term_id, name
	// FROM {$wpdb->term_taxonomy} AS tt
	// JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id
	// WHERE tt.taxonomy = 'post_tag' AND `count` > 0";
	// $tags = $wpdb->get_results($sql, OBJECT_K);

	// get posts
	$sql = "SELECT p.ID, post_title, post_name, user_login, meta_value
	FROM {$wpdb->posts} AS p
	JOIN {$wpdb->postmeta} AS pm ON p.ID = pm.post_id AND meta_key = 'date'
	JOIN {$wpdb->users} AS u ON p.post_author = u.ID
	WHERE p.post_type = 'post' AND post_status = 'publish'
	ORDER BY pm.meta_value";
	$res = $wpdb->get_results($sql);

	foreach ($res as $p) {
		$sql = "SELECT name
		FROM {$wpdb->term_relationships} AS tr
		JOIN {$wpdb->term_taxonomy} AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id AND taxonomy='category'
		JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id
		WHERE tr.object_id = {$p->ID}";
		$cats = $wpdb->get_col($sql);

		$sql = "SELECT name
		FROM {$wpdb->term_relationships} AS tr
		JOIN {$wpdb->term_taxonomy} AS tt ON tr.term_taxonomy_id = tt.term_taxonomy_id AND taxonomy='post_tag'
		JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id
		WHERE tr.object_id = {$p->ID}";
		$tags = $wpdb->get_col($sql);

		$out .= "<a href=\"/{$p->post_name}\">{$p->post_title}</a><br>\n";
		$out .= date('F j, Y, H:i', $p->meta_value) . "<br>\n";
		$out .= implode(', ', $cats) . ' | ' . implode(', ', $tags) . "<br>\n";
		$out .= $p->user_login . "<br><br>\n\n";
	}

	$out .= "TOTAL: " . count($res);

	return $out;
}

function follow_user_btn() {
	global $wpdb;

	$user = wp_get_current_user();
	$authorID = get_the_author_meta('ID');

	if ( ! $user->ID || $user->ID == $authorID) return;

	$sql = $wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}wpsa_subscribe_author WHERE author_id = %d AND subscriber_id = %d", $authorID, $user->ID);
	$subscribed = $wpdb->get_var($sql);

	if ($subscribed) {
		$html = '<a href="#" class="follow-btn-sm active" data-author="'.$authorID.'" title="Unfollow"></a>';
	}
	else {
		$html = '<a href="#" class="follow-btn-sm" data-author="'.$authorID.'" title="Follow"></a>';
	}

	return $html;
}

add_action('wp_ajax_ub_follow', 'ub_follow_user');
function ub_follow_user() {
	global $wpdb;

	$user = wp_get_current_user();
	$authorID = (int) $_POST['author_id'];
	$data = ['author_id' => $authorID, 'subscriber_id' => $user->ID];
	$out = ['follow' => 0];

	if ( ! $user->ID || $user->ID == $authorID) return;

	// unsubscribe
	$unsubscribed = $wpdb->delete($wpdb->prefix .'wpsa_subscribe_author', $data);

	if ($unsubscribed) {
		do_action('wpsa_unsubscribed', $authorID, $user->ID);
	}
	else { // user didn't follow, follow
		$wpdb->insert($wpdb->prefix .'wpsa_subscribe_author', $data);
		do_action('wpsa_subscribed', $authorID, $user->ID);
		$out['follow'] = 1;
	}

	wp_send_json($out);
}

/* START TRICKS WITH wordpress_logged_in  cookie*/
add_action('set_logged_in_cookie', 'ub_hack_cookie', 10, 5);
function ub_hack_cookie($logged_in_cookie, $expire) {
	if ($expire) return;

	$GLOBALS['logged_in_cookie'] = $logged_in_cookie;
}

// remember user even if he doesn't want
add_action('wp_login', 'keep_me_logged_in');
function keep_me_logged_in() {
	$expire = time() + 2 * WEEK_IN_SECONDS;
	if (isset($GLOBALS['logged_in_cookie'])) {
		setcookie(LOGGED_IN_COOKIE, $GLOBALS['logged_in_cookie'], $expire, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), true);
	}
}

// change default "remember me" from 2 weeks to a year
add_filter('auth_cookie_expiration', 'remember_me_for_year');
function remember_me_for_year( $expirein ) {
	return YEAR_IN_SECONDS;
}
/* END */

// detect is user using mobile app or browser
function ub_client($user_agent = '') {
	if ($user_agent == '') $user_agent = $_SERVER['HTTP_USER_AGENT'];
	$client = 'web';

	if (false !== stripos($user_agent, 'ubfitness')) {
		$client = stripos($user_agent, 'android') ? 'android' : 'ios';
	}
	return $client;
}

add_action('wp_ajax_all_tags', 'ajax_all_tags');
add_action('wp_ajax_nopriv_all_tags', 'ajax_all_tags');
function ajax_all_tags() {
	global $wpdb;
	$sql = "SELECT name
		FROM {$wpdb->term_taxonomy} AS tt
		JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id
		WHERE taxonomy = 'post_tag'";
	$result = $wpdb->get_col($sql);
	$result = array_map('strtolower', $result);
	wp_send_json($result);
}

// add_action('wp_ajax_wpfp_add_activity_form', 'wpfp_add_activity_form');
// function wpfp_add_activity_form() {
// 	echo do_shortcode('[wpuf_form id="6900"]');
// 	wp_die();
// }

// replace right/left quotation marks with straight
add_filter('wp_insert_post_data', 'ub_remove_quotes');
function ub_remove_quotes($data) {
	$data['post_content'] = str_replace(array('“', '”'), '"', $data['post_content']);
	return $data;
}

// create start dates if rrule is set
// add_action('wp_insert_post', 'process_rrule', 20, 3);
// function process_rrule($post_ID, $post, $update) {
// 	global $wpdb;

// 	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
// 	if (wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce')) return;

// 	$dates = json_decode(get_post_meta($post_ID, 'dates', true));
// 	if (empty($dates)) return;

// 	include_once get_theme_file_path('inc/php-rrule/RfcParser.php');
// 	include_once get_theme_file_path('inc/php-rrule/RRuleInterface.php');
// 	include_once get_theme_file_path('inc/php-rrule/RRule.php');

// 	// delete future dates and recount
// 	$sql = "DELETE FROM {$wpdb->prefix}dates WHERE post_id = {$post_ID} AND start > NOW()";
// 	$wpdb->query($sql);

// 	foreach ($dates as $date) {
// 		if (empty($date->rrule)) {
// 			$sql = "INSERT INTO {$wpdb->prefix}dates (post_id, start, end) VALUES (%d, %s, %s)";
// 			$sql = $wpdb->prepare($sql, $post_ID, date('Y-m-d H:i', $date->start), date('Y-m-d H:i', $date->end));
// 			$wpdb->query($sql);
// 		}
// 		else {
// 			try {
// 				$rrule = new RRule\RRule($date->rrule, intval($date->start));
// 				$duration = $date->end - $date->start;

// 				$sql_start = "INSERT IGNORE {$wpdb->prefix}dates (post_id, start, end) VALUES ";
// 				$sql_end = '';

// 				foreach ($rrule as $i => $r) {
// 					if ($i > 30) break;
// 					$end = date('Y-m-d H:i', $r->getTimestamp() + $duration); // end date
// 					$sql_end .= sprintf("(%d, '%s', '%s'),", $post_ID, $r->format('Y-m-d H:i'), $end);
// 				}

// 				if ($sql_end) {
// 					$sql_end = substr($sql_end, 0, -1);
// 					$wpdb->query($sql_start . $sql_end);
// 				}
// 			}
// 			catch (Exception $e) {
// 				error_log($e->getMessage());
// 			}
// 		}
// 	}
// }

// function next_date($post) {
// 	global $wpdb;
// 	$post = get_post($post);

// 	if (false === $next = wp_cache_get($post->ID, 'date')) {
// 		$now = time();

// 		$sql1 = "SELECT * FROM {$wpdb->prefix}dates WHERE post_id = {$post->ID} AND start > NOW() ORDER BY start LIMIT 1";
// 		$sql2 = "SELECT * FROM {$wpdb->prefix}dates WHERE post_id = {$post->ID} ORDER BY start DESC LIMIT 1";
// 		$sql = "($sql1) UNION ($sql2) LIMIT 1";
// 		$date = $wpdb->get_row($sql, ARRAY_A);

// 		$next = empty($date) ? array('start' => 0, 'end' => 0) : $date;
// 		wp_cache_set($post->ID, $next, 'date', 5 * HOUR_IN_SECONDS);
// 	}

// 	return $next;
// }

// get activity's next start date
function next_start_date($post, $format = '') {
	$post = get_post($post);

	$next = get_post_meta($post->ID, 'date', true);
	// if (false === $next = wp_cache_get($post->ID, 'next_start')) {
	// 	$now = time();
	// 	$dates = get_post_meta($post->ID, 'date');
	// 	asort($dates);

	// 	foreach ($dates as $d) {
	// 		if ($d > $now) break;
	// 	}
	// 	$next = isset($d) ? intval($d) : 0;
	// 	wp_cache_set($post->ID, $next, 'next_start', HOUR_IN_SECONDS);
	// }

	return $format ? date($format, $next) : intval($next);
}

function next_end_date($post, $format = '') {
	$post = get_post($post);

	$end = get_post_meta($post->ID, 'date_end', true);
	// $start = next_start_date($post);
	// $duration = empty($post->duration) ? update_activity_duration($post) : $post->duration;
	// $end = $start + $duration;

	return $format ? date($format, $end) : intval($end);
}

function update_activity_duration($post) {
	$start = get_post_meta($post->ID, 'date', true);
	$end = get_post_meta($post->ID, 'date_end', true);
	$duration = $end - $start;

	update_post_meta($post->ID, 'duration', $duration);
	return $duration;
}

// START LOGIN PAGE MOD
add_filter('login_headerurl', function() { return home_url(); });
add_filter('login_headertitle', function() { return get_bloginfo('name'); });
add_action('login_enqueue_scripts', function() {
	$app_cfg = get_option('app_config');
	?>
	<style type="text/css">
		#login h1 a, .login h1 a {
			background-image: url(<?= get_logo_url() ?>);
		}
	</style>
<?php });
// END LOGIN PAGE MOD

function get_logo_url() {
	$app_cfg = get_option('app_config');

	if (empty($app_cfg['logo'])) {
		$app_cfg['logo'] = get_template_directory_uri() . '/images/logo.png';
	}

	return $app_cfg['logo'];
}

// hide FB login section if keys not configured
add_action('login_footer', 'hide_newsociallogins');
function hide_newsociallogins() {
	$opt = maybe_unserialize(get_option('nextend_fb_connect'));
	if (empty($opt['fb_appid']) || empty($opt['fb_secret'])) {
		echo '<script>var has_social_form = window.fb_added = true;</script>';
	}
}

add_action('wp_head', 'print_custom_styles');
function print_custom_styles() {
	$out = '';
	$app_cfg = get_option('app_config');

	if ( ! empty($app_cfg['color_text'])) {
		$out .= sprintf('body { color: %s; }', $app_cfg['color_text']);
	}
	if ( ! empty($app_cfg['font'])) {
		$out .= sprintf("body {font-family: '%s', sans-serif;}", $app_cfg['font']);
	}
	if ( ! empty($app_cfg['title_font'])) {
		$out .= sprintf("h1,h2,h3,h4,h5,h6 {font-family: '%s', sans-serif;}", $app_cfg['title_font']);
	}

	if ($out) $out = "\n<style>\n" . $out . "\n</style>\n";
	echo $out;
}
