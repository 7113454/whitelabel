<?php get_header() ?>

<?php if (is_user_logged_in()): ?>
	<div class='profile-menu'>
		<div class='row'>
			<div class='col-md-6 col-md-offset-3 center'>
				<h1>Welcome back</h1>

				<div class='leader-menu center'>
					<a href='/latest' class='btn btn-default'> <i class='fa fa-caret-up'></i> Latest</a>
					<a href='/likes' class='btn btn-default'><i class='fa fa-star-o'></i> Favorites</a> 
					<a href='/hot' class='btn btn-default'><i class='fa fa-arrow-up'></i> Popular</a> 
					<a href='/users' class='btn btn-default'><i class='fa fa-group'></i> Users</a> 
				</div>

				<div class='home-in-options'>
					<p class='lead'>
						Check out <a href='/latest'>what is going on</a>,
						<a href='/hot'>most popular activities</a>, <a href='/users'>who is hot</a>
						or <a href='/edit'>edit your profile</a>.
					</p>
				</div>
		
			</div>
		</div>
	</div>
	<br /><br />

<?php else: ?>
	<div class="home-hero">
		<link rel="stylesheet" href="<?= get_template_directory_uri() ?>/js/slick/slick.css"/>
		<script src="<?= get_template_directory_uri() ?>/js/slick/slick.min.js"></script>

		<?php
		$slides = get_option('home_carousel');
		if (is_array($slides) && count($slides)) {
			echo '<div id="home-carousel">';
			foreach ($slides as $s) {
				print('<div class="slide">');
				printf('<img src="%s" alt="">', $s['img']);
				printf('<div class="txt"><h1>%s</h1></div>', $s['txt']);
				print('</div>');
			}
			echo '</div>';
		}
		?>

		<script>
			jQuery(document).ready(function(){
				jQuery('#home-carousel').slick({autoplay: true, dots: true});
			});
		</script>
	</div>
<?php endif ?>

<?php get_footer() ?>

