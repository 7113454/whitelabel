<?php

class VU_Notifications {
	private $user_id = null;
	private $notif = null;

	private $defaults = array(
		1 => 'follower',
		2 => 'latest', // uncomment function new_post_email_notify()
		4 => 'marketing',
		8 => 'likes',
		16 => 'attends',
		32 => 'messages',
	);

	private $labels = array(
		'follower' => 'New Followers',
		'latest' => 'New activities',
		'marketing' => 'Marketing &amp; promotional e-mail from UB Fitness',
		'likes' => 'Likes',
		'attends' => 'Attending',
		'messages' => 'Messages',
	);

	public function __construct($user_id = null) {
		$this->user_id = $user_id ? $user_id : get_current_user_id();
		$this->notif = get_user_meta($this->user_id, 'notifications', true);
		if ('' === $this->notif) {
			$this->notif = array_sum(array_keys($this->defaults));
		}
	}

	public function subscribe($events) {
		$this->notif = 0;
		foreach ($this->defaults as $k => $v) {
			if ( isset($events[$v]) && filter_var($events[$v], FILTER_VALIDATE_BOOLEAN)) {
				$this->notif |= $k;
			}
		}
		$this->save();
	}

	public function subscribeApi($events) {
		$this->notif = 0;
		foreach ($this->defaults as $k => $v) {
			if ( ! isset($events[$v]) || filter_var($events[$v], FILTER_VALIDATE_BOOLEAN)) {
				$this->notif |= $k;
			}
		}
		$this->save();
	}

	public function save() {
		update_user_meta($this->user_id, 'notifications', $this->notif);
	}

	public function isFlagSet($flag) {
		return (($this->notif & $flag) == $flag);
	}

	public function toArray() {
		$out = array();
		foreach ($this->defaults as $k => $v) {
			$out[$v] = $this->isFlagSet($k);
		}
		return $out;
	}

	public function getLabel($perm) {
		if (ctype_digit($perm)) {
			$perm = $this->defaults[$perm];
		}
		$label = isset($this->labels[$perm]) ? $this->labels[$perm] : $perm;

		return $label;
	}
}

add_shortcode('ub_notifications', 'ub_notifications');
function ub_notifications() {
	$notif = new VU_Notifications();

	$out = sprintf('<form method="post" action="%s">', admin_url('admin-ajax.php'));
	foreach ($notif->toArray() as $k => $v) {
		$checked = $v ? 'checked' : '';
		$out .= '<div class="checkbox checkbox-slider--b-flat">';
		$out .= sprintf('<label class="notif"><input type="checkbox" name="notifications[%s]" %s><span>%s</span></label>', $k, $checked, $notif->getLabel($k));
		$out .= '</div>' . PHP_EOL;
	}
	$out .= '<input type="hidden" name="action" value="notification"></form>';
	return $out;
}

add_action('wp_ajax_notification', 'ub_subscribe_notification');
function ub_subscribe_notification() {
	$notif = new VU_Notifications();
	$notif->subscribe($_POST['notifications']);
	wp_die('OK');
}