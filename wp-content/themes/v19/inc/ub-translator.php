<?php
// translate phrases

class ubTranslate {
	public $lng;
	private $phrases;
	private static $instance = null;

	public static function instance() {
		if (self::$instance === null) {
			self::$instance = new ubTranslate();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->detect_lng();
		$this->load_phrases();
	}

	private function detect_lng() {
		preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
		$this->lng = isset($lang_parse[1]) ? $lang_parse[1][0] : 'en';
	}

	private function load_phrases() {
		global $wpdb;

		if (0 === strpos($this->lng, 'en')) return;

		$sql = $wpdb->prepare("SELECT phrase, translate FROM {$wpdb->prefix}phrases WHERE lng = %s", $lng);
		$buf = $wpdb->get_results($sql);

		foreach ($buf as $b) {
			$this->phrases[$b->phrase] = $b->translate;
		}
	}

	public function tr($phrase) {
		if (0 === strpos($this->lng, 'en')) return;

		if ( ! array_key_exists($phrase, $this->phrases)) $this->unknown_phrase($phrase);
		return $this->phrases[$phrase];
	}

	private function unknown_phrase($phrase) {

	}
}
