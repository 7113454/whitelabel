<?php
$now = time();
$status = next_start_date($post) < $now && next_end_date($post) > $now ? 'going' : (next_end_date($post) < $now ? 'past' : 'future');
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($status); ?>>

	<div class="post-top clearfix">
		<div class="post-avatar">
			<?= get_avatar($post->post_author, 40, '', get_the_author_meta('display_name'), ['extra_attr' => sprintf('title="%s"', get_the_author_meta('display_name'))]) ?>
			<?= follow_user_btn() ?>
		</div>
		<div class="post-author">
			<a href="<?= get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a> 
		</div>
		<div class="post-time">
			<?php printf( _x( '%s', '%s = human-readable time difference', 'your-text-domain' ), v19_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
		</div>
	</div>

	<a href="<?= get_permalink() ?>" title="<?php the_title_attribute() ?>" class="fav-thumb">
		<?php the_post_thumbnail('medium', array('title' => get_the_title(), 'alt' => get_the_title() . ' thumbnail')) ?>
		<?php if ($post->youtube): ?>
			<span class="playVideoBnt" data-youtube-id="<?= youtube_id($post->youtube) ?>" title="Watch Video"><i class="fa fa-play-circle"></i></span>
		<?php endif ?>
	</a>

	<h3 class="entry-header">
		<a href="<?php the_permalink() ?>" class="title" title="<?php the_title_attribute() ?>"><?php the_title() ?></a>
	</h3>

	<?php if ('post' == get_post_type()): ?>
		<div class="post-date-start"><?= ago($post->date) ?></div>
		<address><?= isset($post->location['address']) ? esc_html($post->location['address']) : 'Online only' ?></address>
	<?php else: ?>
		<div class="post-date-start">&nbsp;</div>
		<address><?= get_the_excerpt() ?></address>
	<?php endif ?>


	<footer class="entry-meta">
		<!--<span class="tag-links"><?php the_tags('', '', '') ?>&nbsp;</span>-->

		<a href="#" class="btn btn-default ub-share" data-url="<?= get_the_permalink() ?>" data-text="<?= get_the_title() ?>" data-image="<?= get_the_post_thumbnail_url(null, 'full') ?>" title="Share">
			<i class="fa fa-share"></i>
		</a>

		<?php if (is_super_admin()): ?>
			<button class="btn btn-default notHotBtn" title="Don't show on HOT"><i class="fa fa-flag"></i></button>
		<?php endif ?>

		<?= ub_favorite(get_the_ID()) ?>
	</footer>
</article>

