// autosave form to local storage
(function($) {
	$.fn.vu_form = function() {
		this.each(function() {
			var form = $(this);
			var form_id = form.attr('id');

			// fix for WPUF forms
			if ('undefined' == typeof form_id) {
				form_id = form.find('input[name=form_id]').val();
				form_id && form.attr('id', form_id);
			}

			if ( ! form_id) {
				console.warn('Form has no ID and cannot be saved.');
				return this;
			}

			// load saved data if any
			$.fn.vu_form.load(form);

			form.on('input', function() {
				$.fn.vu_form.save(form);
			});

			form.on('submit', function() {
				$.fn.vu_form.clear(form);
			});
		});

		return this;
	};

	$.fn.vu_form.save = function(form) {
//		console.log('save form ' + Math.floor(Date.now() / 1000));
		localStorage.setItem('form-' + form.attr('id'), JSON.stringify(form.serializeArray()));
	}

	$.fn.vu_form.load = function(form) {
		var json = JSON.parse(localStorage.getItem('form-' + form.attr('id')));
		$.each(json, function(i, val) {
			if ('_wpnonce' == val.name) return;
			form.find('[name="'+val.name+'"]').val(val.value);
		});
	}

	$.fn.vu_form.clear = function(form) {
		localStorage.removeItem('form-' + form.attr('id'));
	}
}( jQuery ));
