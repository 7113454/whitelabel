/**
 * Theme functions file
 *
 * Contains handlers for navigation, accessibility, header sizing
 * footer widgets and Featured Content slider
 *
 */

( function( $ ) {
	// USER SUBMITTED POSTS MODIFICATIONS
	$('#btnAdvanced').on('click', function(e) {
		e.preventDefault();
		$("#advancedBox").toggleClass("hidden");

		if (0 == $('#usp-map .gm-style').length) {
			init_map('#usp-map');
		}
	});

	$('.wpuf-form-add').vu_form();
	$('.select-repeat').rrule();

	$('#post_title').on('input', function() {
		var phrase = $(this).val();
		fillTags(phrase);
	});
	// END

	$(function(){
		$(document).on('click', '.ub-fav', ub_favorite);
		$(document).on('click', '.ub-share', ub_share);
		$(document).on('click', '.follow-btn-sm', ub_follow);
		$('.btn-attend').on('click', ub_attend);

		if (typeof logged_in != 'undefined' && logged_in) init_date_picker();

		$('#btnCookieOK').on('click', function() {
			Cookies.set('cookieOK', 'OK', {expires: 365, path: '/'});
			$(this).parent().fadeOut('slow');
		});

		// disable submit button after click
		$('#your-profile input[type=submit]').on('click', function() {
//			$(this).val('Please wait...').prop('disabled', true);
			var div = $('<div class="text-center"><img src="/wp-content/themes/v19/images/ajax-loader.gif" style="width:initial;"></div>');
			var offset = $(this).position();
			div.css({
				position: 'absolute',
				background: '#FFF',
				margin: '20px 0',
				'line-height': '50px',
				top: offset.top,
				left: offset.left,
				width: $(this).outerWidth(),
				height: $(this).outerHeight()
			});
			$(this).after(div);
		});

		$('.infinite-scroll').jscroll({
			debug: false,
			contentSelector: '.infinite-scroll',
			nextSelector: '.next a',
			loadingHtml: '<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
		});

		$('#date-type').on('click', changeDateType).trigger('click');
	});

	$('.search-toggle').on('click', function(e) {
		e.preventDefault();
		$('#collapsedSearch').slideToggle(400, function() {
			$('input:first', this).focus();
			$('.top-menu .search-toggle').toggleClass('active');

			if ( $(this).is(':visible')) {
				if (logged_in) $.post(ajaxurl, {action: 'toggle_search_form', showSearch: 1});
				Cookies.set('showSearch', 1, {expires: 30});
			}
			else {
				if (logged_in) $.post(ajaxurl, {action: 'toggle_search_form', showSearch: 0});
				Cookies.remove('showSearch');
			}
		});
	});

	$('.playVideoBnt').on('click', function(e) {
		e.preventDefault();
		embedYouTube($(this).data('youtube-id'), this);
	});

	$(document).on('click', '.notHotBtn', function() {
		if ( ! confirm('Are you sure want hide the activity?')) return;
		var post = $(this).closest("[id^='post-']");
		var id = post.attr('id').match(/\d+/)[0];
		$.post(ajaxurl, {action: "not_hot", post_id: id, value: 1});
		post.parent().remove();
	});

	// collapse panels
	$('.btn-collapse-panel').on('click', function(e) {
		e.preventDefault();
		var panel = $(this).closest('.panel').toggleClass('panel-closed');
		var id = panel.attr('id');

		if ( ! id) {
			console.error('Panel has no ID attribute.');
			return;
		}

		var cookie = Cookies.get('closed');
		cookie = cookie ? cookie.split(',') : [];

		// save panel state in cookie
		if (panel.hasClass('panel-closed')) {
			cookie.push(id);
		}
		else {
			cookie.splice(cookie.indexOf(id), 1);
		}
		Cookies.set('closed', cookie.join(','), {expires: 365})
	});

	$(document).on('click', '.btn-more', function() {
		var btn = $(this);
		btn.prop('disabled', true).html('<i class="fa fa-spinner fa-spin"></i>');
		$.get(btn.data('target'), function(r) {
			btn.replaceWith(r.html);
		});
	});
} )( jQuery );

// fix file upload in iOS Safari
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
if (iOS) {
	jQuery('.file-selector').on('click', function(e) {
//		e.stopPropagation();
		jQuery(this).closest('.wpuf-el').find('input[type=file]').trigger('click');
	});
}

function init_map(el) {
		jQuery(el).locationpicker({
			radius: 0,
			inputBinding: {
				latitudeInput: jQuery('#usp-lat'),
				longitudeInput: jQuery('#usp-lon'),
				locationNameInput: jQuery('#user-submitted-location')
			},
			location: {longitude: -73.82480777777776, latitude: 40.7324319},
			enableAutocomplete: true
		});
}

function ub_favorite(e) {
	e.preventDefault();

	if ( ! logged_in) {
		location.href = "/login?action=register";
		return;
	}

	var btn = jQuery(this);
	var data = {
		action: "ub_like",
		post_id: btn.data('id')
	};
	jQuery.post(ajaxurl, data, function(r) {
		btn.replaceWith(r.btn);
	});
}

function ub_attend() {
	var btn = jQuery(this);
	var post_id = btn.data('post');

	btn.toggleClass('active');

	jQuery.post(ajaxurl, {post_id: post_id, action: 'attending'});
}

function ub_report(type, id, btn) {
	var $ = jQuery.noConflict();
	var form = $('#formReport');

	if ( ! form.length) { // create form if not exists yet
		var html = " \
<form id=\"formReport\" method=\"post\" action=\"\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\"> \
	<div class=\"modal-dialog\"> \
		<div class=\"modal-content\"> \
			<div class=\"modal-header\"> \
				<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button> \
				<h4 class=\"modal-title\">Help Us Understand What's Happening</h4> \
			</div> \
			<div class=\"modal-body\"> \
				<div class=\"form-group\"> \
					<label>Subject</label> \
					<select name=\"subject\" class=\"form-control\"> \
						<option>Spam</option> \
						<option>Threatening, harassing, or inciting violence</option> \
						<option>Personal and confidential information</option> \
						<option>It's annoying or not interesting</option> \
					</select> \
				</div> \
				<div class=\"form-group\"> \
					<label>Message</label> \
					<textarea class=\"form-control\" rows=\"3\" name=\"message\"></textarea> \
				</div> \
			</div> \
			<div class=\"modal-footer\"> \
				<input type=\"hidden\" name=\"action\" value=\"report\"> \
				<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button> \
				<button type=\"button\" class=\"btn btn-primary\" id=\"btnSubmitReport\">Report</button> \
			</div> \
		</div> \
	</div> \
</form> \
";
		$('body').append(html);
		form = $('#formReport');
		form.append('<input type="hidden" name="item" value="' + type + '">');
		form.append('<input type="hidden" name="id" value="' + id + '">');

		$('#btnSubmitReport').on('click', function() {
			btn.prop('disabled', true);
			form.modal('hide');
			$.post(location.href, form.serializeArray());
		});
	}

	form.modal();
}

// START ACTIVITIES MAP
var infowindow = null;
var markers = [];
var activities_map = null;

function ub_activities_map(type) {
	var marker = null;
//	var bounds = new google.maps.LatLngBounds();
	var myLatLng = {lat: 35.404588, lng: -40.519732};
	var first_run = false;

	if ( ! activities_map) {
		first_run = true;

		activities_map = new google.maps.Map(document.getElementById('activities-map'), {
			zoom: 3,
			center: myLatLng
		});
	}

	infowindow = new google.maps.InfoWindow({content: "holding..."});

	// remove old markers
	for (var i in markers) {
		markers[i].setMap(null);
		markers.splice(i, 1);
	}
	
	for (var i in activities[type]) {
		var a = activities[type][i];
		var date = new Date(a['date'] * 1000);

		if (a['img']) {
			var html = '<table><tr><td><img src="' + a['img'] + '" alt="" class="img-circle"></td><td>'
				+ '<strong><a href="' + a['url'] + '">' + a['title'] + '</a></strong>'
				+ '<div>Start ' + date.toUTCString() + '</div>'
				+ '<div class="descr"><a href="' + a['author_url'] + '">' + a['author'] + '</a><div>'
				+ '</td></tr></table>';
		}
		else {
			var html = '<strong><a href="' + a['url'] + '">' + a['title'] + '</a></strong>'
				+ '<div>Start ' + date.toUTCString() + '</div>'
				+ '<div class="descr"><a href="' + a['author_url'] + '">' + a['author'] + '</a><div>';
		}

		marker = new google.maps.Marker({
			map: activities_map,
			title: a['title'],
			html: html,
			icon: (a['featured'] ? base_url + '/wp-content/themes/v19/images/gmap-marker.png' : null),
			position: {lat: parseFloat(a['location']['lat']), lng: parseFloat(a['location']['lng'])}
		});

		if (a['featured']) {
			marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
		}

		google.maps.event.addListener(marker, 'click', function () {
			infowindow.setContent(this.html);
			infowindow.open(activities_map, this);
		});
		
//		bounds.extend(marker.getPosition());
		markers.push(marker);
	}

	// zoom to position if provided in URL
	if (first_run && getUrlParameter('lat')) {
		var LatLng = {lat: parseFloat(getUrlParameter('lat')), lng: parseFloat(getUrlParameter('lng'))};
		if (getUrlParameter('lat')) {
			activities_map.setCenter(LatLng);
			activities_map.setZoom(16);
		}

		// add marker is there is no one
		var markerExists = false;
		// for (i in markers) {
		// 	if (LatLng.lat == markers[i].getPosition().lat() && LatLng.lng == markers[i].getPosition().lng()) {
		// 		markerExists = true;
		// 	}
		// }
		if ( ! markerExists) {
			marker = new google.maps.Marker({
				map: activities_map,
//				title: a['title'],
//				html: '',
//				icon: (a['featured'] ? base_url + '/wp-content/themes/v19/images/gmap-marker.png' : null),
				position: LatLng
			});
			markers.push(marker);
		}
	}
	else {
//		activities_map.fitBounds(bounds);
		var markerCluster = new MarkerClusterer(activities_map, markers, {imagePath: base_url+'/wp-content/themes/v19/images/m'});
	}
}

jQuery('#activities-map-buttons input[type=radio]').on('change', function() {
	if (jQuery(this).prop('checked')) {
		ub_activities_map(jQuery(this).val());
	}
});
// END ACTIVITIES MAP

function edit_activity(post_id) {
	var $ = jQuery.noConflict();;
	$.get(ajaxurl, {action: 'get_activity', post_id: post_id}, function(r) {
		if ('undefined' == typeof r.ID) return false;

		var form = $('#addListingModal form');
		form.find('input[name="post_title"]').val(r.post_title);
		form.find('textarea[name="post_content"]').val(r.post_content);
		form.find('input[name="date_start"]').val(r.date);
		form.find('input[name="date_end"]').val(r.date_end);
		form.find('input[name="info_url"]').val(r.info_url);
		form.find('input[name="youtube"]').val(r.youtube);
		form.find('input[name="hashtag"]').val(r.hashtag);
//		form.find('select[name="repeat"]').val(r.repeat);

		form.find('input:radio[name=disabled_facilities]').each(function() {
			$(this).prop('checked', $(this).val() == r.disabled_facilities);
		});
		form.find('input:radio[name=food_available]').each(function() {
			$(this).prop('checked', $(this).val() == r.food_available);
		});

		for (var i in r.category) {
			form.find('select[name="category[]"]').val(r.category[i].term_id);
		}

		var tags = [];
		for (i in r.tags) {
			tags.push(r.tags[i].name);
		}
		form.find('input[name="tags"]').val(tags.join());

		var id_field = form.find('input[name="post_id"]');
		if ( ! id_field.length) {
			id_field = $('<input type="hidden" name="post_id">').appendTo(form);
			$('<input type="hidden" name="post_date" value="'+r.post_date+'">').appendTo(form);
		}
		id_field.val(post_id);

		if ('undefined' != typeof r.location.address) {
			form.find('input[name="location"]').val(r.location.address);
			form.find('input[name="latitude"]').val(r.location.lat);
			form.find('input[name="longitude"]').val(r.location.lng);
		}

		$('#addListingModal').modal();
	});
	return false;
}

function showRepeatOptions(e) {
	var $ = jQuery.noConflict();
	var repeat = $(this).val();

	$('#usp_form .r-toggle').each(function() {
		if ($(this).hasClass(repeat)) {
			$(this).removeClass('hidden');
		}
		else {
			$(this).addClass('hidden');
		}
	});
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function ub_share(event) {
	event.preventDefault();

	var $ = jQuery.noConflict();
	var btn = $(this);
	var text = encodeURIComponent(btn.data('text'));
	var url = encodeURIComponent(btn.data('url'));
	var image = encodeURIComponent(btn.data('image'));
	var type = btn.closest('.status').length ? 'status' : 'post';

	var modal = $('<div id="shareModal" class="modal fade '+type+'"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">Share with friends</h4></div><div class="modal-body"></div></div></div></div>');
	var modal_body = $('.modal-body', modal);

	modal_body.append('<a href="https://twitter.com/share?text='+text+'&amp;url='+url+'&amp;hashtags=fitness,health,ubfitness" class="btn btn-default btn-twitter" target="_blank"><i class="fa fa-twitter"></i></a>');
	modal_body.append('<a href="https://www.facebook.com/sharer.php?u='+url+'" class="btn btn-default btn-fb" target="_blank"><i class="fa fa-facebook btn-fb"></i></a>');
	modal_body.append('<a href="https://pinterest.com/pin/create/button/?url='+url+'&amp;media='+image+'&amp;description='+text+'" class="btn btn-default btn-pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>');
	modal_body.append('<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url='+url+'&amp;title='+text+'&amp;summary=by+UB+Fitness" target="_blank" class="btn btn-default btn-linkedin"><i class="fa fa-linkedin"></i></a>');
	modal_body.append('<a href="mailto:?subject='+encodeURIComponent('Check this out from UB Fitness')+'&amp;body='+text+'%20'+url+'" title="Email to a friend/ colleague" class="btn btn-default btn-email"><i class="fa fa-envelope-o"></i></a>');

	$('body').append(modal);
	modal.modal();

	// destroy when close modal
	modal.on('hidden.bs.modal', function() {
		$(this).remove();
	});
}

function init_date_picker() {
	var $ = jQuery;

	var opt = {
		dateFormat: "mm/dd/yy",
		showOn: "button",
		buttonText: "<i class=\"fa fa-calendar\"></i>",
		controlType: 'select'
	};
	var start = $('#date_start').val() ? $('#date_start').val() : moment().hour(9).minute(0).format('MM/DD/YYYY hh:mm');
	$('#date_start').val(start).datetimepicker(opt); // init datetimepicker

	// start date set limits for end date
	opt.beforeShow = function() {
		var date_start = $('#date_start').val();
		if ('' == date_start) return false;

		var d = new Date(date_start);
		d.setUTCMonth(d.getUTCMonth() + 3);
		$(this).datepicker("option", {minDate: date_start, maxDate: d});
	};
	var end = $('#date_end').val() ? $('#date_end').val() : moment().hour(9).minute(0).add(1, 'week').format('MM/DD/YYYY hh:mm');
	$('#date_end').val(end).datetimepicker(opt);
}

function changeDateType(e) {
	e.preventDefault();
	var $ = jQuery.noConflict();

	var btn = $(this);
	var type = btn.data('type');
	type = undefined == type ? 5 : type;
	type = 5 == type ? 1 : type+1;
	$(this).data('type', type);

	$('.date[data-time]').each(function() {
		var time = moment.utc($(this).data('time') * 1000);
		switch (type) {
			case 1:
				$('.timezone').html('Your time');
				$(this).html(time.local().fromNow());
				break;
			case 2:
				$('.timezone').html('Local time');
				$(this).html(time.tz(event_tz).fromNow());
				break;
			case 3:
				$('.timezone').html('Your time');
				$(this).html(time.local().format('D MMM Y HH:mm'));
				break;
			case 4:
				$('.timezone').html('Local time');
				$(this).html(time.tz(event_tz).format('D MMM Y HH:mm'));
				break;
			case 5:
				$('.timezone').html('UTC');
				$(this).html(time.format('D MMM Y HH:mm'));
				break;
		}
	});
}

function embedYouTube(youtube_id, el) {
	var url = 'https://www.youtube.com/embed/' + youtube_id + '?autoplay=1';
	var html = '<iframe width="100%" height="100%" src="' + url + '" frameborder="0" allowfullscreen></iframe>';
	jQuery(el).parent().html(html);
}

function ub_follow(e) {
	e.preventDefault();

	var data = {
		action: 'ub_follow',
		author_id: jQuery(this).data('author')
	};

	jQuery.post(ajaxurl, data, function(r) {
		if (r.follow) jQuery('.follow-btn-sm[data-author=' + data.author_id + ']').addClass('active').attr('title', 'Unfollow');
		else jQuery('.follow-btn-sm[data-author=' + data.author_id + ']').removeClass('active').attr('title', 'Follow');
	});
}

jQuery('.notif input').on('change', function() {
	var data = jQuery(this).closest('form').serializeArray();
	jQuery.post(ajaxurl, data);
});

function add_message(msg) {
	var date = moment.utc(msg.date);
	var files = '';

	for (var i in msg.files) {
		if (0 == msg.files[i].mime.indexOf('image/')) {
			files += '<div class="file"><a href="'+ msg.files[i].url +'" target="_blank"><img src="'+ msg.files[i].url +'" class="img-rounded"></a></div>';
		}
		else {
			files += '<div class="file"><a href="'+ msg.files[i].url +'" target="_blank">'+ msg.files[i].name +'</a></div>';
		}
	}

	var tr = '<tr><td><input type="checkbox" name="id[]" value="'+msg.id+'"></td>'
		+ '<td class="user">'+msg.sender.name+'</td>'
		+ '<td class="msg">' + msg.text + files + '</td>'
		+ '<td class="date">'+date.format('h:mm A')+'</td></tr>';
	jQuery('#thread').append(tr);
}

function fillTags(phrase) {
	if (typeof all_tags === "undefined") {
		jQuery.ajax(ajaxurl, {
			async: false,
			data: {action: "all_tags"},
			success: function(tags) { window.all_tags = tags; }
		});
	}

	var tags = [];
	var words = phrase.split(' ');
	var tags_field = jQuery('.wpuf_tags_6900');

	for (var i in words) {
		if (all_tags.indexOf(words[i].toLowerCase()) != -1) {
			tags.push(words[i].toLowerCase());
		}
	}

	tags_field.val(tags.join(', '));
}
