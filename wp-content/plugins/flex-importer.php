<?php
/*
Plugin Name: Import Flex
Plugin URI: 
Description: Import events from <a href="https://www.joinflex.tv/">joinflex.tv</a>, once a day.
Author: ub.fitness
Version: 1.0
Author URI: 
*/

define('FLEX_KEY', 'ub.fitness');
define('FLEX_SECRET', 'b56ab073e5b8a7e7a6132fe9457a4332');
define('FLEX_AUTHOR_ID', 653);

register_activation_hook(__FILE__, 'flex_activation');
function flex_activation() {
	if ( ! wp_next_scheduled ('flex_cronjob')) {
		wp_schedule_event(time(), 'daily', 'flex_cronjob');
	}
}

register_deactivation_hook(__FILE__, 'flex_deactivation');
function flex_deactivation() {
	wp_clear_scheduled_hook('flex_cronjob');
}

add_action('flex_cronjob', 'flex_run');
// main function
function flex_run() {
	global $wpdb;

	// includes for media_sideload_image()
	require_once(ABSPATH . 'wp-admin/includes/media.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	wp_set_current_user(FLEX_AUTHOR_ID);

	remove_filter('wp_insert_post', 'ub_custom_fields');
	remove_action('new_to_publish', 'ub_enqueue_to_notify');

	$args = array(
		'timeout' => 5,
		'headers' => array('authorization' => 'Basic ' . base64_encode(FLEX_KEY . ':' . FLEX_SECRET)),
	);
	$response = wp_remote_get('https://api.joinflex.tv/v1/events', $args);

	if ( ! is_array($response) || 200 != $response['response']['code']) {
		trigger_error('Flex Import: cannot fetch data.', E_USER_WARNING);
		return false;
	}

	$data = json_decode($response['body']);
//	$data = json_decode(file_get_contents('/home/misc/www/flex.json'));
	unset($response);
	$cnt = 0;
	$now = time();

	$sql = "SELECT meta_value FROM {$wpdb->postmeta} WHERE meta_key = '_flex_id'";
	$imported = $wpdb->get_col($sql);

	foreach ($data as $e) {
		// skip imported and finished events
		if (in_array($e->id, $imported) || strtotime($e->start_date_time) < $now) continue;

		$id = flex_import($e);

		$cnt++; if ($cnt > 9) break;
	}
}

function flex_import($event) {
	$date_start = strtotime($event->start_date_time);

	$post = array(
		'post_title' => $event->name,
		'post_content' => $event->description,
//		'post_author' => 639,
		'post_status' => 'publish',
		'meta_input' => array(
			'_flex_id' => $event->id,
			'date' => $date_start,
			'date_end' => $date_start + $event->duration * 60,
			'info_url' => $event->url,
		),
	);

	$id = wp_insert_post($post);

	if ($id) {
		wp_set_post_categories($id, flex_search_category($event->category));
		wp_set_post_tags($id, array('Online', 'Flex', 'Stream'));

		// set featured image
		$img = media_sideload_image($event->image_url, $id, 'flex');
		if ( ! is_wp_error($img)) {
			$media = get_attached_media('image', $id);
			if ( ! empty($media)) set_post_thumbnail($id, key($media));
		}
	}

	return $id;
}

// add_action('flex_cronjob', 'flex_delete_expired');
// function flex_delete_expired() {
// 	global $wpdb;

// 	$sql = "SELECT k.post_id
// 		FROM {$wpdb->postmeta} AS k
// 		JOIN {$wpdb->postmeta} AS d ON k.post_id=d.post_id AND d.meta_key='date_end'
// 		WHERE k.meta_key='_flex_id' AND d.meta_value < UNIX_TIMESTAMP()
// 		LIMIT 200";
// 	$posts = $wpdb->get_col($sql);

// 	foreach ($posts as $id) {
// 		wp_delete_post($id, true);
// 	}
// }

function flex_search_category($name) {
	$term = get_term_by('name', $name, 'category');

	return $term ? $term->term_id : 0;
}
