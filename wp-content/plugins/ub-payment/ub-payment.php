<?php
/*
Plugin Name: UB Payment
Plugin URI: 
Description: Payments integration
Author: ub.fitness
Version: 1.0
Author URI: 
*/

add_action('admin_menu', 'bc_register_menu');
function bc_register_menu() {
	add_menu_page('Payments', 'Payments', 'manage_options', 'bc-home', 'bc_home_page');
}

function bc_home_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include('admin/settings.php');
}

add_action('admin_init', 'register_bc_settings');
function register_bc_settings() {
	register_setting('bc-settings', 'bc_pspid');
	register_setting('bc-settings', 'bc_shain');
	register_setting('bc-settings', 'bc_shaout');
	register_setting('bc-settings', 'bc_sandbox');
}

// create payment form
add_shortcode('bc_form', 'bc_form');
function bc_form() {
	$bc = new Barclaycard();
	$order = $bc->order($_GET['item']);

	if ($order) {
		$out = $bc->form();
	}
	else {
		$out = '<div class="alert alert-danger">Cannot process.</div>';
	}

	return $out;
}

// handle the payment proccessor response
add_action('wp_ajax_nopriv_bc_webhook', 'bc_webhook');
function bc_webhook() {
	global $wpdb;
	$bc = new Barclaycard();
	$order = false;

	$fields = $_POST;
	unset($fields['SHASIGN']);

	if ($bc->validateFeedback($fields, $_POST['SHASIGN'])) {
		$sql = $wpdb->prepare("SELECT * FROM wp_orders WHERE order_id = %d", $fields['orderID']);
		$order = $wpdb->get_row($sql);
	}

	if ($order) {
		$action = explode('-', $order->text, 2);
		$wpdb->update('wp_orders', array('reference' => $fields['PAYID']), array('order_id' => $order->order_id));

		switch($action[0]) {
			case 'activity':
				ubMakePostFeatured($action[1]);
				break;
		}
	}

	wp_die();
}

class Barclaycard {
	private $PSPID = 'epdq7006360';

	// LIVE
	private $SHA_IN = 'G2uQ-!zEv#nFdL3?';
	private $SHA_OUT = 'tz+U@6+t?4NTzfk*';
	private $URL = 'https://payments.epdq.co.uk/ncol/prod/orderstandard.asp';

	// TEST
//	private $SHA_IN = 'W-Cp3AHa&B5&KRFS';
//	private $SHA_OUT = 'Z35$65QFJt5a&T#6';
//	private $URL = 'https://mdepayments.epdq.co.uk/ncol/test/orderstandard.asp';

	private $prices = array(
		'activity' => 500, // featured activity
	);

	private $order = false;
	private $amount = 0;

	function order($item) {
		global $wpdb;
		$this->order = uniqid();
		$parts = split('-', $item);

		if ( ! array_key_exists($parts[0], $this->prices)) {
			trigger_error('Invalid Payment: ' . $item, E_USER_WARNING);
			return false;
		}

		$this->amount = $this->prices[$parts[0]];
		$wpdb->insert('wp_orders', array('order_id' => $this->order, 'text' => $item, 'amount' => $this->amount));

		return $this->order;
	}


	private function sign($fields, $key) {
		$str = '';
		ksort($fields, SORT_STRING | SORT_FLAG_CASE);

		foreach ($fields as $k => $v) {
			$str .= strtoupper($k) . '=' . $v . $key;
		}

		return strtoupper(sha1($str));
	}

	function validateFeedback($fields, $shasign) {
		return $this->sign($fields, $this->SHA_OUT) == $shasign;
	}

	function form() {
		$fields = array(
			'AMOUNT' => $this->amount,
			'CURRENCY' => 'GBP',
			'LANGUAGE' => 'en_US',
			'ORDERID' => $this->order,
			'PSPID' => $this->PSPID,

//			'LOGO' =>  get_template_directory_uri() . '/images/logo-in.png',
		);

		$out = '<form method="POST" action="' . $this->URL . '" id="bc_form">';

		foreach ($fields as $k => $v) {
			$out .= sprintf('<input type="hidden" name="%s" value="%s">', $k, $v);
		}

		$out .= '<input type="hidden" name="SHASIGN" value="' . $this->sign($fields, $this->SHA_IN) . '">';
		$out .= '</form><script>document.getElementById("bc_form").submit();</script>';
		return $out;
	}
}

