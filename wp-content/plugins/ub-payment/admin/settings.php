<div class="wrap">
	<h2>Barclaycard Settings</h2>

	<form method="post" action="options.php">
		<?php settings_fields('bc-settings') ?>
		<?php do_settings_sections('bc-settings') ?>

		<table class="form-table">
			<tr>
				<th scope="row">PSPID</th>
				<td><input type="text" name="bc_pspid" value="<?= esc_attr(get_option('bc_pspid')) ?>" class="regular-text"/></td>
			</tr>
			<tr>
				<th scope="row">SHA-IN</th>
				<td>
					<input type="password" name="bc_shain" value="<?= esc_attr(get_option('bc_shain')) ?>" class="regular-text"/>
					<button type="button" class="togglePass">Show</button>
				</td>
			</tr>
			<tr>
				<th scope="row">SHA-OUT</th>
				<td>
					<input type="password" name="bc_shaout" value="<?= esc_attr(get_option('bc_shaout')) ?>" class="regular-text"/>
					<button type="button" class="togglePass">Show</button>
				</td>
			</tr>
			<tr>
				<th scope="row">Sandbox</th>
				<td><input type="checkbox" name="bc_sandbox" value="1" <?= get_option('bc_sandbox') ? 'checked' : '' ?> /></td>
			</tr>
		</table>

		 <?php submit_button() ?>
	</form>
</div>

<script>
	jQuery(function($){
		$('.togglePass').on('click', function() {
			$(this).siblings('input').each(function() {
				if ('password' == $(this).attr('type')) {
					$(this).attr('type', 'text');
				}
				else if ('text' == $(this).attr('type')) {
					$(this).attr('type', 'password');
				}
			});
		});
	});
</script>
