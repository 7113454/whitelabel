<?php
/*
Plugin Name: Import UFC
Plugin URI: 
Description: Import UFC events, twice daily. Updates previously imported events when they changes. http://ufc-data-api.ufc.com/
Author: ub.fitness
Version: 1.1
Author URI: 
*/

register_activation_hook(__FILE__, 'ufc_activation');
function ufc_activation() {
	if ( ! wp_next_scheduled ('ufc_cronjob')) {
		wp_schedule_event(time(), 'twicedaily', 'ufc_cronjob');
	}
}

register_deactivation_hook(__FILE__, 'ufc_deactivation');
function ufc_deactivation() {
	wp_clear_scheduled_hook('ufc_cronjob');
}

add_action('ufc_cronjob', 'ufc_run');
// main function
function ufc_run() {
	global $wpdb;

	// changing user to admin, otherwise video will be filtered out
	wp_set_current_user(1);

	// includes for media_sideload_image()
	require_once(ABSPATH . 'wp-admin/includes/media.php');
	require_once(ABSPATH . 'wp-admin/includes/file.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');

	remove_filter('wp_insert_post', 'ub_custom_fields');

	$data = file_get_contents('http://ufc-data-api.ufc.com/api/v3/iphone/events');
//	$data = file_get_contents('/home/misc/www/events.json');
	$data = json_decode($data);
	if (null === $data) {
		trigger_error('UFC Import: cannot deserialize data.', E_USER_WARNING);
		return false;
	}

	$data = array_reverse($data); // start from oldest

	$cnt = 0;
	$now = time();

	$sql = "SELECT id.meta_value AS '_ufc_id', date.meta_value AS '_ufc_last_modified', id.post_id "
		. "FROM {$wpdb->postmeta} AS id "
		. "LEFT JOIN {$wpdb->postmeta} AS date ON id.post_id = date.post_id AND date.meta_key = '_ufc_last_modified' "
		. "WHERE id.meta_key = '_ufc_id'";
	$imported = $wpdb->get_results($sql, OBJECT_K);

	foreach ($data as $e) {
		$is_imported = array_key_exists($e->id, $imported);
		$is_updated = $is_imported && strtotime($e->last_modified) > $imported[$e->id]->_ufc_last_modified;
		if ( ! $is_updated && $is_imported) continue; // skip imported events
		if ( ! $is_updated && strtotime($e->end_event_dategmt) < $now) continue; // skip finished events

		$post_id = $is_updated ? $imported[$e->id]->post_id : 0;
		$id = ufc_import($e, $post_id);

		$cnt++; if ($cnt > 9) break;
	}
}

/**
 * insert or update event
 *
 * @param stdClass $event
 * @param int $post_id
 * @return int
 */
function ufc_import($event, $post_id = 0) {
	$date_start = strtotime($event->event_dategmt);

	$video = $event->trailer_url
		? '<p><video controls preload="auto" width="640" height="264"><source src="'.$event->trailer_url.'" type="video/mp4"></video></p>'
		: '';

	$post = array(
		'ID' => $post_id,
		'post_title' => $event->base_title,
		'post_content' => $video . $event->short_description,
		'post_author' => 639,
		'post_status' => 'publish',
		'meta_input' => array(
			'_ufc_id' => $event->id,
			'_ufc_last_modified' => strtotime($event->last_modified),
			'date' => $date_start,
			'date_end' => strtotime($event->end_event_dategmt),
			'info_url' => 'http://www.ufc.com/',
		),
	);

	// do not add to title "TBA vs TBD"
	if ( ! preg_match('/TBD|TBA|TBC/', $event->title_tag_line)) {
		$post['post_title'] .= ': ' . str_replace(' vs ', ' v ', $event->title_tag_line);
	}

	$post['post_title'] .= ' - ' . date('jS F Y', $date_start);

	$id = $post_id ? wp_update_post($post) : wp_insert_post($post);

	if ($id && empty($post_id)) {
		wp_set_post_categories($id, ufc_default_category());
		wp_set_post_tags($id, array('UFC', 'MMA', 'Ultimate Fighter'));

		if ($event->location) {
			$addr = ufc_geocode($event->location);
		}
		elseif (isset($event->latitude)) {
			$addr = ub_reverce_geocode($event->latitude, $event->longitude);
		}

		// set post location and "city" taxonomy
		if ($addr) {
//			$tags = tax_city_id($addr['country'], $addr['city']);
//			wp_set_post_terms($id, $tags, 'city');

			$location = array(
				'address' => esc_html($addr['formatted_address']),
				'lat' => esc_html($addr['lat']),
				'lng' => esc_html($addr['lng'])
			);
			update_post_meta($id, 'location', $location);
		}

		// remove old featured image
		if ($post_id && $thumb_id = get_post_thumbnail_id($post_id)) {
			wp_delete_attachment($thumb_id, true);
		}

		// set featured image
		$img = media_sideload_image($event->feature_image, $id, 'ufc');
		if ( ! is_wp_error($img)) {
			$media = get_attached_media('image', $id);
			if ( ! empty($media)) set_post_thumbnail($id, key($media));
		}
	}

	return $id;
}

/**
 * get coordinates based on address
 *
 * @param string $address
 */
function ufc_geocode($address) {
	$url = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . GOOGLE_API_KEY . '&address=' . urlencode($address);
	$json = @file_get_contents($url);
	$data = json_decode($json);


	if($data->status == "OK") {
		$out = array('formatted_address' => $data->results[0]->formatted_address);

 		foreach ($data->results[0]->address_components as $c) {
			if (in_array('locality', $c->types)) {
				$out['city'] = $c->long_name;
			}
			elseif (in_array('country', $c->types)) {
				$out['country'] = $c->long_name;
			}
		}

		$out['lat'] = $data->results[0]->geometry->location->lat;
		$out['lng'] = $data->results[0]->geometry->location->lng;
	}
	else {
		$out = false;
	}

	return $out;
}

// default category for imported events
function ufc_default_category() {
	require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');
	$id = wp_create_category('UFC');
	return array($id);
}
