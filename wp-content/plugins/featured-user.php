<?php
/*
Plugin Name: Featured User
Plugin URI: 
Description: Create featured authors. Hooks: [featured-users number="10"]
Author: ub.fitness
Version: 1.0
Author URI: 
*/

function ub_add_roles() {
	$capabilities = array(
		'delete_posts' => true,
		'delete_published_posts' => true,
		'edit_posts' => true,
		'edit_published_posts' => true,
		'publish_posts' => true,
		'read' => true,
		'upload_files' => true,
	);
	add_role('featured', 'Featured', $capabilities);
}
register_activation_hook(__FILE__, 'ub_add_roles' );

add_shortcode('featured-users', 'featured_users');
function featured_users($attr) {
	global $authordata;
	$old_authordata = $authordata;
	$args = array(
		'role' => 'featured',
		'number' => 10,
		'orderby' => 'display_name',
	);

	$args = array_merge($args, $attr);
	$users = get_users($args);

	ob_start();
	if ($users) {
		foreach ($users as $user) {
			$authordata = $user;
			include(get_stylesheet_directory() . '/content-user.php');
		}
	}
	else {
		get_template_part('content', 'none');
	}

	$authordata = $old_authordata;
	return ob_get_clean();
}
