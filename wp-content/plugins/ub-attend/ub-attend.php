<?php
/*
Plugin Name: UB Attend
Plugin URI: 
Description: Add "Attending" button
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'at_install');
function at_install() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE `wp_attend` (
		  post_id bigint(20) NOT NULL,
		  user_id bigint(20) NOT NULL,
		  date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  UNIQUE KEY post_id_user_id (post_id, user_id)
		) $charset_collate;";

	$wpdb->query($sql);
}

function is_attending($post, $user) {
	global $wpdb;

	// don't want to create even more SQL queries :-/
//	$post = isset($post->ID) ? $post : get_post($post);
//	$user = isset($user->ID) ? $user : get_user_by('ID', $user);

	if (is_object($post) && is_object($user)) {
		$sql = "SELECT * FROM {$wpdb->prefix}attend WHERE post_id = {$post->ID} AND user_id = {$user->ID}";
	}
	else {
		$sql = "SELECT * FROM {$wpdb->prefix}attend WHERE post_id = {$post} AND user_id = {$user}";
	}

	return !! $wpdb->get_var($sql);
}

function attend_btn($post) {
	$user = wp_get_current_user();

	if ( ! $user->ID) return false;

	$post = isset($post->ID) ? $post : get_post($post);
	$active = is_attending($post, $user) ? 'active' : '';
	$out = sprintf('<button class="btn btn-default btn-attend %s" data-post="%d" title="I am thinking about attending"><i class="fa fa-check"></i></button>', $active, $post->ID);
	return $out;
}

add_action('wp_ajax_attending', 'ub_attending');
function ub_attending() {
	global $wpdb;

	$user = wp_get_current_user();
	$post = get_post($_POST['post_id']);
	$out = array('attending' => false, 'code' => 0);

	if ( ! $user->ID || ! $post) wp_send_json(array('code' => 1)); // die here

	$data = array('post_id' => $post->ID, 'user_id' => $user->ID);
	if (is_attending($post, $user)) {
		do_action('attending_remove', $post->ID);
		$wpdb->delete($wpdb->prefix . 'attend', $data);
	}
	else {
		do_action('attending_add', $post->ID);
		$out['attending'] = true;
		$data['date'] = current_time('mysql');
		$wpdb->insert($wpdb->prefix . 'attend', $data);
	}

	wp_send_json($out);
}

function api_attending(WP_REST_Request $request) {
	$_POST['post_id'] = $request['id'];
	ub_attending();
}

add_filter('rest_post_query', 'attending_filter', 10, 2);
function attending_filter($args, $request) {
	global $wpdb;
	$user = wp_get_current_user();

	// show attending activities
	if (isset($request['ub_filter']) && 'attending' == $request['ub_filter']) {
		$posts = $wpdb->get_col("SELECT post_id FROM {$wpdb->prefix}attend WHERE user_id = {$user->ID}");
		if ($posts) {
			$args['post__in'] = $posts;
		}
		else {
			$args['post__in'] = array(0);
		}
	}

	return $args;
}

add_action('init', 'at_endpoints');
function at_endpoints(){
    add_rewrite_endpoint('attending', EP_PERMALINK);
}

add_filter('template_include', 'activity_attending');
function activity_attending($tmpl)
{
	global $wp_query;
    if(is_singular() && array_key_exists('attending', $wp_query->query_vars))
    {
		$tmpl = get_stylesheet_directory() . '/post-attending.php';
    }

	return $tmpl;
}

function attending_members($post) {
	global $wpdb;

	$sql = "SELECT user_id FROM {$wpdb->prefix}attend WHERE post_id = {$post->ID}";
	$ids = $wpdb->get_col($sql);

    $args = array(
        'include' => $ids ? $ids : array(0), // make empty result if no ids
    );
    $q = new WP_User_Query($args);
	return $q;
}

function api_attending_members(WP_REST_Request $request) {
	global $wpdb;

	$sql = $wpdb->prepare("SELECT user_id FROM {$wpdb->prefix}attend WHERE post_id = %d", $request['id']);
	$ids = $wpdb->get_col($sql);

	return $ids;
}

add_shortcode('my_attendings', 'my_attendings');
function my_attendings($attr) {
	global $wpdb;
	ob_start();
	$user = wp_get_current_user();

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$posts_per_page = isset($attr['posts_per_page']) ? $attr['posts_per_page'] : get_option('posts_per_page');

	$sql = $wpdb->prepare("SELECT post_id FROM {$wpdb->prefix}attend WHERE user_id = %d", $user->ID);
	$ids = $wpdb->get_col($sql);
	$ids = $ids ? $ids : array(0); // make empty result if no ids

	$args = array('post__in' => $ids, 'posts_per_page' => $posts_per_page, 'paged' => $paged);
	$q = new WP_Query($args);

	while ($q->have_posts()) {
		$q->the_post();
		get_template_part('content', 'favourite-author-posts');
	}

	echo '<span class="pager next">' . get_next_posts_link('Next', $q->max_num_pages) . '</span>';
	wp_reset_postdata();
	return ob_get_clean();
}

add_shortcode('ub_attendees', 'ub_attendees');
function ub_attendees() {
	global $attendees, $post;

	if ( ! is_user_logged_in()) return;

	ob_start();
	$page = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
	$user = wp_get_current_user();

	$args = array('author' => $user->ID, 'posts_per_page' => 10, 'paged' => $page);
	$q = new WP_Query($args);

	while ($q->have_posts()) {
		$q->the_post();
		$attendees = attending_members($post);
		include plugin_dir_path(__FILE__) . 'ub_attendees.php';
	}

	if ( ! $q->post_count) {
		echo '<div class="alert alert-info">No activities listed yet. Why not add one?</div>';
	}

	echo '<span class="pager next">' . get_next_posts_link('Next', $q->max_num_pages) . '</span>';
	wp_reset_postdata();
	return ob_get_clean();
}

