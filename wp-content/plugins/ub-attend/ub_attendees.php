<?php global $post, $attendees; ?>

<div class="attendee-listing">
	<div class="attendee-count"><?= $attendees->total_users ?></div>
	<div class="attendee-details">
		<a href="<?= get_permalink() ?>">
			<div class="attendee-thumb">
				<?php the_post_thumbnail('large', array('class' => 'attendee-tb')) ?>
			</div>
		</a>
		<p><a href="<?= get_permalink() ?>"><?php the_title() ?></a></p>
	</div>

	<p>
		<?php foreach ($attendees->results as $u): ?>
			<a href="<?= get_author_posts_url($u->ID, $u->user_nicename) ?>"><?= get_avatar( $u->ID, 40 ); ?></a>
		<?php endforeach ?>
	</p>
</div>
