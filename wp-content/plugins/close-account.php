<?php
/*
Plugin Name: Close Account
Plugin URI: 
Description: Allow users to delete their account. It's "soft delete" - account does not removed, just marked as "deleted" in admin panel.
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'ca_add_roles' );
function ca_add_roles() {
	$capabilities = array();
	add_role('closed', 'Closed', $capabilities);
}

add_filter('authenticate', 'ca_authenticate', 30);

/**
 * deny deleted user login
 *
 * @param WP_User|WP_Error $user WP_User object if login successful, otherwise WP_Error object.
 */
function ca_authenticate($user) {
	// user successfuly logged in, but have "closed" role
	if ('WP_User' == get_class($user) && isset($user->caps['closed'])) {
		$error = new WP_Error();
		$error->add('closed_account', __('<strong>ERROR</strong>: Account is closed.'));
		return $error;
	}
	return $user;
}

// add_filter('template_include', 'ca_view_closed');
// function ca_view_closed($template) {
// 	global $wp_query;
// 	global $wp_the_query;
// 	global $queried_object;
// 	if (isset($wp_query->query['author_name'])) {
// //		var_dump($queried_object);
// 	}
// 	return $template;
// }
