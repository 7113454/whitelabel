<?php
/*
Plugin Name: UB Search
Plugin URI: 
Description: Search events by location
Author: ub.fitness
Version: 1.1
Author URI: 
*/

register_activation_hook(__FILE__, 'ub_search_activation');
function ub_search_activation() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}post_location");

	$sql = "CREATE TABLE {$wpdb->prefix}post_location (
		`post_id` bigint(20) unsigned NOT NULL,
		`lat` double NOT NULL,
		`lng` double NOT NULL,
		UNIQUE KEY `post_id` (`post_id`)
	) $charset_collate;";
	$wpdb->query($sql);

	ub_populate_location_table();
}

function ub_populate_location_table() {
	global $wpdb;

	$wpdb->query("TRUNCATE TABLE {$wpdb->prefix}post_location");

	$meta = $wpdb->get_results("SELECT post_id, meta_value FROM {$wpdb->postmeta} WHERE meta_key = 'location'");
	$total = count($meta);
	$buff = '';
	foreach ($meta as $k => $m) {
		$addr = unserialize($m->meta_value);
		if ($addr && isset($addr['lat'])) {
			$buff .= sprintf("(%d, '%s', '%s'),", $m->post_id, $addr['lat'], $addr['lng']);
		}

		if ($buff && (! $k % 20 || $k == $total-1) ) {
			$sql = "INSERT INTO {$wpdb->prefix}post_location (post_id, lat, lng) VALUES ";
			$wpdb->query($sql . substr($buff, 0, -1));
			$buff = '';
		}
	}
}

add_action('pre_get_posts', 'ub_modify_search');
function ub_modify_search($query) {
	if (is_admin() || ! $query->is_main_query()) return;
	if (empty($_GET['near'])) return;

	$dist = isset($_GET['dist']) && is_numeric($_GET['dist']) ? $_GET['dist'] : 35;
	$loc = near2latlng($_GET['near']);
	if ($loc) {

		$id = ub_search($loc['lat'], $loc['lng'], $dist);
		$id = $id ? $id : array(0); // fix if no IDs found
		$query->set('post__in', $id);
	}
	else {
		// TODO: show notification if location not found
		$query->set('post__in', array(0));
	}
}

function ub_search($lat, $lng, $dist = 35) {
	global $wpdb;

	$sql = "SELECT post_id, 
		(3959 * acos(cos(radians($lat)) * cos(radians(lat)) * cos(radians(lng) - radians($lng)) + sin(radians($lat)) * sin(radians(lat)))) AS distance
		FROM {$wpdb->prefix}post_location
		HAVING distance < $dist";

	return $wpdb->get_col($sql);
}

function ub_search_form() {
	$dist = [10, 30, 50, 70, 100, 150];
	require plugin_dir_path(__FILE__) . 'search-form.php';
}

// update the locations table
add_action('added_post_meta', 'search_save_location', 10, 4);
add_action('updated_post_meta', 'search_save_location', 10, 4);
function search_save_location($meta_id, $object_id, $meta_key, $meta_value) {
	global $wpdb;
	if ('location' != $meta_key) return;

	if (empty($meta_value['lat'])) {
		$wpdb->delete($wpdb->prefix . 'post_location', array('post_id' => $object_id));
	}
	else {
		$sql = $wpdb->prepare("REPLACE INTO {$wpdb->prefix}post_location VALUES (%d, %s, %s)", $object_id, $meta_value['lat'], $meta_value['lng']);
		$wpdb->query($sql);
	}
}

add_action('delete_post_meta', 'delete_post_location', 10, 4);
function delete_post_location($meta_ids, $object_id, $meta_key, $_meta_value) {
	global $wpdb;

	if ('location' == $meta_key) {
		$wpdb->delete($wpdb->prefix . 'post_location', array('post_id' => $object_id));
	}
}

// mod for "WP REST API" plugin
add_filter('rest_post_query', 'ub_api_search', 10, 2);
function ub_api_search($args, $request) {
	$near = $request->get_param('near');
	$dist = $request->get_param('dist') && is_numeric($request->get_param('dist')) ? $request->get_param('dist') : 35;

	if ($near) {
		$loc = near2latlng($near);
		if ($loc) {

			$id = ub_search($loc['lat'], $loc['lng'], $dist);
			$id = $id ? $id : array(0); // fix if no IDs found
			$args['post__in'] = $id;
		}
		else {
			$args['post__in'] = array(0);
		}
	}
	return $args;
}

function near2latlng($near) {
	if (preg_match('/^\-?\d+(\.\d+)?,\-?\d+(\.\d+)?$/', $near)) {
		$buf = explode(',', $near);
		$out = array('lat' => $buf[0], 'lng' => $buf[1]);
	}
	else {
		$out = ub_geocode($near);
	}
	return $out;
}

// sync form state between devices
add_action('wp_ajax_toggle_search_form', 'toggle_search_form');
function toggle_search_form() {
	$user_id = get_current_user_id();
	empty($_POST['showSearch']) ? delete_user_meta($user_id, 'showSearch') : update_user_meta($user_id, 'showSearch', 1);
	wp_send_json(['msg' => 'ok']);
}

// sync showSearch cookie between devices
add_action('init', 'ub_sync_search_form');
function ub_sync_search_form() {
	$user_id = get_current_user_id();
	if ( ! $user_id || 'POST' == $_SERVER['REQUEST_METHOD']) return;

	$meta = get_user_meta($user_id, 'showSearch', true);
	if ($meta && empty($_COOKIE['showSearch'])) {
		setcookie('showSearch', 1, strtotime('+1 month'), '/');
		$_COOKIE['showSearch'] = 1;
	}
	elseif ( ! $meta && ! empty($_COOKIE['showSearch'])) {
		setcookie('showSearch', '', 1, '/');
		$_COOKIE['showSearch'] = 0;
	}
}