<form method="get" id="ub-search" action="<?= site_url() ?>">
	<label>
		Find: <input type="text" name="s" placeholder="Search" value="<?= isset($_GET['s']) ? esc_html($_GET['s']) : '' ?>">
	</label>
	<label class="near">
		Near: <input type="text" name="near" placeholder="Location" value="<?= isset($_GET['near']) ? esc_html($_GET['near']) : '' ?>">
	</label>
	<label>
		<select name="dist">
			<?php foreach ($dist as $d): ?>
				<?php $sel = empty($_GET['dist']) && 30 == $d ? 'selected' : (isset($_GET['dist']) && $d == $_GET['dist'] ? 'selected' : '') ?>
				<option value="<?= $d ?>" <?= $sel ?>><?= $d ?> mi</option>
			<?php endforeach ?>
		</select>
	</label>
	<button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>