jQuery(function($) {

	$('#prev-messages').on('click', function() {
		var btn = $(this);
		var btn_text = btn.text();
		var page = btn.data('page');
		page = 'undefined' == typeof page ? 2 : page+1;
		btn.prop('disabled', true);
		btn.html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');

		var data = {
			"action": "get_thread",
			"thread": getUrlParameter('thread'),
			"page": page
		};

		$.get(ajaxurl, data, function(r) {
			btn.data('page', page);
			btn.prop('disabled', false);
			btn.text(btn_text);

			if (0 == r.total_pages || page == r.total_pages) {
				btn.hide();
			}

			var html = $("<div>"+r.html+"</div>");
			$('#thread').prepend(html.find('#thread tr'));
		});
	});

	// recipient autocomplete
	$('#msgRecipientInput').autocomplete({
		"source": ajaxurl+'?action=rcpt_autocomplete',
		"minLength": 3,
		"select": function(e, ui) {
			$('#msgRecipientInput').val(ui.item.user_login);
			// $('#msgRecipientUser').val(ui.item.user_login);
			return false;
		},
		"response": function(e, ui) {
			found_users = [];
			for (var i in ui.content) {
				found_users.push(ui.content[i].user_login);
			}
		}
	})
	.autocomplete( "instance" )._renderItem = function(ul, item) {
		var html = "<li>" + item.display_name + " (" + item.user_login + ")</li>";
		return $(html).appendTo(ul);
	};

	// check is recipient login valid
	$('#msgRecipientInput').on('blur', function() {
		var user = $(this).val();
		if (-1 == found_users.indexOf(user)) {
			$('#msgRecipientInput').val('');
			// $('#msgRecipientUser').val('');
		}
	});

	$('#btnSubmitMessage').on('click', function() {
		var btn = $(this).prop('disabled', true);
		var form = $(this).closest('form');
		var data = form.serializeArray();
		data.push({name: "action", value: "msg_ajax_send"});

		$('#ub-msg-form .alert').remove(); // remove previously created alerts

		$.post(ajaxurl, data, function(r) {
			btn.prop('disabled', false);
			if ( typeof r.error !== 'undefined' && r.error) {
				var html = '<div class="alert alert-dismissible alert-danger">' + r.msg + '</div>';
				html = $(html).prepend('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#ub-msg-form .modal-body').append(html);
			}
			else {
				form.find('textarea').val('');
				location.reload(false);
			}
		});
	});

	$('.send-message').on('click', function() {
		var form = $(this).closest('form');
		var data = form.serializeArray();
		var btn = $(this).prop('disabled', true);

		$.post(ajaxurl, data, function(r) {
			btn.prop('disabled', false);
			if (typeof r.error !== 'undefined') {
				// error
			}
			else {
				add_message(r);
				form.find('textarea').val('');
				$('#msgAttachList').html('');
			}
		});
	});

	$(document).on('change', '.select-msg', function() {
		if ($('.select-msg:checked').length) $('#delete-msg-btn').show();
		else $('#delete-msg-btn').hide();
	});

	$('#delete-msg-btn').on('click', function() {
		if ( ! confirm('Are you sure?')) return;

		var data = {"action": "msg_delete", "id": []};
		$('.select-msg:checked').each(function(i) {
			data.id[i] = $(this).val();
		});
		$.post(ajaxurl, data, function(r) {
			location.reload();
		});
	});

	$('#msgAttachFile').on('change', function() {
		if ($(this).val()) $(this).closest('form').trigger('submit');
	});

	$('#msgAttachBtn').on('click', function() {
		$('#msgAttachFile').val('').trigger('click');
	});

	// delete attached file BEFORE message submission
	$(document).on('click', '.btnAttachUndo', function(e) {
		e.preventDefault();
		var id = $(this).parent().find('input[name="files[]"]').val();
		$(this).parent().remove();
		$.post(ajaxurl, {action: "attach_delete", id: id});
	});

	$('#msgAttachForm').ajaxForm({
		success: function(r) {
			if (typeof r.error == 'undefined') {
				var html = '<div class="file" id="file_'+r.id+'"><a href="'+r.url+'" target="_blank"><span class="name">'+r.name+'</span></a> '
				 + '<span class="size">('+r.size+')</span>'
				 + '<a href="#" class="btnAttachUndo" title="Delete"><i class="fa fa-times-circle"></i></a>'
				 + '<input type="hidden" name="files[]" value="'+r.id+'"></div>';
				$('#msgAttachList').append(html);
			}
			else {
				$.notify(r.error, 'error');
			}
		}
	});
});

var found_users = [];
