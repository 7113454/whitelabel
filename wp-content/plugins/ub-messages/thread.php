<?php
if( ! defined('ABSPATH')) die('You are not allowed to call this page directly.');

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$ppage = 10;
$messages = get_thread($_GET['thread'], ['page' => $page, 'ppage' => $ppage]);
$messages = array_reverse($messages);
$total_pages = ceil(sql_found_rows()/$ppage);

$thread = msg_fix_thread($_GET['thread']);
$ids = explode(',', $thread);
$rcpt = $ids[0] == get_current_user_id() ? get_user_by('id', $ids[1]) : get_user_by('id', $ids[0]);
?>

<?php if ($total_pages > 1): ?>
	<button id="prev-messages" type="button" class="btn btn-default btn-block">Previous messages</button>
<?php endif ?>

<table id="thread" class="table messages">
<?php foreach ($messages as $m): ?>
	<?php $user = get_user_by('id', $m->sender_id) ?>
	<tr class="<?= $m->read ? '' : 'new' ?>">
		<td class="min"><input type="checkbox" name="id[]" value="<?= $m->id ?>" class="select-msg"></td>
		<td class="user"><?= $user->display_name ?></td>
		<td class="msg">
			<?= $m->text ?>

			<?php foreach ($m->files as $f): ?>
				<?php if (0 === strpos($f['mime'], 'image/')): ?>
					<div class="file"><a href="<?= $f['url'] ?>" target="_blank"><img src="<?= $f['url'] ?>" class="img-rounded"></a></div>
				<?php else: ?>
					<div class="file"><a href="<?= $f['url'] ?>" target="_blank"><?= $f['name'] ?></a></div>
				<?php endif ?>
			<?php endforeach ?>
		</td>
		<td class="date"><span title="<?= $m->date ?>"><?= date('g:i A', strtotime($m->date)) ?></span></td>
	</tr>
<?php endforeach ?>
</table>

<form method="post" action="<?= admin_url("admin-ajax.php") ?>" class="panel panel-default">
	<div class="panel-body">
		<textarea name="text" class="form-control" rows="3"></textarea>
		<div id="msgAttachList"></div>
	</div>
	<div class="panel-footer">
		<input type="hidden" name="action" value="msg_ajax_send">
		<input type="hidden" name="recipient" value="<?= $rcpt->user_login ?>">
		<button type="button" class="btn btn-primary send-message">Send</button>
		<button type="button" id="msgAttachBtn" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach...</button>
	</div>
</form>

<form id="msgAttachForm" method="post" action="<?= admin_url("admin-ajax.php") ?>" class="hidden" enctype="multipart/form-data">
	<input type="hidden" name="action" value="msg_save_file">
	<input type="file" name="file" id="msgAttachFile">
</form>