<?php
if( ! defined('ABSPATH')) die('You are not allowed to call this page directly.');

$messages = get_messages_list();
?>

<?php if (empty($messages)): ?>

<?php else: ?>
	<table id="ub-messages" class="table messages">
	<?php foreach ($messages as $m): ?>
		<?php $user = get_current_user_id() == $m->sender_id ? get_user_by('id', $m->recipient_id) : get_user_by('id', $m->sender_id) ?>
		<tr class="<?= $m->read || $m->sender_id == get_current_user_id() ? '' : 'new' ?>">
			<td class="min"><input type="checkbox" name="id[]" value="<?= $m->thread ?>" class="select-msg"></td>
			<td class="info">
				<a href="<?= messages_page_url('?thread='.$m->thread) ?>" class="link">&nbsp;</a>
				<span class="user"><?= $user->display_name ?></span>
				<div class="msg"><?= wp_trim_words($m->text, 15) ?></div>
			</td>
			<td class="min"><span class="date"><?= ago(strtotime($m->date)) ?></span></td>
		</tr>
	<?php endforeach ?>
	</table>
<?php endif ?>