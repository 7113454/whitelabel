<script src="<?= plugins_url('/ub-messages/msg.js') ?>"></script>
<p>
	<!-- <button id="select-all-msg" class="btn btn-default"><i class="fa fa-check-square-o" aria-hidden="true"></i></button> -->
	<button class="btn btn-default" data-toggle="modal" data-target="#ub-msg-form">Compose</button>
	<a href="<?= messages_page_url() ?>" class="btn btn-default <?= empty($_GET['thread']) ? 'active' : '' ?>">Messages</a>
	<button id="delete-msg-btn" type="button" class="btn btn-danger">Delete</button>
</p>

<?php
if (isset($_GET['thread'])) {
	include($dir . 'thread.php');
}
elseif (isset($_GET['compose'])) {
	include($dir . 'compose.php');
}
else {
	include($dir . 'index.php');
}
?>

<div class="alert alert-info"><strong>Tip:</strong> enable push <a href="/notifications">notifications</a> for real time chat.</div>

<form id="ub-msg-form" method="post" action="" class="modal fade ui-front">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">New Message</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Recipient</label>
					<input type="text" id="msgRecipientInput" class="form-control" name="recipient" value="<?= isset($rcpt) ? $rcpt->user_login : '' ?>">
				</div>
				<div class="form-group">
					<label>Message</label>
					<textarea name="text" class="form-control" rows="3"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button id="btnSubmitMessage" type="button" class="btn btn-primary">Send</button>
			</div>
		</div>
	</div>
</form>
