<?php

class REST_Messages_Controller extends WP_REST_Controller {

	public function __construct() {
		$this->namespace = 'ub';
		$this->rest_base = 'msg';
	}

	/**
	 * Registers the routes for the objects of the controller.
	 *
	 * @see register_rest_route()
	 */
	public function register_routes() {

		register_rest_route( $this->namespace, '/' . $this->rest_base, array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
//				'args'                => $this->get_collection_params(),
			),
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'callback'            => array( $this, 'create_item' ),
				'permission_callback' => array( $this, 'create_item_permissions_check' ),
//				'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
			),
			'schema' => array( $this, 'get_public_item_schema' ),
		));

		register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<id>[\d]+)', array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item' ),
			),
			array(
				'methods'             => WP_REST_Server::DELETABLE,
				'callback'            => array( $this, 'delete_item' ),
//				'permission_callback' => array( $this, 'delete_item_permissions_check' ),
			),
		));
	}

	public function get_items_permissions_check($request) {
		if (0 == get_current_user_id()) {
			return new WP_Error( 'rest_authorization_required', 'Sorry, you are not authorized.', ['status' => rest_authorization_required_code()]);
		}
		return true;
	}

	public function get_items($request) {
		$page = isset($request['page']) ? (int)$request['page'] : 1;
		$ppage = isset($request['per_page']) ? (int)$request['per_page'] : 10;

		if ($request['thread']) {
			$pre = get_thread($request['thread'], ['page' => $page, 'ppage' => $ppage]);
			$total = sql_found_rows();
			$total_pages = ceil($total/$ppage);
		}
		else {
			$pre = get_messages_list();
			$total = count($pre);
			$total_pages = 1;
		}

		$messages = array();
		foreach ($pre as $m) {
			$messages[] = $this->prepare_item_for_response($m, $request);
		}

		$response = rest_ensure_response($messages);
		$response->header('X-WP-Total', $total);
		$response->header('X-WP-TotalPages', $total_pages);

		// add prev/next urls
		$base = add_query_arg($request->get_query_params(), rest_url(sprintf( '%s/%s', $this->namespace, $this->rest_base)));
		if ( $page > 1 ) {
			$prev_page = $page - 1;

			if ( $prev_page > $total_pages ) {
				$prev_page = $total_pages;
			}

			$prev_link = add_query_arg( 'page', $prev_page, $base );
			$response->link_header( 'prev', $prev_link );
		}
		if ( $total_pages > $page ) {
			$next_page = $page + 1;
			$next_link = add_query_arg( 'page', $next_page, $base );

			$response->link_header( 'next', $next_link );
		}

		return $response;
	}

	public function prepare_item_for_response($msg, $request) {
		$sender = get_user_by('id', $msg->sender_id);
		$rcpt = get_user_by('id', $msg->recipient_id);

		unset($msg->delete, $msg->sender_id, $msg->recipient_id);

		$msg->sender = [
			'id' => $sender->ID,
			'name' => $sender->display_name,
			'avatar' => rest_get_avatar_urls($sender->ID),
		];
		$msg->recipient = [
			'id' => $rcpt->ID,
			'name' => $rcpt->display_name,
			'avatar' => rest_get_avatar_urls($rcpt->ID),
		];

		return $msg;
	}

	public function create_item_permissions_check($request) {
		if (0 == get_current_user_id()) {
			return new WP_Error( 'rest_authorization_required', 'Sorry, you are not authorized.', ['status' => rest_authorization_required_code()]);
		}
		return true;
	}

	public function create_item($request) {
		$rcpt = get_user_by('id', $request['recipient_id']);

		if (empty($rcpt)) {
			return new WP_Error( 'rest_invalid_rcpt', 'Invalid recipient.', ['status' => 400]);
		}

		try {
			$id = msg_save($rcpt->user_login, $request['text'], $request['attach']);
			$out = $this->get_item(['id' => $id]);
		} catch (Exception $e) {
			$out = new WP_Error( 'submission_error', $e->getMessage(), ['status' => 400]);
		}
		return $out;
	}

	public function delete_item($request) {
		if (0 == get_current_user_id()) {
			return new WP_Error( 'rest_authorization_required', 'Sorry, you are not authorized.', ['status' => rest_authorization_required_code()]);
		}
		
		$ok = msg_delete_own($request['id']);

		return $ok ? ['OK'] : ['Something wrong'];
	}

	public function get_item($request) {
		$msg = msg_get_own($request['id']);

		if ( ! $msg) {
			return new WP_Error('rest_invalid_id', __('Invalid ID.'), array('status' => 404));
		}

		return $this->prepare_item_for_response($msg, $request);
	}
}
