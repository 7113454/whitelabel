<?php
/*
Plugin Name: Messages
Plugin URI: 
Description: Private Messages. Use [ub-messages] shortcode.
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'msg_activation');
function msg_activation() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}messages` (
	`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	`sender_id` bigint(20) unsigned NOT NULL,
	`recipient_id` bigint(20) unsigned NOT NULL,
	`thread` varchar(100) NOT NULL,
	`text` text NOT NULL,
	`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`read` tinyint(3) unsigned NOT NULL DEFAULT '0',
	`delete` set('sender','recipient') NOT NULL,
	PRIMARY KEY (`id`),
	KEY `thread` (`thread`)
	) $charset_collate;";
	$wpdb->query($sql);

	$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}messages_attach` (
		`id` varchar(15) NOT NULL,
		`msg_id` bigint(20) unsigned DEFAULT NULL,
		`user_id` bigint(20) unsigned NOT NULL,
		`name` varchar(255) DEFAULT NULL,
		`file` varchar(255) NOT NULL,
		`size` int(10) unsigned NOT NULL DEFAULT '0',
		`mime` varchar(129) NOT NULL,
		`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`),
		KEY `msg_id` (`msg_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$wpdb->query($sql);
}

register_uninstall_hook(__FILE__, 'msg_uninstall');
function msg_uninstall() {
	global $wpdb;

	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}messages");
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}messages_attach");
}

add_shortcode('ub-messages', 'ub_messages_shortcode');
function ub_messages_shortcode() {
	$user = wp_get_current_user();
	if ( ! $user->ID) return;

	$dir = plugin_dir_path( __FILE__ );
	ob_start();

	include($dir . 'layout.php');

	return ob_get_clean();
}

function get_messages_list() {
	global $wpdb;
	$ppage = 50;

	$user = wp_get_current_user();
	$sql = "SELECT MAX(id) FROM {$wpdb->prefix}messages
		WHERE {$user->ID} IN (`sender_id`, `recipient_id`) AND NOT(IF({$user->ID} = sender_id, 1, 2) & `delete`)
		GROUP BY thread LIMIT $ppage";
	$ids = $wpdb->get_col($sql);

	if ($ids) {
		$sql = sprintf("SELECT * FROM {$wpdb->prefix}messages WHERE id IN (%s) ORDER BY id DESC", implode(',', $ids));
		$messages = $wpdb->get_results($sql);
	}
	else {
		$messages = array();
	}
	return $messages;
}

/**
 * get chat with a person
 *
 * @param $thread_id mixed Could be string as thread id ("3,1") or integer as member id
 * @param $atts array attributes, see the $dflt
 */
function get_thread($thread_id, $atts = false) {
	global $wpdb;

	$dflt = [
		'ppage' => 10,
		'page' => 1,
	];
	$prop = array_merge($dflt, $atts);

	$start = ($prop['page']-1) * $prop['ppage'];
	$user = wp_get_current_user();
	$thread = msg_fix_thread($thread_id);
	if ( ! $thread) return array();

	$sql = "SELECT SQL_CALC_FOUND_ROWS id, sender_id, recipient_id, thread, text, `date`, `read`
		FROM {$wpdb->prefix}messages
		WHERE thread = %s AND NOT(IF(%d = sender_id, 1, 2) & `delete`)
		ORDER BY id DESC
		LIMIT $start,{$prop['ppage']}";
	$sql = $wpdb->prepare($sql, $thread, $user->ID);

	$messages = $wpdb->get_results($sql, OBJECT_K);

	if ($messages) {
		$sql = sprintf("SELECT * FROM {$wpdb->prefix}messages_attach WHERE msg_id IN(%s)", implode(',', array_keys($messages)));
		$attachments = $wpdb->get_results($sql);
	}

	// update unread messages
	$unread = [];
	foreach ($messages as $k => $m) {
		if ($user->ID == $m->recipient_id && empty($m->read)) {
			$unread[] = $m->id;
		}

		$messages[$k]->files = [];
		foreach ($attachments as $a) {
			if ($m->id != $a->msg_id) continue;
			$messages[$k]->files[] = [
				'id' => $a->id,
				'name' => basename($a->file),
				'size' => $a->size,
				'url' => sprintf('%s?action=msg_file&id=%s', admin_url("admin-ajax.php"), $a->id),
				'mime' => $a->mime,
			];
		}
	}
	if ($unread) {
		$sql = sprintf("UPDATE {$wpdb->prefix}messages SET `read` = 1 WHERE id IN(%s)", implode(',', $unread));
		$wpdb->query($sql);

		do_action('message_read', $unread);
	}

	return $messages;
}

function msg_fix_thread($thread) {
	$user = wp_get_current_user();
	$ids = explode(',', $thread);

	if (count($ids) == 2 && ! in_array($user->ID, $ids)) return false;
	elseif (count($ids) == 2) return build_thread_id($ids[0], $ids[1]);
	elseif (ctype_digit($thread)) return build_thread_id($user->ID, $thread);
}

function sql_found_rows() {
	global $wpdb;
	return $wpdb->get_var("SELECT FOUND_ROWS()");
}

function messages_page_url($append = '') {
	return site_url('messages' .  $append);
}

add_action('wp_ajax_get_thread', 'ajax_get_thread');
function ajax_get_thread() {
	$out = [];

	ob_start();
	include plugin_dir_path( __FILE__ ) . 'thread.php';
	$out['html'] = ob_get_clean();
	$out['total_pages'] = $total_pages;
	wp_send_json($out);
}

add_action('wp_ajax_rcpt_autocomplete', 'rcpt_autocomplete');
function rcpt_autocomplete() {
	global $wpdb;

	$out = ['need more chars'];
	$term = isset($_GET['term']) ? $_GET['term'] : '';

	if (strlen($term) > 2) {
		$sql = "SELECT ID, display_name, user_login FROM {$wpdb->users} WHERE display_name LIKE %s AND ID != %d LIMIT 10";
		$sql = $wpdb->prepare($sql, '%'.$term.'%', get_current_user_id());
		$out = $wpdb->get_results($sql);
	}
	wp_send_json($out);
}

add_action('wp_ajax_msg_ajax_send', 'msg_ajax_send');
function msg_ajax_send() {
	$out = ['error' => 0, 'msg' => 'Message sent.'];
	try {
		$files = isset($_POST['files']) ? $_POST['files'] : null;
		$id = msg_save($_POST['recipient'], $_POST['text'], $files);

		$out = (array)msg_get_own($id);
		$out = msg_prepare_for_response($out);
	} catch (Exception $e) {
		$out = ['error' => 1, 'msg' => $e->getMessage()];
	}
	wp_send_json($out);
}

function msg_prepare_for_response($msg) {
	$sender = get_user_by('id', $msg['sender_id']);
	$rcpt = get_user_by('id', $msg['recipient_id']);

	$msg['sender'] = ['id' => $sender->ID, 'name' => $sender->display_name];
	$msg['recipient'] = ['id' => $rcpt->ID, 'name' => $rcpt->display_name];
	unset($msg['sender_id'], $msg['recipient_id']);

	return $msg;
}

add_action('wp_ajax_msg_delete', 'msg_ajax_delete');
function msg_ajax_delete() {
	global $wpdb;

	$user = wp_get_current_user();

	foreach ($_POST['id'] as $id) {
		$sql = "UPDATE {$wpdb->prefix}messages SET `delete` = `delete` | IF(%d = sender_id, 1, 2) WHERE %s IN (id, thread) AND %d IN(sender_id, recipient_id)";
		$sql = $wpdb->prepare($sql, $user->ID, $id, $user->ID);
		$wpdb->query($sql);
	}
	wp_send_json(['OK']);
}

/**
 * submit private message
 *
 * @param string $rcpt_login recipient login
 * @param string $text
 * @param string|array $attach_id
 */
function msg_save($rcpt_login, $text, $attach_id = '') {
	global $wpdb;
	$sender = wp_get_current_user();
	$rcpt = get_user_by('login', $rcpt_login);

	if (empty($sender->ID)) throw new Exception('Empty message sender.');
	elseif (empty($rcpt->ID)) throw new Exception('Invalid recipient.');
	elseif (empty($text) && empty($attach_id)) throw new Exception('Empty message.');

	$data = [
		'sender_id' => $sender->ID,
		'recipient_id' => $rcpt->ID,
		'thread' => build_thread_id($sender->ID, $rcpt->ID),
		'text' => esc_html($text),
		'date' => current_time('mysql'),
		'read' => 0,
	];

	$wpdb->insert($wpdb->prefix.'messages', $data);
	$data['id'] = $wpdb->insert_id;

	if ($attach_id) link_attachment($data['id'], $attach_id);

	do_action('message_sent', $data);

	return $data['id'];
}

function build_thread_id($from_id, $rcpt_id) {
	return $from_id > $rcpt_id ? "$from_id,$rcpt_id" : "$rcpt_id,$from_id";
}

/**
 * Mark message as deleted
 *
 * @param $id int message id
 *
 * @return int number of affected rows
 */
function msg_delete_own($id) {
	global $wpdb;

	$user = wp_get_current_user();

	$sql = "UPDATE {$wpdb->prefix}messages SET `delete` = IF(sender_id = %d, 1, 2) | `delete` WHERE id = %d AND %d IN(sender_id, recipient_id)";
	$sql = $wpdb->prepare($sql, $user->ID, $id, $user->ID);
	return $wpdb->query($sql);
}

function msg_get_own($id) {
	global $wpdb;

	$user = wp_get_current_user();

	$sql = "SELECT * FROM {$wpdb->prefix}messages WHERE id = %d AND %d IN (sender_id, recipient_id) AND NOT(IF(%d = sender_id, 1, 2) & `delete`)";
	$sql = $wpdb->prepare($sql, $id, $user->ID, $user->ID);
	$msg = $wpdb->get_row($sql);

	if ($msg && $msg->recipient_id == $user->ID && 0 == $msg->read) {
		$wpdb->update($wpdb->prefix.'messages', ['read' => 1], ['id' => $msg->id]);
		do_action('message_read', [$msg->id]);
	}

	// attachments
	if ($msg) {
		$sql = "SELECT * FROM {$wpdb->prefix}messages_attach WHERE msg_id = {$msg->id}";
		$attachments = $wpdb->get_results($sql);

		$msg->files = [];
		foreach ($attachments as $a) {
			$msg->files[] = [
				'id' => $a->id,
				'name' => $a->name,
				'size' => $a->size,
				'url' => sprintf('%s?action=msg_file&id=%s', admin_url("admin-ajax.php"), $a->id),
				'mime' => $a->mime,
			];
		}
	}

	return $msg;
}

function msg_unread_cnt() {
	global $wpdb;

	if ( ! isset($GLOBALS['msg_unread_cnt'])) {
		$user_id = get_current_user_id();
		$sql = $wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}messages WHERE recipient_id = %d AND `read` = 0 AND `delete` ^ 2", $user_id);
		$GLOBALS['msg_unread_cnt'] = $user_id ? $wpdb->get_var($sql) : 0;
	}


	return $GLOBALS['msg_unread_cnt'];
}

function msg_thread_link($user_id) {
	$me = wp_get_current_user();
	if ( ! $me->ID) return;

	$thread = build_thread_id($me->ID, $user_id);
	$url = messages_page_url('?thread=' . $thread);
	return $url;
}

// function test_data() {
// 	global $wpdb;
// 	$date = new DateTime();

// 	for ($i = 10; $i < 50; $i++) {
// 		$date->modify('+1 minute');
// 		$ins = ['text' => $i, 'date' => $date->format('Y-m-d H:i'), 'thread' => '3,1'];

// 		if ($i % 2 == 0) {
// 			$ins['sender_id'] = 3;
// 			$ins['recipient_id'] = 1;
// 		}
// 		else {
// 			$ins['sender_id'] = 1;
// 			$ins['recipient_id'] = 3;
// 		}

// 		$wpdb->insert($wpdb->prefix.'messages', $ins);
// 	}
// }

add_action('wp_ajax_msg_save_file', 'ajax_save_file');
function ajax_save_file() {
	global $wpdb;
	/** Include admin functions to get access to wp_handle_upload() */
	require_once ABSPATH . 'wp-admin/includes/admin.php';

	// replace default WP upload directory and reload cache of wp_upload_dir()
	define('UPLOADS', '../attachments');
	$dir = wp_upload_dir(null, false, true);

	$file = wp_handle_upload($_FILES['file'], ['test_form' => false]);

	if (isset($file['error'])) {
		wp_send_json($file);
	}

	$uid = uniqid();
	$size = filesize($file['file']);

	$sql = "INSERT INTO {$wpdb->prefix}messages_attach (id, user_id, name, file, size, mime, date) VALUES (%s, %d, %s, %s, %d, %s, NOW())";
	$sql = $wpdb->prepare($sql, $uid, get_current_user_id(), esc_html($_FILES['file']['name']), str_replace(trailingslashit($dir['basedir']), '', $file['file']), $size, $file['type']);
	$wpdb->query($sql);

	$out = [
		'id' => $uid,
		'name' => esc_html($_FILES['file']['name']),
		'size' => size_format($size),
		'url' => sprintf('%s?action=msg_file&id=%s', admin_url("admin-ajax.php"), $uid),
	];

	wp_send_json($out);
}

add_action('wp_ajax_msg_file', 'msg_get_file');
function msg_get_file() {
	global $wpdb;

	$sql = "SELECT ma.* FROM {$wpdb->prefix}messages_attach AS ma
		LEFT JOIN {$wpdb->prefix}messages AS m ON ma.msg_id = m.id
		WHERE ma.id = %s AND (msg_id IS NULL OR %d IN (sender_id, recipient_id))";

	$sql = $wpdb->prepare($sql, $_GET['id'], get_current_user_id());
	$attach = $wpdb->get_row($sql);

	if ( ! $attach) {
		status_header(404);
		echo '<html><head><title>404 Not Found</title></head><body bgcolor="white"><center><h1>404 Not Found</h1></center></body></html>';
		die();
	}

	header('Cache-Control: max-age=7776000');
	header('Content-Type: ' . $attach->mime);
	header('Content-Length: ' . $attach->size);
	header('Content-Disposition: inline; filename="'.$attach->name.'"');
	readfile(dirname(ABSPATH) . '/attachments/' . $attach->file);

	wp_die();
}

// delete attached file BEFORE message submission
add_action('wp_ajax_attach_delete', 'msg_ajax_delete_file');
function msg_ajax_delete_file() {
	global $wpdb;

	$sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}messages_attach WHERE id = %s AND msg_id IS NULL", $_POST['id']);
	$attach = $wpdb->get_row($sql);

	if ($attach) {
		$sql = "DELETE FROM {$wpdb->prefix}messages_attach WHERE id = '{$attach->id}'";
		$wpdb->query($sql);
		unlink(dirname(ABSPATH) . '/attachments/' . $attach->file);
	}

	wp_send_json('OK');
}

/**
 * link attachment with message
 *
 * @param int $msg_id message ID
 * @param array|string attachment ID. Could be a string or serveral ID in array
 */
function link_attachment($msg_id, $attach) {
	global $wpdb;

	$attach = (array) esc_sql($attach);
	$sql = "UPDATE {$wpdb->prefix}messages_attach SET msg_id = $msg_id WHERE id IN('".implode("','", $attach)."') AND msg_id IS NULL";
	$wpdb->query($sql);
}

add_action('rest_api_init', 'rest_msg_init');
function rest_msg_init() {
	include plugin_dir_path( __FILE__ ) . 'rest-messages-controller.php';
	$controller = new REST_Messages_Controller;
	$controller->register_routes();

	include plugin_dir_path( __FILE__ ) . 'reset-attach-controller.php';
	$attach = new REST_Message_Attachment_Controller;
	$attach->register_routes();
}
