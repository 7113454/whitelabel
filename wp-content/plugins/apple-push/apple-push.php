<?php
/*
Plugin Name: Apple Push
Plugin URI: 
Description: Notify iOS devices about new activities
Author: ub.fitness
Version: 1.0
Author URI: 
*/

add_action('rest_insert_user', 'ap_register_token', 10, 2);

/**
 * @param WP_User         $user      Data used to create the user.
 * @param WP_REST_Request $request   Request object.
 */
function ap_register_token($user, $request) {
	global $wpdb;

	if ( ! empty($request['ios_token'])) {
		$token = str_replace(' ', '', $request['ios_token']);
		// delete token if exists
		$wpdb->delete($wpdb->usermeta, array('meta_key' => 'ios_token', 'meta_value' => $token));

		add_user_meta($user->ID, 'ios_token', $token);
	}
}

add_action('rest_api_init', 'ap_rest');
function ap_rest() {
	register_rest_route('test', '/apn', array('methods' => 'POST', 'callback' => 'api_test_apn'));
	register_rest_route('test', '/apn-raw', array('methods' => 'POST', 'callback' => 'api_test_apn_raw'));
}

add_action('ub_push', 'ap_notify', 10, 2);
/**
 * @param WP_Post	$post
 * @param array		$subscribers
 */
function ap_notify($post, $subscribers) {
	global $wpdb;

	$dir = plugin_dir_path(__FILE__);
//	$apn_url = 'ssl://gateway.sandbox.push.apple.com:2195';
//	$apn_cert = $dir . 'development.pem';
	$apn_url = 'ssl://gateway.push.apple.com:2195';
	$apn_cert = $dir . 'production.pem';

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', $apn_cert);
	stream_context_set_option($ctx, 'ssl', 'passphrase', '12345');

	$fp = stream_socket_client($apn_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	stream_set_blocking($fp, 0);

	if (!$fp) {
		trigger_error("Cannot connect to APNS: $err $errstr", E_USER_WARNING);
		return false;
	}

	$apple_expiry = time() + 86400;
	$body = array(
		'post_id' => $post->ID,
		'aps' => array(
			'alert' => html_entity_decode($post->post_title),
			'sound' => 'default',
			'link_url' => site_url(),
		)
	);

	$ids = implode(',', $subscribers);
	$sql = "SELECT tok.umeta_id, tok.meta_value, cnt.meta_value AS unread
	FROM {$wpdb->usermeta} AS tok
	LEFT JOIN {$wpdb->usermeta} AS cnt ON tok.user_id = cnt.user_id AND cnt.meta_key = 'unread'
	WHERE tok.meta_key = 'ios_token' AND tok.user_id IN($ids)";
	$tokens = $wpdb->get_results($sql);

	foreach ($tokens as $s) {
		$count = count(unserialize($s->unread));
		$body['aps']['badge'] = $count ? $count : 1;
		$payload = json_encode($body);

//		$msg = chr(0) . pack('n', 32) . pack('H*', $s->meta_value) . pack('n', strlen($payload)) . $payload;
		$msg = pack("C", 1) . pack("N", $s->umeta_id) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', $s->meta_value) . pack("n", strlen($payload)) . $payload;
		
		fwrite($fp, $msg, strlen($msg));
//		file_put_contents(ABSPATH . 'apn.log', $s->meta_value . ':' . $payload . PHP_EOL, FILE_APPEND);

		if (checkAppleErrorResponse($fp)) {
			break;
		}
	}

	usleep(500000);
    checkAppleErrorResponse($fp);

	fclose($fp);
}

function checkAppleErrorResponse($fp) {
   //byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID). Should return nothing if OK.
   $apple_error_response = fread($fp, 6);
   //NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait forever when there is no response to be sent.

   if ($apple_error_response) {
        //unpack the error response (first byte 'command" should always be 8)
        $err = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

        if ($err['status_code'] == '0') {
            $err['status_code'] = '0-No errors encountered';
        } else if ($err['status_code'] == '1') {
            $err['status_code'] = '1-Processing error';
        } else if ($err['status_code'] == '2') {
            $err['status_code'] = '2-Missing device token';
        } else if ($err['status_code'] == '3') {
            $err['status_code'] = '3-Missing topic';
        } else if ($err['status_code'] == '4') {
            $err['status_code'] = '4-Missing payload';
        } else if ($err['status_code'] == '5') {
            $err['status_code'] = '5-Invalid token size';
        } else if ($err['status_code'] == '6') {
            $err['status_code'] = '6-Invalid topic size';
        } else if ($err['status_code'] == '7') {
            $err['status_code'] = '7-Invalid payload size';
        } else if ($err['status_code'] == '8') {
            $err['status_code'] = '8-Invalid token';
        } else if ($err['status_code'] == '255') {
            $err['status_code'] = '255-None (unknown)';
        } else {
            $err['status_code'] = $err['status_code'] . '-Not listed';
        }

		trigger_error("APNS: ID[{$err['identifier']}] CODE[{$err['status_code']}]", E_USER_WARNING);

		global $wpdb;
		$wpdb->delete($wpdb->usermeta, array('umeta_id' => $err['identifier']));

        return $error_response;
   }
   return false;
}

function api_test_apn($request) {
	global $wpdb;

	if ( ! current_user_can('administrator')) {
		return new WP_Error( 'admin_only', __( 'You are not administrator.' ), array( 'status' => 403 ) );
	}

	$post_id = isset($request['post_id']) ? $request['post_id'] : 1756;
	$post = get_post($post_id);
	if (empty($post)) {
		return new WP_Error('post_not_found', __( 'Post not found' ), array( 'status' => 404 ) );
	}

	$sql = $wpdb->prepare("SELECT meta_value FROM {$wpdb->usermeta} WHERE user_id = %d AND meta_key = 'ios_token'", $request['user_id']);
	$token = $wpdb->get_var($sql);
	if (empty($token)) {
		return new WP_Error('token_not_found', __( 'Token not found' ), array( 'status' => 404 ) );
	}

	do_action('ub_push', $post, array(intval($request['user_id'])));

	return ['msg' => 'OK'];
}

// Parameters: user_id, payload
function api_test_apn_raw($request) {
	global $wpdb;

	if ( ! current_user_can('administrator')) {
		return new WP_Error( 'admin_only', __( 'You are not administrator.' ), array( 'status' => 403 ) );
	}

	$dir = plugin_dir_path(__FILE__);
	$apn_url = 'ssl://gateway.push.apple.com:2195';
	$apn_cert = $dir . 'production.pem';

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', $apn_cert);
	stream_context_set_option($ctx, 'ssl', 'passphrase', '12345');

	$fp = stream_socket_client($apn_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	stream_set_blocking($fp, 0);

	if (!$fp) {
		trigger_error("Cannot connect to APNS: $err $errstr", E_USER_WARNING);
		return new WP_Error('cannot_connect', "Cannot connect to APNS: $err $errstr", array( 'status' => 500 ) );
	}

	$sql = $wpdb->prepare("SELECT meta_value FROM {$wpdb->usermeta} WHERE user_id = %d AND meta_key = 'ios_token'", $request['user_id']);
	$token = $wpdb->get_var($sql);
	if (empty($token)) {
		return new WP_Error('token_not_found', __( 'Token not found' ), array( 'status' => 404 ) );
	}

	$apple_expiry = time() + 86400;
	$payload = $request['payload'];
	$msg = pack("C", 1) . pack("N", time()) . pack("N", $apple_expiry) . pack("n", 32) . pack('H*', $token) . pack("n", strlen($payload)) . $payload;
		
	fwrite($fp, $msg, strlen($msg));
	usleep(500000);
    checkAppleErrorResponse($fp);

	fclose($fp);
}