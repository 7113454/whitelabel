// Initialize Firebase
  var config = {
	apiKey: "AIzaSyAAGhOw5mrx8H2Ls7tiNtW8yWum39C5mrM",
	authDomain: "api-project-301207243533.firebaseapp.com",
	databaseURL: "https://api-project-301207243533.firebaseio.com",
	storageBucket: "api-project-301207243533.appspot.com",
	messagingSenderId: "301207243533"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.onMessage(function(pl) {
	var type = typeof pl.data.type == 'undefined' ? false : pl.data.type;

	if ("message" == type) {
		// user viewing the thread, just append message
		if (location.href.indexOf('thread=' + pl.data.thread) > 0) {
			var item = JSON.parse(pl.data.item);
			add_message(item);
		}
		// add notification
		else {
			jQuery('body').addClass('unread-msg');
		}
	}
});

messaging.onTokenRefresh(function() {
	messaging.getToken()
	.then(function(refreshedToken) {
		setTokenSentToServer(false);
		sendTokenToServer(refreshedToken);
	})
	.catch(function(err) {
		console.log('Unable to retrieve refreshed token ', err);
	});
});

function sendTokenToServer(token) {
	if (!isTokenSentToServer()) {
		var data = {"action": "fcm_token_save", "token": token};
		jQuery.post(ajaxurl, data);
		setTokenSentToServer(true);
	}
}

function isTokenSentToServer() {
	if (window.localStorage.getItem('sentToServer') == 1) {
		return true;
	}
	return false;
}

function setTokenSentToServer(sent) {
	if (sent) {
		window.localStorage.setItem('sentToServer', 1);
	} else {
		window.localStorage.setItem('sentToServer', 0);
	}
}

function getFcmToken() {
	messaging.getToken()
	.then(function(currentToken) {
		jQuery('#web-push').removeClass('hidden');
		if (currentToken) {
			console.log('fcm token: ' + currentToken);
			sendTokenToServer(currentToken);
		} else {
	        console.log('No Instance ID token available. Request permission to generate one.');
	    }
	})
	.catch(function(err) {
		console.log('An error occurred while retrieving token. ', err);
	});
}

function deleteToken() {
	messaging.getToken()
	.then(function(currentToken) {
		messaging.deleteToken(currentToken)
		.then(function() {
			var data = {"action": "fcm_token_delete", "token": currentToken};
			jQuery.post(ajaxurl, data);
			setTokenSentToServer(false);
		})
		.catch(function(err) {
			console.log('Unable to delete token. ', err);
		});
	})
	.catch(function(err) {
		console.log('Error retrieving Instance ID token. ', err);
	});
}

jQuery(function($) {
	getFcmToken();

	if (isTokenSentToServer()) {
		$('#web-push input[type=checkbox]').prop('checked', true);
	}

	$('#web-push input[type=checkbox]').on('change', function() {
		var subscribe = $(this).prop('checked');
		if (subscribe) {
			messaging.requestPermission()
			.then(getFcmToken);
		}
		else {
			deleteToken();
		}
	});
});