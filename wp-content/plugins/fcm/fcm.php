<?php
/*
Plugin Name: Firebase Cloud Messaging
Plugin URI: 
Description: PUSH notifications
Author: ub.fitness
Version: 1.0
Author URI: 
*/

define('FCM_SERVER_KEY', 'AAAARiFZyw0:APA91bHd9jXBi_TF_05z5QzqGjKAuNR7AJ75aUWMkqDJlofOlTPPQSZuHVo1RvJaq2oFoix-u2RnWtMoAcpC6LaP_eXpsuv7Lxv889bScxbDcng9Ju7Twk--iFr_-4uFbVTvKXxuEuMxUpZhcDOrCN30w8nIWLefKg');

register_activation_hook(__FILE__, 'fcm_activation');
function fcm_activation() {
	global $wpdb;

	$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}fcm_tokens (
		`id` bigint unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`user_id` bigint unsigned NOT NULL,
		`token` varchar(255) NOT NULL,
		`agent` varchar(255) DEFAULT NULL,
		`app` enum('web','ios','android') NOT NULL DEFAULT 'ios',
		`check` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
		UNIQUE KEY `token` (`token`),
		KEY `user_id` (`user_id`),
		FOREIGN KEY (`user_id`) REFERENCES `wp_users` (`ID`) ON DELETE CASCADE
	) DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci";
	$wpdb->query($sql);

	if ( ! wp_next_scheduled ('fcm_clean')) {
		wp_schedule_event(time(), 'daily', 'fcm_clean');
	}

}

register_deactivation_hook(__FILE__, 'fcm_deactivation');
function fcm_deactivation() {
	wp_clear_scheduled_hook('fcm_clean');
}


add_action('rest_api_init', 'fcm_rest_init');
function fcm_rest_init() {
	register_rest_route('ub', '/fcm', array('methods' => 'POST', 'callback' => 'fcm_rest_token'));
	register_rest_route('ub', '/fcm-test', array('methods' => 'POST', 'callback' => 'fcm_test'));
}

function fcm_rest_token($request) {
	global $wpdb;

	$user = wp_get_current_user();
	if (empty($user->ID)) {
		return new WP_Error( 'rest_not_logged_in', __( 'You are not currently logged in.' ), array( 'status' => 401 ) );
	}
	if (empty($request['token'])) {
		return new WP_Error('token_required', 'Empty "token" parameter.', array('status' => 400));
	}

	$sql = $wpdb->prepare("SELECT id FROM {$wpdb->prefix}fcm_tokens WHERE token = %s", $request['token']);
	$exists = $wpdb->get_var($sql);

	if ( ! $exists) {
		$app = stripos($_SERVER['HTTP_USER_AGENT'], 'Android') ? 'android' : 'ios';
		$data = ['user_id' => $user->ID, 'token' => $request['token'], 'agent' => $_SERVER['HTTP_USER_AGENT'], 'app' => $app];
		$wpdb->insert($wpdb->prefix . 'fcm_tokens', $data);
	}

	return ['OK'];
}

add_action('ub_push', 'fcm_notify', 10, 2);
/**
 * @param WP_Post	$post
 * @param array		$subscribers
 */
function fcm_notify($post, $subscribers) {
	global $wpdb;

	$ids = implode(',', $subscribers);
	$sql = "SELECT tok.token, tok.app, cnt.meta_value AS unread
	FROM {$wpdb->prefix}fcm_tokens AS tok
	LEFT JOIN {$wpdb->usermeta} AS cnt ON tok.user_id = cnt.user_id AND cnt.meta_key = 'unread'
	WHERE tok.user_id IN($ids)";
	$tokens = $wpdb->get_results($sql);

	$recipients_cnt = count($tokens);

	$author = get_user_by('id', $post->post_author);

	$template = [
		'data' => [
			'type' => 'post',
			'post_id' => $post->ID,
		],
		'notification' => [
			'title' => $author->display_name,
			'body' => html_entity_decode($post->post_title),
			'sound' => 'default',
//			'icon' => home_url('wp-content/themes/v19/images/logo-in.png'),
//			'click_action' => get_permalink($post),
		],
	];

	foreach ($tokens as $s) {
		$data = $template;
		$count = count(unserialize($s->unread));
		$data['notification']['badge'] = $count ? $count : 1;

		if ('web' == $s->app) {
			$data['notification']['icon'] = home_url('wp-content/themes/v19/images/logo-in.png');
			$data['notification']['click_action'] = get_permalink($post);
		}

		if ($recipients_cnt > 1) {
			$data['registration_ids'][] = $s->token;
		}
		else {
			$data['to'] = $s->token;
		}
	}

	fcm_send($data);
}

function fcm_send($data) {
	$url = 'https://fcm.googleapis.com/fcm/send';
	$args = array(
		'headers' => array(
			'Content-Type' => 'application/json',
			'Authorization' => 'key='.FCM_SERVER_KEY,
		),
		'body' => json_encode($data),
	);

	$response = wp_remote_post($url, $args);

	if (is_wp_error($response)) {
		trigger_error('FCM: ' . $response->get_error_message(), E_USER_WARNING);
	}
	elseif (isset($response['code']) && 200 != $response['code']) {
		trigger_error('FCM: ' . $response['body'], E_USER_WARNING);
	}

	return $response;
}

function fcm_test($request) {
	if (empty($request['token'])) {
		return new WP_Error('token_required', 'Empty "token" parameter.', array('status' => 400));
	}

	$post = empty($request['post_id']) ? NULL : get_post($request['post_id']);
	if ( ! $post) {
		$posts = get_posts('orderby=rand&numberposts=1');
		$post = array_shift($posts);
	}
	$author = get_user_by('id', $post->post_author);

	$data = [
		'data' => [
			'post_id' => $post->ID,
		],
		'notification' => [
			'title' => $author->display_name,
			'body' => html_entity_decode($post->post_title),
			'sound' => 'default',
//			'icon' => home_url('wp-content/themes/v19/images/logo-in.png'),
			// 'click_action' => get_permalink($post),
			'badge' => rand(1, 50),
		],
		'to' => $request['token']
	];
	return fcm_send($data);
}

// subscribe to web push
add_shortcode('push_subscribe', 'fcm_web_subscribe');
function fcm_web_subscribe() {
	$user = wp_get_current_user();

	if ( ! $user->ID) return;

	ob_start();
	echo '<div id="web-push" class="hidden checkbox checkbox-slider--b-flat"><label class="notif">';
	echo '<input type="checkbox"><span>Notify on this device</span></label>';
	echo '</div>';
	return ob_get_clean();
}

add_action('wp_enqueue_scripts', 'fcm_scripts');
function fcm_scripts() {
	$user = wp_get_current_user();
	if ( ! $user->ID) return;

	// Safari 7 has issues with firebase
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'Safari')) return;

	wp_enqueue_script('firebase', 'https://www.gstatic.com/firebasejs/4.1.3/firebase.js', array(), null, true);
	wp_enqueue_script('fcm', site_url('/wp-content/plugins/fcm/fcm.js'), array(), '020617', true);
}

// add or delete fcm token
add_action('wp_ajax_fcm_token_delete', 'wp_ajax_fcm_token');
add_action('wp_ajax_fcm_token_save', 'wp_ajax_fcm_token');
function wp_ajax_fcm_token() {
	global $wpdb;

	$user = wp_get_current_user();
	$data = ['user_id' => $user->ID, 'token' => $_POST['token'], 'agent' => $_SERVER['HTTP_USER_AGENT'], 'app' => 'web'];

	if ('fcm_token_save' == $_POST['action']) {
		$wpdb->insert($wpdb->prefix . 'fcm_tokens', $data);
	}
	else {
		$wpdb->delete($wpdb->prefix . 'fcm_tokens', ['token' => $_POST['token']]);
	}

	wp_die('OK');
}

add_action('fcm_clean', 'fcm_test_tokens');
function fcm_test_tokens() {
	global $wpdb;

	$valid = [];
	$invalid = []; // invalid token ids

	$tokens = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}fcm_tokens ORDER BY `check` ASC LIMIT 10");
	foreach ($tokens as $t) {
		$data = ['data' => ['test' => 'test'], 'to' => $t->token, 'dry_run' => true];
		$resp = fcm_send($data);
		$json = json_decode($resp['body']);

		if ($json->failure) {
			$invalid[] = $t->id;
		}
		else {
			$valid[] = $t->id;
		}
	}

	if ($valid) {
		$sql = sprintf("UPDATE {$wpdb->prefix}fcm_tokens SET `check` = '%s' WHERE id IN (%s)", current_time('mysql'), implode(',', $valid));
		$wpdb->query($sql);
	}

	if ($invalid) {
		$sql = sprintf("DELETE FROM {$wpdb->prefix}fcm_tokens WHERE id IN (%s)", implode(',', $invalid));
		$wpdb->query($sql);
	}
}

add_action('message_sent', 'fcm_new_message');
function fcm_new_message($data) {
	global $wpdb;

	$sql = "SELECT token FROM {$wpdb->prefix}fcm_tokens AS t
		LEFT JOIN {$wpdb->prefix}usermeta AS um ON t.user_id = um.user_id AND um.meta_key = 'notifications'
		WHERE t.user_id = %d AND t.app != 'web' AND (um.meta_value IS NULL OR um.meta_value & 32)";
	$sql = $wpdb->prepare($sql, $data['recipient_id']);
	$tokens = $wpdb->get_col($sql);
//	$tokens = $wpdb->get_col("SELECT token FROM {$wpdb->prefix}fcm_tokens WHERE user_id = {$data['recipient_id']} AND app != 'web'");

	if ( ! $tokens) return;

	if ( ! class_exists('REST_Messages_Controller')) {
		include WP_PLUGIN_DIR . '/ub-messages/rest-messages-controller.php';
	}
	$rest = new REST_Messages_Controller();
	$msg = msg_get_own($data['id']);
	$msg = $rest->prepare_item_for_response($msg, false);

	$sender = get_user_by('id', $data['sender_id']);
	$template = [
		'data' => [
			'type' => 'message',
			'thread' => $data['thread'],
			'item' => $msg,
		],
		'notification' => [
			'title' => $sender->display_name,
			'body' => $data['text'],
			'sound' => 'default',
			'icon' => get_avatar_url($sender->user_email, ['size' => '40']),
		],
	];

	if (count($tokens) > 1) {
		$template['registration_ids'] = $tokens;
	}
	else {
		$template['to'] = $tokens[0];
	}

	fcm_send($template);
}

add_action('message_sent', 'fcm_new_message_web');
function fcm_new_message_web($data) {
	global $wpdb;

	$sql = "SELECT token FROM {$wpdb->prefix}fcm_tokens AS t
		LEFT JOIN {$wpdb->prefix}usermeta AS um ON t.user_id = um.user_id AND um.meta_key = 'notifications'
		WHERE t.user_id = %d AND t.app = 'web' AND (um.meta_value IS NULL OR um.meta_value & 32)";
	$sql = $wpdb->prepare($sql, $data['recipient_id']);
	$tokens = $wpdb->get_col($sql);
	// $tokens = $wpdb->get_col("SELECT token FROM {$wpdb->prefix}fcm_tokens WHERE user_id = {$data['recipient_id']} AND app = 'web'");

	if ( ! $tokens) return;

	$sender = get_user_by('id', $data['sender_id']);
	$template = [
		'data' => [
			'type' => 'message',
			'thread' => $data['thread'],
			'item' => msg_prepare_for_response($data),
		],
		'notification' => [
			'title' => $sender->display_name,
			'body' => $data['text'],
			'sound' => 'default',
			'icon' => get_avatar_url($sender->user_email, ['size' => '40']),
			'click_action' => messages_page_url('?thread=' . $data['thread']),
		],
	];

	if (count($tokens) > 1) {
		$template['registration_ids'] = $tokens;
	}
	else {
		$template['to'] = $tokens[0];
	}

	fcm_send($template);
}
