<h2>Clear Nearest Transport cache</h2>

<?php if (isset($_POST['action']) && 'flush_transit_cache' == $_POST['action']): ?>
	<div class="notice notice-success is-dismissible">
		<p>Cache has been cleared!</p>
	</div>
<?php endif ?>

<form method="post" action="">
	<div style="vertical-align:middle; display:inline-block; ">Here you can clear the cache.</div>
	<input type="hidden" name="action" value="flush_transit_cache">
	<button type="submit" class="button">Clear cache</button>
</form>