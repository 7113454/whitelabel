<?php
/*
Plugin Name: Nearest Transport
Plugin URI: 
Description: Display transit stations near event
Author: ub.fitness
Version: 1.0
Author URI: 
*/

// register_activation_hook(__FILE__, 'nt_activation');
// function nt_activation() {
// 	global $wpdb;

// 	$charset_collate = $wpdb->get_charset_collate();

// 	$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}cache_transit` (
// 		`post_id` bigint(20) unsigned NOT NULL,
// 		`data` text NOT NULL,
// 		KEY `post_id` (`post_id`),
// 		CONSTRAINT `wp_cache_transit_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wp_posts` (`ID`) ON DELETE CASCADE
// 	) $charset_collate;";
// 	$wpdb->query($sql);
// }

// register_uninstall_hook(__FILE__, 'nt_uninstall');
// function nt_uninstall() {
// 	global $wpdb;

// 	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}cache_transit");
// }

/**
 * Fetch nearest transit stations
 *
 * @param int|WP_Post $post Optional. Post ID or WP_Post object. Default is global $post.
 */
function get_transport_near($post = 0) {
	global $wpdb;
	$post = get_post($post);

	if (empty($post->location)) return;

	if ( ! isset($post->_transit)) {
		$transit = array();
		$url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?type=transit_station&rankby=distance'
			. sprintf('&key=%s&location=%s,%s', GOOGLE_API_KEY, $post->location['lat'], $post->location['lng']);
		$resp = wp_remote_get($url);
		$json = json_decode($resp['body']);

		foreach ($json->results as $r) {
			if (isset($transit[$r->types[0]])) continue; // save only one type for now

			if ('bus_station' == $r->types[0]) continue; // do not save bus stations

			$transit[$r->types[0]] = array(
				'name' => $r->name,
				'dist' => round(calculateDistance($post->location['lat'], $post->location['lng'], $r->geometry->location->lat, $r->geometry->location->lng), 3),
				'type' => $r->types[0],
			);
		}

		update_post_meta($post->ID, '_transit', $transit);
	}

	return $post->_transit;
}

add_action('rest_api_init', 'rest_add_transit');
function rest_add_transit() {
	register_rest_field('post', 'transit', array('get_callback' => 'rest_transit'));
}

function rest_transit($obj) {
	return get_transport_near($obj['id']);
}

add_action('ub_settings_page', 'nt_settings_page');
function nt_settings_page() {
	global $wpdb;
	if ( ! current_user_can('manage_options')) return;

	if (isset($_POST['action']) && 'flush_transit_cache' == $_POST['action']) {
		$wpdb->delete($wpdb->postmeta, ['meta_key' => '_transit']);
	}

	include plugin_dir_path( __FILE__ ) . 'settings-page.php';
}

// function get_bus_numbers($place_id) {
// 	$url = 'https://maps.googleapis.com/maps/api/place/details/json?key='.GOOGLE_API_KEY.'&placeid='.$place_id;
// }