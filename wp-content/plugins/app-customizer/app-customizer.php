<?php
/*
Plugin Name: App Manager [White Label]
Description: Change the look of mobile app. PHP GD required. PHP GMP extenstion is recommended for the image perceptual hash.
Author: 
Version: 1.1
*/

register_activation_hook(__FILE__, 'app_customizer_activate');
function app_customizer_activate() {
	$purl = plugin_dir_url(__FILE__);
	$carousel = "{$purl}img/slider1.png\n{$purl}img/slider2.png\n{$purl}img/slider3.png\n{$purl}img/slider4.png";
	$defaults = array(
		'color_main' => '#eaeaea',
		'color_text' => '#444444',
		'color_alt' => '#f7584c',
		'logo' => $purl . 'img/logo.png',
		'font' => 'Montserrat-Regular',
		'instagram' => '',

		"suggested" => 1, "voice_search" => 1, "messages" => 1, "search_location" => 1, "search_location" => 1,
		"video" => 1, "calendar" => 1, "share" => 1, "map" => 1, "profile_settings" => 1, "likes" => 1, "attending" => 1,
		"nearest_transport" => 1, "date_time" => 1, "search" => 1, "location" => 1, "description" => 1, "profiles" => 1,
		"follow" => 1, "facebook" => 1, "inappropriate_report" => 1, "launch_game" => 1, 'comments' => 1,
		'ads' => 0, 'analytics' => 0, 'ar' => 0, 'wallet' => 0,
	);

	update_option('app_config', $defaults, 'no');
}

add_action('admin_init', 'er_app_settings_init');
function er_app_settings_init() {
	global $app_checkboxes;
	register_setting('app_customize', 'app_config', 'sanitize_app_settings');

	$app_checkboxes = array(
		'suggested' => 'Suggested',
		'voice_search' => 'Voice Search',
		'messages' => 'Private Messages',
		'search_location' => 'Search Location',
		'video' => 'Video',
		'calendar' => 'Caneldar',
		"share" => 'Share',
		"map" => 'Map',
		"profile_settings" => 'Profile Settings',
		"likes" => 'Likes',
		"attending" => 'Attending',
		"nearest_transport" => 'Nearest Transport',
		"date_time" => 'Date & Time',
		"search" => 'Search',
		"location" => 'Location',
		"description" => 'Description',
		"profiles" => 'Profiles',
		"follow" => 'Follow',
		"facebook" => 'Facebook',
		"inappropriate_report" => 'Report Inappropriate',
		"launch_game" => 'Launch Game',
		'comments' => 'Comments',
		'ads' => 'Ads',
		'analytics' => 'Analytics',
		'ar' => 'Augmented Reality',
		'wallet' => 'iOS Wallet',
	);

	// add_settings_section('app_main_settings', 'Main Settings', 'print_main_section', 'app_config');
	// add_settings_field('voice_search', 'Voice Search', 'print_voice_search', 'app_config', 'app_main_settings');
}

// create Options page
add_action('admin_menu', 'app_config_menu');
function app_config_menu() {
	add_options_page('App Config', 'App Config', 'manage_options', 'app_config', 'app_config_html');
}
function app_config_html() {
	global $app_checkboxes;
	wp_enqueue_style('wp-color-picker');
    wp_enqueue_script('wp-color-picker');
    // wp_enqueue_script('jquery-ui-tabs');
    // wp_enqueue_script('jquery-ui-accordion');

	$app_config = get_option('app_config');
	$fonts = array(
		'Arial', 'Helvetica', 'Times New Roman', 'Times', 'Courier New', 'Courier', 'Verdana', 'Georgia',
		'Palatino', 'Garamond', 'Bookman', 'Comic Sans MS', 'Trebuchet MS', 'Arial Black', 'Impact'
	);
	asort($fonts);
	include plugin_dir_path(__FILE__) . 'options-page.php';
}

// rest api
add_action('rest_api_init', 'api_app_init');
function api_app_init() {
	register_rest_route('wp/v2', '/app-settings', array('methods' => 'GET', 'callback' => 'api_get_config'));
//	register_rest_route('wp/v2', '/ar-find', array('methods' => 'POST', 'callback' => 'api_find_ar'));

//	register_rest_field(array('post', 'ar_object'), 'image', array('get_callback' => function($object) {
//		return get_the_post_thumbnail_url($object['id'], 'large');
//	}));
}

function api_get_config() {
	$cfg = get_option('app_config');
	$cfg['admin_email'] = get_option('admin_email');
	return apply_filters('api_get_config', $cfg);
}

function sanitize_app_settings($data) {
	global $app_checkboxes;
	$cfg = $data;

	$cfg['eula_enabled'] = empty($data['eula_enabled']) ? 0 : 1;
	foreach ($app_checkboxes as $k => $v) {
		$cfg[$k] = empty($data[$k]) ? 0 : 1;
	}
	return $cfg;
}

// add filter by meta fields
// add_filter('rest_post_query', 'filter_meta_field', 10, 2);
// function filter_meta_field($args, $request) {
// 	if (isset($request['filter']['meta_key'])) {
// 		$args['meta_key'] = $request['filter']['meta_key'];
// 	}
// 	if (isset($request['filter']['meta_value'])) {
// 		$args['meta_value'] = $request['filter']['meta_value'];
// 	}
// 	if (isset($request['filter']['meta_compare'])) {
// 		$args['meta_compare'] = $request['filter']['meta_compare'];
// 	}
// 	return $args;
// }

// add_action('init', 'register_ar_post');
// function register_ar_post() {
// 	register_post_type('ar_object', array(
// 		'label' => 'AR Objects',
// 		'public' => true,
// 		'show_in_rest' => true,
// 		'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields'),
// 	));
// }

// start perceptual image hash for AR Objects
/*
add_action('added_post_meta', 'ar_perceptual_hash', 10, 4);
add_action('updated_post_meta', 'ar_perceptual_hash', 10, 4);
function ar_perceptual_hash($meta_id, $object_id, $meta_key, $_meta_value) {
	if ('ar_object' != get_post_type() || '_thumbnail_id' != $meta_key) return;
	if ('' == $file = get_attached_file($_meta_value)) return;

	require_once(plugin_dir_path(__FILE__) . 'imagehash/Implementation.php');
	require_once(plugin_dir_path(__FILE__) . 'imagehash/Implementations/PerceptualHash.php');
	require_once(plugin_dir_path(__FILE__) . 'imagehash/ImageHash.php');

	$implementation = new Jenssegers\ImageHash\Implementations\PerceptualHash;
	$hasher = new Jenssegers\ImageHash\ImageHash($implementation);
	$hash = $hasher->hash($file);

	update_post_meta($object_id, '_perceptual_hash', $hash);
}

function api_find_ar($request) {
	global $wpdb;
	require_once(plugin_dir_path(__FILE__) . 'imagehash/Implementation.php');
	require_once(plugin_dir_path(__FILE__) . 'imagehash/Implementations/PerceptualHash.php');
	require_once(plugin_dir_path(__FILE__) . 'imagehash/ImageHash.php');

	if (empty($_FILES['file'])) {
		return new WP_Error( 'rest_upload_no_data', __( 'No data supplied.' ), array( 'status' => 400 ) );
	}

	$implementation = new Jenssegers\ImageHash\Implementations\PerceptualHash;
	$hasher = new Jenssegers\ImageHash\ImageHash($implementation);
	$hash = $hasher->hash($_FILES['file']['tmp_name']);

	$sql = "SELECT post_id, post_title, meta_value FROM {$wpdb->postmeta}
		JOIN {$wpdb->posts} ON post_id = ID WHERE meta_key = '_perceptual_hash'";
	$hashes = $wpdb->get_results($sql);

	$min = false;
//	$debug = array();
	foreach ($hashes as $h) {
		$h->distance = $hasher->distance($hash, $h->meta_value);
		if ( ! $min || $h->distance < $min->distance) $min = $h;

//		$debug[] = array('id' => $h->post_id, 'dist' => $h->distance);
	}

	// START DEBUG
	// $ddir = '/tmp/ar_debug/';
	// mkdir($ddir);
	// copy($_FILES['file']['tmp_name'], $ddir . basename($_FILES['file']['tmp_name']));

	// ob_start();
	// echo "\n-----------------------------\n";
	// echo "FILE: " . basename($_FILES['file']['tmp_name']) . PHP_EOL;
	// var_dump($debug);
	// file_put_contents($ddir . 'log.txt', ob_get_clean(), FILE_APPEND);
	// END DEBUG

	if ($min && $min->distance <= 20) {
		$request = new WP_REST_Request('GET', '/wp/v2/ar_object/' . $min->post_id);
		return rest_do_request( $request );
	}
	else {
		$error = new WP_Error('rest_ar_not_found', 'Not Found.', array('status' => 404));
		return $error;
	}
}
*/
// end

// modify WP Email Template settings
add_filter('wp_email_template_wp_email_template_general_get_settings', 'mod_wp_email_template_general');
function mod_wp_email_template_general($value) {
	$app_config = get_option('app_config');
	$value['background_colour']['color'] = $app_config['color_main'];
	return $value;
}
add_filter('wp_email_template_wp_email_template_style_header_image_get_settings', 'mod_wp_email_template_style_header_image');
function mod_wp_email_template_style_header_image($value) {
	$app_config = get_option('app_config');
	$value['header_image'] = $app_config['logo'];
	$value['header_image_url'] = site_url();
	return $value;
}
add_filter('wp_email_template_wp_email_template_social_media_get_settings', 'mod_wp_email_template_social_media');
function mod_wp_email_template_social_media($value) {
	$app_config = get_option('app_config');
	$value['email_facebook'] = empty($app_config['facebook_url']) ? '' : $app_config['facebook_url'];
	$value['email_twitter'] = empty($app_config['twitter_url']) ? '' : $app_config['twitter_url'];
	return $value;
}
