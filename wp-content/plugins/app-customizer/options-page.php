<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" type="text/css" media="all" />

<div class="wrap">
	<h1>App Config</h1>

	<form method="post" action="options.php" class="form-horizontal">
		<table class="form-table">
			<tr>
				<th scope="row">Main Color</th>
				<td><input type="text" name="app_config[color_main]" value="<?= $app_config['color_main'] ?>" class="color" /></td>
			</tr>
			<tr>
				<th scope="row">Text Color</th>
				<td><input type="text" name="app_config[color_text]" value="<?= $app_config['color_text'] ?>" class="color" /></td>
			</tr>
			<tr>
				<th scope="row">Alt Color</th>
				<td><input type="text" name="app_config[color_alt]" value="<?= $app_config['color_alt'] ?>" class="color" /></td>
			</tr>
			<tr>
				<th scope="row">Title Font</th>
				<td>
					<select name="app_config[title_font]">
						<?php
						foreach ($fonts as $f) {
							$selected = selected($app_config['title_font'], $f, false);
							printf("<option value=\"%s\" %s>%s</option>", $f, $selected, $f);
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">Body Font</th>
				<td>
					<select name="app_config[font]">
						<?php
						foreach ($fonts as $f) {
							$selected = selected($app_config['font'], $f, false);
							printf("<option value=\"%s\" %s>%s</option>", $f, $selected, $f);
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">Logo Image</th>
				<td><input type="text" name="app_config[logo]" value="<?= $app_config['logo'] ?>" class="regular-text" /></td>
			</tr>
			<tr>
				<th scope="row">Instagram URL</th>
				<td><input type="text" name="app_config[instagram]" value="<?= $app_config['instagram'] ?>" class="regular-text" /></td>
			</tr>
			<tr>
				<th scope="row">EULA</th>
				<td>
					<?php $tmp = empty($app_config['eula_enabled']) ? '' : 'checked' ?>
					<label><input type="checkbox" name="app_config[eula_enabled]" <?= $tmp ?>> Show</label>
					<textarea name="app_config[eula_text]" placeholder="Place EULA here" class="large-text" cols="50" rows="4"><?php print(htmlspecialchars($app_config['eula_text'])) ?></textarea>
				</td>
			</tr>

			<?php foreach ($app_checkboxes as $k => $v): $checked = isset($app_config[$k]) && $app_config[$k] ? 'checked' : ''; ?>
				<tr>
					<td colspan="2">
						<label>
							<input type="checkbox" name="app_config[<?= $k ?>]" value="1" <?= $checked ?> > <?= $v ?>
						</label>
					</td>
				</tr>
			<?php endforeach ?>
		</table>

		<?php 
		settings_fields('app_customize');
		do_settings_sections('app_config');
		submit_button();
		?>
	</form>
</div>

<script>
	jQuery(function() {
		// jQuery("#accordion").accordion();
		// jQuery("#tabs").tabs();
		jQuery('.color').wpColorPicker();
	})
</script>
<style>
	label { display: inline-block; width: 200px; font-weight: 600; vertical-align: top; }
</style>