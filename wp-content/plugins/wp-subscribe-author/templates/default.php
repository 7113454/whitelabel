<?php 
$post_id = $data['data']['post_id'];
$author_id = $data['data']['author_id'];
$subscriber_email = $data['data']['subscriber_email'];

$post = get_post( $post_id );
$post_title = $post->post_title;
$post_content = wp_trim_words( $post->post_content );
$post_url =  esc_url(get_permalink($post));

$author_name = get_userdata($author_id)->display_name;
$blogname = get_bloginfo('name');
$author_url = esc_url( get_author_posts_url($post_id));

$unsubscribe_link = site_url().'?wpsa_unsubscribe='.md5($subscriber_email).'&author='.$author_id;

echo $emailtemplate = <<<EOD
<html>
	<body>
		<h2>New Post From $author_name</h2>
		<h3><a href="$post_url">$post_title</a></h3>
		<p>$post_content</p>

		<br><br>
		<p><a href="$unsubscribe_link">unsubscribe from this list</a></p>
	</body>
</html>

EOD;

