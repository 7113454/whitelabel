<?php if (in_array('administrator',  wp_get_current_user()->roles)): ?>
	<div class="alert alert-info">
		<p>Here you can import events in CSV and ICS format, 100 events at max.</p>
		<p>
			<strong><a href="<?= admin_url('admin-ajax.php?action=example_csv') ?>">Download</a></strong> example CSV file.
		</p>
	</div>

	<form id="importForm" method="post" action="<?= admin_url( 'admin-ajax.php' ) ?>" enctype="multipart/form-data" class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-2 control-label">Events in queue</label>
			<div class="col-sm-10">
				<span id="queueCnt" class="badge"><?= $queueCnt ?></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Events Source</label>
			<div class="col-sm-10">
				<button id="selectFileBtn" type="button" class="btn btn-default btn-sm">Browse...</button>
				<input id="events" type="file" name="events" class="hide">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Event Category</label>
			<div class="col-sm-10">
				<?php wp_dropdown_categories(array('hierarchical' => 1, 'orderby' => 'name', 'class' => 'form-control')) ?>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Event Tags</label>
			<div class="col-sm-10">
				<input type="text" name="post_tags" class="form-control">
				<span class="help-block">Comma separated</span>
			</div>
		</div>
		<!--
		<div id="importProgress" class="progress">
			<div class="progress-bar" style="width: 0%"></div>
		</div>
		-->
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<input type="hidden" name="action" value="csv_import">
				<button id="importBtn" type="button" class="btn btn-primary">Import</button>
				<span id="importProgress" class="label label-default" style="display:none"></span>
			</div>
		</div>
	</form>
<?php else: ?>
	<div class="alert alert-warning">You are not allowed to use importer!</div>
<?php endif ?>

<script src="<?= site_url('/wp-includes/js/jquery/jquery.form.min.js') ?>"></script>
<script>
	var totalEvents = <?= $queueCnt ?>; // for progress bar
	var inProgress = false;

	jQuery(function($) {
		$('#selectFileBtn').on('click', function() {
			$('#events').trigger('click');
		});

		$('#importBtn').on('click', function() {
			inProgress = ! inProgress;
			var text = inProgress ? 'Pause' : 'Import';
			$(this).text(text);

			inProgress && $('#importForm').trigger('submit');
		});

		var opt = {
			success: onSuccess,
			beforeSubmit: function(data, form) { return inProgress; }
		};
		$('#importForm').ajaxForm(opt);

		function onSuccess(response, status, xhr, $form) {
			if (0 == response.queueCnt) {
				inProgress = false; // finish import
				$('#importBtn').text('Import');
			}

			if ( ! totalEvents) totalEvents = response.queueCnt;
			$('#queueCnt').text(response.queueCnt);
			$form.find('input[type=file]').val('');
			$form.trigger('submit');

			if (totalEvents) {
				var progress = Math.ceil((totalEvents - response.queueCnt) * 100 / totalEvents);
				// $('#importProgress').show().find('.progress-bar').css('width', progress+'%');
				$('#importProgress').html(progress+'% Complete').show();
			}
		}
	});
</script>