<div class="wrap">
	<h2>CSV/ICS Importer</h2>

	<?php if ( ! $queueCnt): ?>
		<div class="notice notice-info is-dismissible">
			<p><a href="<?= admin_url('admin-ajax.php?action=example_csv') ?>">Download</a> example CSV file.</p>
		</div>
	<?php endif ?>

	<form method="post" id="importForm" action="<?= admin_url( 'admin-ajax.php' ) ?>" enctype="multipart/form-data">
		<table class="form-table">
			<tr>
				<th>Events in queue</th>
				<td><span id="queueCnt"><?= $queueCnt ?></span></td>
			</tr>
			<tr>
				<th>Post Owner</th>
				<td><?php wp_dropdown_users(['selected' => get_current_user_id()]) ?></td>
			</tr>
			<tr>
				<th>Post Status</th>
				<td>
					<select name="post_status">
					<?php
						foreach (get_post_statuses() as $k => $v) {
							printf('<option value="%s" %s>%s</option>', $k, ('publish' == $k ? 'selected' : ''), $v);
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<th>Post Category</th>
				<td><?php wp_dropdown_categories(array('hierarchical' => 1, 'orderby' => 'name')) ?></td>
			</tr>
			<tr>
				<th>Post Tags</th>
				<td>
					<input type="text" class="regular-text" name="post_tags">
					<p class="description">Comma separated</p>
				</td>
			</tr>
			<tr>
				<th>CSV/ICS File</th>
				<td>
					<input type="file" name="events">
					<p class="description"><?php printf( __( 'Maximum upload file size: %s.' ), esc_html(size_format(wp_max_upload_size()))) ?></p>
				</td>
			</tr>
		</table>
		<p class="submit">
			<input type="hidden" name="action" value="csv_import">
			<button id="importBtn" type="button" class="button button-primary">Import</button>
		</p>
	</form>
</div>

<script>
	var totalEvents = <?= $queueCnt ?>; // for progress bar
	var inProgress = false;

	jQuery(function($) {

		$('#importBtn').on('click', function() {
			inProgress = ! inProgress;
			var text = inProgress ? 'Pause' : 'Import';
			$(this).text(text);

			inProgress && $('#importForm').trigger('submit');
		});

		var opt = {
			success: onSuccess,
			beforeSubmit: function(data, form) { return inProgress; }
		};
		$('#importForm').ajaxForm(opt);

		function onSuccess(response, status, xhr, $form) {
			if (0 == response.queueCnt) {
				inProgress = false; // finish import
				$('#importBtn').text('Import');
			}

			if ( ! totalEvents) totalEvents = response.queueCnt;
			$('#queueCnt').text(response.queueCnt);
			$form.find('input[type=file]').val('');
			$form.trigger('submit');
		}
	})
</script>