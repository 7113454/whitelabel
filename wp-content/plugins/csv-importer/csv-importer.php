<?php
/*
Plugin Name: UBF Importer
Plugin URI: 
Description: Import events from CSV or ICS file
Author: ub.fitness
Version: 1.1
Author URI: 
*/

register_activation_hook(__FILE__, 'csv_import_activation');
function csv_import_activation() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}csv_queue (
		`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		`user_id` bigint(20) unsigned NOT NULL,
		`post_title` varchar(255) NOT NULL,
		`post_content` text NULL,
		`date_start` varchar(50) NOT NULL,
		`date_end` varchar(50) NOT NULL,
		`location` varchar(255) DEFAULT NULL,
		`image` varchar(255) DEFAULT NULL,
		PRIMARY KEY (`id`)
	) $charset_collate;";
	$wpdb->query($sql);
}

register_uninstall_hook(__FILE__, 'csv_import_uninstall');
function csv_import_uninstall() {
	global $wpdb;

	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}csv_queue");
}

add_action('admin_menu', 'csv_import_menu');
function csv_import_menu() {
	add_management_page('UBF Importer', 'UBF Importer', 'manage_options', 'csv-import-page.php', 'csv_import_page');
}

add_action('admin_enqueue_scripts', 'csv_enqueue_scripts');
function csv_enqueue_scripts($hook) {
	if ('tools_page_csv-import-page' != $hook) return;
	wp_enqueue_script('jquery-form');
}

function csv_import_page() {
	global $wpdb;

	$queueCnt = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}csv_queue WHERE user_id = " . get_current_user_id());
	require plugin_dir_path(__FILE__) . 'csv-import-page.php';
}

add_action('wp_ajax_csv_import', 'csv_import');
function csv_import() {
	global $wpdb;
	$limit = 15;
	$user_id = get_current_user_id();
	remove_filter('wp_insert_post', 'ub_custom_fields');

	if ( ! current_user_can('manage_options')) wp_send_json(['error' => 'forbidden']);

	if ($_FILES['events']) file2db($_FILES['events']['tmp_name']);

	// get several events from queue and import them
	$events = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}csv_queue WHERE user_id = $user_id LIMIT $limit", OBJECT_K);
	foreach ($events as $e) {
		csv_create_post($e);
	}

	// delete events from queue
	$sql = sprintf("DELETE FROM {$wpdb->prefix}csv_queue WHERE id IN (%s)", implode(',', array_keys($events)));
	$wpdb->query($sql);

	$queueCnt = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}csv_queue WHERE user_id = " . get_current_user_id());
	wp_send_json(['queueCnt' => $queueCnt]);
}

function csv_create_post($event) {
	$post_author = current_user_can('administrator') && isset($_POST['user']) ? intval($_POST['user']) : get_current_user_id();
	$post_status = current_user_can('administrator') && isset($_POST['post_status']) ? $_POST['post_status'] : 'publish';
	$post = array(
		'post_title' => $event->post_title,
		'post_content' => $event->post_content,
		'post_status' => $post_status,
		'post_author' => $post_author,
		'post_category' => array($_POST['cat']),
	);


	$post_id = wp_insert_post($post);

	if ($post_id) {
		update_post_meta($post_id, 'date', strtotime($event->date_start));
		update_post_meta($post_id, 'date_end', strtotime($event->date_end));
		
		if ( ! empty($_POST['post_tags'])) {
			wp_set_post_tags($post_id, $_POST['post_tags']);
		}

		$l = $event->location ? ub_geocode($event->location) : false;
		if ($l) {
			update_post_meta($post_id, 'location', array('address' => $l['formatted_address'], 'lat' => $l['lat'], 'lng' => $l['lng']));
		}

		// set featured image
		if ($event->image) {
			// set featured image
			$img = media_sideload_image($event->image, $post_id);
			if ( ! is_wp_error($img)) {
				$media = get_attached_media('image', $post_id);
				if ( ! empty($media)) set_post_thumbnail($post_id, key($media));
			}
		}
	}

	return $post_id;
}

function file2db($file) {
	global $wpdb;
	$user_id = get_current_user_id();
	$mime = mime_content_type($file);
	$cnt = 0;
	$max_events = current_user_can('administrator') ? 10000 : 100;
	$buf = array();
	$sql = "INSERT INTO {$wpdb->prefix}csv_queue (user_id, post_title, post_content, date_start, date_end, location, image) VALUES ";

	// if ('application/zip' == $mime) {

	// }

	if ('text/calendar' == $mime) {
		require plugin_dir_path(__FILE__) . 'lib/ICal/EventObject.php';
		require plugin_dir_path(__FILE__) . 'lib/ICal/ICal.php';

		$ical = new ICal\ICal($file);

		foreach ($ical->events() as $ev) {
			$buf[] = $wpdb->prepare("(%d, %s, %s, %s, %s, %s, %s)", $user_id, $ev->summary, '', $ev->dtstart, $ev->dtend, $ev->location, '');
			if (0 == ($cnt % 40)) {
				$wpdb->query($sql . implode(',', $buf));
				$buf = array();
				$cnt++;
			}
			if ($cnt > $max_events) break;
		}
		if (count($buf)) $wpdb->query($sql . implode(',', $buf));
	}
	elseif ('text/plain' == $mime) {
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle)) !== FALSE) {
				if (empty($buf) && 'title' == $data[0]) continue; // skip first line
				$buf[] = $wpdb->prepare("(%d, %s, %s, %s, %s, %s, %s)", $user_id, $data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);

				if (0 == ($cnt % 40)) {
					$wpdb->query($sql . implode(',', $buf));
					$buf = array();
					$cnt++;
				}
				if ($cnt > $max_events) break;
			}
			if (count($buf)) $wpdb->query($sql . implode(',', $buf));
		}
	}

}


// shortcode for the frontend
add_shortcode('ub-importer', 'ub_importer');
function ub_importer() {
	global $wpdb;
	$queueCnt = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}csv_queue WHERE user_id = " . get_current_user_id());

	ob_start();
	require plugin_dir_path(__FILE__) . 'csv-import-shortcode.php';
	return ob_get_clean();
}

// provide example.csv for user
add_action('wp_ajax_example_csv', 'download_example_csv');
function download_example_csv() {
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment;filename=example.csv');

	$fp = fopen('php://output', 'w');
	fputcsv($fp, ['title', 'content', 'start', 'end', 'location', 'image']);
	fclose($fp);

	wp_die();
}
