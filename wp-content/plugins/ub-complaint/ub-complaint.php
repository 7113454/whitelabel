<?php
/*
Plugin Name: UB Complaint
Plugin URI: 
Description: offensive content reports
Author: ub.fitness
Version: 1.0
Author URI: 
*/

/*
CREATE TABLE `wp_complaints` (
  `id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `item_type` varchar(100) NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `ip` varchar(15) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `agent` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
*/

function ub_create_complaint($attr) {
	global $wpdb;
	$wpdb->insert($wpdb->prefix . 'complaints', $attr);
	ub_report_notify($attr);
}

function ub_report_notify($attr) {
	$subject = get_option('blogname') . ' Report';
	$message = "Offensive report:
URL: url
Subject: subject
Message: message
IP: ip
Agent: agent
";

	$attr['url'] = 'user' == $attr['item_type'] ? get_author_posts_url($attr['item_id']) : get_permalink($attr['item_id']);

	$message = str_replace(array_keys($attr), $attr, $message);
	return wp_mail(get_option('admin_email'), $subject, $message);
}

// replace native function from wp-includes/pluggable.php
function wp_new_user_notification( $user_id, $deprecated = null, $notify = '' ) {
    if ( $deprecated !== null ) {
        _deprecated_argument( __FUNCTION__, '4.3.1' );
    }

    global $wpdb, $wp_hasher;
    $user = get_userdata( $user_id );

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $message  = sprintf(__('New user registration on your site %s:'), $blogname) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
    $message .= sprintf(__('Email: %s'), $user->user_email) . "\r\n";

    @wp_mail(get_option('admin_email'), sprintf(__('[%s] New User Registration'), $blogname), $message);

    // `$deprecated was pre-4.3 `$plaintext_pass`. An empty `$plaintext_pass` didn't sent a user notifcation.
    if ( 'admin' === $notify || ( empty( $deprecated ) && empty( $notify ) ) ) {
        return;
    }

    // Generate something random for a password reset key.
    $key = wp_generate_password( 20, false );

    /** This action is documented in wp-login.php */
    do_action( 'retrieve_password_key', $user->user_login, $key );

    // Now insert the key, hashed, into the DB.
    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . WPINC . '/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
    $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

    $message = sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
    $message .= __('To set your password, visit the following address:') . "\r\n\r\n";
    $message .= network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login') . "\r\n\r\n";

    wp_mail($user->user_email, sprintf(__('[%s] Your username and password info'), $blogname), $message);
}

