<?php
/*
Plugin Name: Import Diesel Gym
Plugin URI: 
Description: Import events for http://www.dieselgym.co.uk/
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'dg_activation');
function dg_activation() {
	if ( ! wp_next_scheduled ('dg_cronjob')) {
		wp_schedule_event(time(), 'weekly', 'dg_cronjob');
	}
}

register_deactivation_hook(__FILE__, 'dg_deactivation');
function dg_deactivation() {
	wp_clear_scheduled_hook('dg_cronjob');
}

add_action('dg_cronjob', 'dg_run');
function dg_run() {
	global $wpdb;

	remove_filter('wp_insert_post', 'ub_custom_fields');

	$dow = [1 => 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	$week = date('Y-W');
	if ($week <= get_option('_dg_last_update')) return;

	$source = 'http://www.dieselgym.co.uk/timetables-prices/';
	$response = wp_remote_get($source);
	if ( ! is_array($response) || 200 != $response['response']['code']) {
		trigger_error('Import: cannot fetch data.', E_USER_WARNING);
		return false;
	}

	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($response['body']);
	// $doc->loadHTMLFile('/home/misc/www/dieselgym.html');

	$xpath = new DOMXpath($doc);
	$rows = $xpath->query('//*[@id="all-events"]/table/tbody/tr');


	foreach ($rows as $tr) {
		foreach($tr->childNodes as $i => $td) {
			if (0 == $i || '' == $td->nodeValue) continue; // skip the first and empty columns

			$start = strtotime($dow[$i] . ' ' . $xpath->query('.//*[@class="top_hour"]', $td)->item(0)->nodeValue);
			$end = strtotime($dow[$i] . ' ' . $xpath->query('.//*[@class="bottom_hour"]', $td)->item(0)->nodeValue);

			$data = [
				'title' => $xpath->query('.//*[@class="event_header"]', $td)->item(0)->nodeValue,
				'content' => $xpath->query('.//*[@class="before_hour_text"]', $td)->item(0)->nodeValue,
				'start' => $start,
				'end' => $end,
			];
			dg_import($data);
		}
	}
	update_option('_dg_last_update', $week, 'no');
}

function dg_import($event) {
	$location = array(
		'address' => 'Simon House, Butchers Row, London E1W 3HB, UK',
		'lat' => '51.511207',
		'lng' => '-0.042653'
	);

	$post = array(
		'post_author' => 818,
		'post_category' => [7],
		'tags_input' => ['Martial arts', 'MMA', 'Jiu-Jitsu', 'Wrestling', 'Personal training'],

		'post_title' => $event['title'],
		'post_content' => $event['content'],
		'post_status' => 'publish',
		'meta_input' => array(
			'date' => $event['start'],
			'date_end' => $event['end'],
			'location' => $location,
			'info_url' => 'http://www.dieselgym.co.uk/',
			'_thumbnail_id' => 16329
		),
	);

	$id = wp_insert_post($post);

	return $id;
}