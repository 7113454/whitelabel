<?php
/*
Plugin Name: UB Like
Plugin URI: 
Description: Favorite posts
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'lk_activation');
function lk_activation() {
	global $wpdb;

	$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}like` (
		`post_id` bigint(20) unsigned NOT NULL,
		`user_id` bigint(20) unsigned NOT NULL,
		`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		UNIQUE KEY `post_user` (`post_id`,`user_id`),
		CONSTRAINT `wp_like_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `wp_posts` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci";
	$wpdb->query($sql);

	// import data from wp-favorite-posts
	if (0 == $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}like")) {
		import_wpfp();
	}
}

register_uninstall_hook(__FILE__, 'lk_uninstall');
function lk_uninstall() {
	global $wpdb;

	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}like");
}

function ub_toggle_like($post) {
	global $wpdb;

	$post = get_post($post);
	$user = wp_get_current_user();

	if ( ! $post || ! $user->ID) {
		trigger_error('Cannot like', E_USER_ERROR);
	}

	$data = ['post_id' => $post->ID, 'user_id' => $user->ID];

	if ($wpdb->delete($wpdb->prefix . 'like', $data)) {
		do_action('ub_unlike', $data);
		$like = false;
	} else {
		$data['date'] = current_time('mysql');
		$wpdb->insert($wpdb->prefix . 'like', $data);
		do_action('ub_like', $data);
		$like = true;
	}
	return $like;
}

add_action('ub_like', 'post_like_counter');
add_action('ub_unlike', 'post_like_counter');
function post_like_counter($data) {
	global $wpdb;

	$cnt = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}like WHERE post_id = " . $data['post_id']);
	update_post_meta($data['post_id'], '_like', $cnt);
}

function get_user_likes($user_id = null) {
	global $wpdb;

	$user_id = $user_id ? $user_id : get_current_user_id();

	$sql = $wpdb->prepare("SELECT post_id FROM {$wpdb->prefix}like WHERE user_id = %d", $user_id);
	return $user_id ? $wpdb->get_col($sql) : [];
}

function do_i_like($post_id) {
	global $wpdb;

	$user_id = get_current_user_id();
	$like = false;

	if ($post_id && $user_id) {
		$sql = $wpdb->prepare("SELECT post_id FROM {$wpdb->prefix}like WHERE post_id = %d AND user_id = %d", $post_id, $user_id);
		$like = !! $wpdb->get_var($sql);
	}

	return $like;
}

add_action('wp_ajax_ub_like', 'ub_ajax_like');
function ub_ajax_like() {
	$post_id = $_POST['post_id'];
	$like = ub_toggle_like($post_id);
	$out = array('btn' => ub_favorite($post_id, $like));
	wp_send_json($out);
}

function import_wpfp() {
	global $wpdb;

	$sql = "SELECT * FROM {$wpdb->usermeta} WHERE meta_key = 'wpfp_favorites'";
	$fav = $wpdb->get_results($sql);
	foreach ($fav as $f) {
		$ids = unserialize($f->meta_value);
		if (empty($ids)) continue;

		$sql = "INSERT IGNORE {$wpdb->prefix}like (post_id, user_id) VALUES ";
		foreach ($ids as $i) {
			$sql .= $wpdb->prepare("(%d, %d),", $i, $f->user_id);
		}
		$wpdb->query(substr($sql, 0, -1));
	}

	// update like counters
	$wpdb->query("DELETE FROM {$wpdb->postmeta} WHERE meta_key = '_like'");

	$sql = "SELECT post_id, COUNT(*) AS cnt FROM {$wpdb->prefix}like GROUP BY post_id";
	$count = $wpdb->get_results($sql);

	if ($count) {
		$sql = "INSERT INTO {$wpdb->postmeta} (post_id, meta_key, meta_value) VALUES ";
		foreach ($count as $c) {
			$sql .= $wpdb->prepare("(%d, '_like', %d),", $c->post_id, $c->cnt);
		}
		$wpdb->query(substr($sql, 0, -1));
	}
}