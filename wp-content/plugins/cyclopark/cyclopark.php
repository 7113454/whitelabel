<?php
/*
Plugin Name: Import Cyclopark
Plugin URI: 
Description: 
Author: ub.fitness
Version: 1.0
Author URI: 
*/

define('CY_CALENDAR', 'cycloparkmarketing@gmail.com'); // calendar ID

register_activation_hook(__FILE__, 'cy_activation');
function cy_activation() {
	cy_initial_import();

	if ( ! wp_next_scheduled('cyclopark_cron')) {
		wp_schedule_event(time(), 'daily', 'cyclopark_cron');
	}

	if ( ! wp_next_scheduled('cyclopark_update_time')) {
		wp_schedule_event(time(), 'daily', 'cyclopark_update_time');
	}
}

register_deactivation_hook(__FILE__, 'cy_deactivation');
function cy_deactivation() {
	wp_clear_scheduled_hook('cyclopark_cron');
	wp_clear_scheduled_hook('cyclopark_update_time');
}

function cy_initial_import() {
	global $wpdb;

	$calendar_id = 'cycloparkmarketing@gmail.com';
	$opt = array(
		'key' => GOOGLE_API_KEY,
		'timeMin' => date('c'),
		// 'maxResults' => 2,
	);

	remove_filter('wp_insert_post', 'ub_custom_fields');
	$exists = $wpdb->get_col("SELECT meta_value FROM {$wpdb->postmeta} WHERE meta_key = '_google_id'");

	do {
		$url = 'https://www.googleapis.com/calendar/v3/calendars/' . CY_CALENDAR . '/events?';
		$response = wp_remote_get($url . http_build_query($opt));

		if ( ! is_array($response)) return; // something is not right, exit

		$cal = json_decode($response['body']);
		foreach ($cal->items as $e) {
			if (in_array($e->id, $exists)) continue;

			py_create_post($e);
			$exists[] = $e->id;
		}

		if (isset($cal->nextSyncToken)) {
			update_option('cy_sync_token', $cal->nextSyncToken, 'no');
		}

		$opt['pageToken'] = empty($cal->nextPageToken) ? '' : $cal->nextPageToken;

		// break; // DEBUG

	} while ($opt['pageToken']);
}

function py_create_post($event, $post_id = 0) {
	$post = array(
		'ID' => $post_id,
		'post_title' => $event->summary,
		'post_content' => empty($event->description) ? $event->summary : $event->description,
		'post_status' => 'publish',
		'post_author' => 707,
		'post_category' => [25],
		'tags_input' => ['Cyclopark', 'bicycle', 'cycling', 'skate'],
	);


	$ID = wp_insert_post($post);

	if ($ID) {
		update_post_meta($ID, 'date', strtotime($event->start->dateTime));
		update_post_meta($ID, 'date_end', strtotime($event->end->dateTime));

//		update_post_meta($ID, '_recurrence', $event->recurrence[0]);
		update_post_meta($ID, '_google_id', $event->id);

		update_post_meta($ID, 'info_url', 'http://www.cyclopark.com/');

		update_post_meta($ID, 'location', array(
			'address' => 'The Tollgate, Wrotham Rd, Istead Rise, Kent DA11 7NP',
			'lat' => '51.419311',
			'lng' => '0.354112')
		);
	}

	return $ID;
}

add_action('cyclopark_cron', 'cy_sync');
function cy_sync() {
	global $wpdb;
	$token = get_option('cy_sync_token');
	remove_filter('wp_insert_post', 'ub_custom_fields');

	if (empty($token)) return;

	$opt = ['key' => GOOGLE_API_KEY, 'syncToken' => $token];
	$url = 'https://www.googleapis.com/calendar/v3/calendars/' . CY_CALENDAR . '/events?';
	$response = wp_remote_get($url . http_build_query($opt));

	if ( ! is_array($response)) return; // something is not right, exit

	$cal = json_decode($response['body']);

	if (empty($cal->items)) return; // no changes

	foreach ($cal->items as $e) {
		$sql = $wpdb->prepare("SELECT post_id FROM {$wpdb->postmeta} WHERE meta_key = '_google_id' AND meta_value = %s", $e->id);
		$post_id = $wpdb->get_var($sql);

		if ('cancelled' == $e->status && $post_id) {
			wp_delete_post($post_id);
		}
		else {
			py_create_post($e, $post_id);
		}
	}

	if (isset($cal->nextSyncToken)) {
		update_option('cy_sync_token', $cal->nextSyncToken, 'no');
	}

//	wp_mail('serge@enrappture.com', 'Cyclopark sync', 'Please check the sync, syncToken: ' . $token);
}

// update event start/end date
add_action('cyclopark_update_time', 'cy_update_date');
function cy_update_date() {
	global $wpdb;

	$sql = "SELECT goog.post_id, goog.meta_value
		FROM $wpdb->postmeta AS date
		INNER JOIN $wpdb->postmeta AS goog ON date.post_id = goog.post_id AND goog.meta_key = '_google_id'
		WHERE date.meta_key = 'date_end' AND date.meta_value BETWEEN UNIX_TIMESTAMP()-259200 AND UNIX_TIMESTAMP()";
//		WHERE date.meta_key = 'date_end' AND date.meta_value < UNIX_TIMESTAMP()";
	$result = $wpdb->get_results($sql);

	$opt = ['key' => GOOGLE_API_KEY, 'maxResults' => 1, 'timeMin' => date('c')];

	foreach ($result as $r) {
		$url = 'https://www.googleapis.com/calendar/v3/calendars/'.CY_CALENDAR.'/events/'.$r->meta_value.'/instances?';
		$response = wp_remote_get($url . http_build_query($opt));
		$json = empty($response['body']) ? array() : json_decode($response['body']);
		if ( ! $json || ! $json->items) continue;

		update_post_meta($r->post_id, 'date', strtotime($json->items[0]->start->dateTime));
		update_post_meta($r->post_id, 'date_end', strtotime($json->items[0]->end->dateTime));
	}

//	wp_mail('serge@enrappture.com', 'Cyclopark update', 'Updated cnt: ' . count($result));
}
