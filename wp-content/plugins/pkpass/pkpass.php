<?php
/*
Plugin Name: iOS Wallet
Plugin URI: 
Description: provides the functionality to create a pass for Wallet in Apple's iOS 6 and newe
Author: 
Version: 1.0
Author URI: 
*/
add_action('rest_api_init', 'api_register_pkpass');
function api_register_pkpass() {
	register_rest_route('wp/v2', '/pkpass/(?P<id>[\d]+)', array('methods' => 'GET', 'callback' => 'api_pkpass'));
}

function api_pkpass($obj) {
	require_once plugin_dir_path(__FILE__) . 'php-pkpass/PKPass.php';
	$post = get_post($obj['id']);
	$app_cfg = get_option('app_config');

	if (empty($post)) {
		$error = new WP_Error('rest_post_invalid_id', __( 'Invalid post ID.' ), array( 'status' => 404 ));
		return $error;
	}

	$pass = new PKPass\PKPass(get_option('pkpass_cert'), get_option('pkpass_cert_pass'));
	if($pass->checkError($error) == true) {
		return array('error' => $error);
	}

	$data = array(
		'passTypeIdentifier' => get_option('passTypeIdentifier'),
		'teamIdentifier' => get_option('teamIdentifier'),
		'formatVersion' => 1,
		'organizationName' => get_bloginfo('name'),
		'description' => $post->post_title,
		'serialNumber' => strval($post->ID),
		'relevantDate' => next_start_date($post, 'Y-m-d\TH:i:sP'),
		'logoText' => get_bloginfo('name'),
		'backgroundColor' => sprintf('rgb(%s)', hex2RGB($app_cfg['color_main'], true)),
		'labelColor' => sprintf('rgb(%s)', hex2RGB($app_cfg['color_text'], true)),
		'foregroundColor' => sprintf('rgb(%s)', hex2RGB($app_cfg['color_alt'], true)),
		'eventTicket' => array(
			'primaryFields' => array(
				array('key' => 'event-name', 'label' => 'Event', 'value' => $post->post_title),
			),
			'secondaryFields' => array(
				array(
					'dateStyle' => 'PKDateStyleMedium',
					'isRelative' => true,
					'key' => 'start-date',
					'label' => 'Start Date',
					'timeStyle' => 'PKDateStyleShort',
					'value' => next_start_date($post, 'Y-m-d\TH:i:sP')
				),
				array(
					'dateStyle' => 'PKDateStyleMedium',
					'isRelative' => true,
					'key' => 'end-date',
					'label' => 'End Date',
					'timeStyle' => 'PKDateStyleShort',
					'value' => next_end_date($post, 'Y-m-d\TH:i:sP')
				)
			),
		),
		'barcodes' => array(
			array('message' => get_permalink($post), 'format' => 'PKBarcodeFormatPDF417', 'messageEncoding' => 'iso-8859-1')
		),
	);
	if ( ! empty($post->location)) {
		$data['locations'] = array(array(
			'latitude' => floatval($post->location['lat']),
			'longitude' => floatval($post->location['lng']),
//			'relevantText' => 'Store nearby on 1st and Main.',
		));
	}
	$pass->setData($data);

	$pass->addFile(plugin_dir_path(__FILE__) . 'images/icon.png');
	$pass->addFile(plugin_dir_path(__FILE__) . 'images/icon@2x.png');
	$pass->addFile(plugin_dir_path(__FILE__) . 'images/logo.png');
	$pass->addFile(plugin_dir_path(__FILE__) . 'images/logo@2x.png');

	if ( ! empty($post->_thumbnail_id) && $img = wp_get_attachment_image_src($post->_thumbnail_id, 'full')) {
		$tmp1 = tempnam(sys_get_temp_dir(), '');
		$tmp2 = tempnam(sys_get_temp_dir(), '');

		$imagick = new Imagick($img[0]);
		$imagick->setImageFormat("png");
		$imagick2 = clone $imagick;

		$imagick->resizeImage(375, 98, Imagick::FILTER_LANCZOS, 1, true);
		$imagick->writeImage($tmp1);
		$imagick->clear();

		$imagick2->resizeImage(750, 196, Imagick::FILTER_LANCZOS, 1, true);
		$imagick2->writeImage($tmp2);
		$imagick2->clear();

		$pass->addFile($tmp1, 'strip.png');
		$pass->addFile($tmp2, 'strip@2x.png');
	}

	// Create and output the pass
	if( ! $pass->create(true)) {
		return array('error' => $pass->getError());
	}

	// delete temp files
	if (isset($tmp1)) {
		unlink($tmp1);
		unlink($tmp2);
	}
}

add_action('admin_init', 'ios_settings_init', 20);
function ios_settings_init() {
	add_settings_section("ios_keys", "iOS", 'ios_section_callback', "api-keys");

	add_settings_field("passTypeIdentifier", "passTypeIdentifier", "display_passTypeIdentifier", "api-keys", "ios_keys");
	add_settings_field("teamIdentifier", "teamIdentifier", "display_teamIdentifier", "api-keys", "ios_keys");
	add_settings_field("pkpass_cert", "Certificate", "display_pkpass_cert", "api-keys", "ios_keys");
	add_settings_field("pkpass_cert_pass", "Certificate Password", "display_pkpass_cert_pass", "api-keys", "ios_keys");

	register_setting('api_keys', 'passTypeIdentifier');
	register_setting('api_keys', 'teamIdentifier');
	register_setting('api_keys', 'pkpass_cert', array('sanitize_callback' => 'sanitize_pkpass_cert'));
	register_setting('api_keys', 'pkpass_cert_pass');
}

function ios_section_callback() {
	echo 'Used for pkpass.';
}

function display_passTypeIdentifier() {
	echo '<input type="text" name="passTypeIdentifier" value="'. get_option('passTypeIdentifier') . '" class="regular-text" />';
}
function display_teamIdentifier() {
	echo '<input type="text" name="teamIdentifier" value="'. get_option('teamIdentifier') . '" class="regular-text" />';
}
function display_pkpass_cert() {
	echo '<input type="file" name="pkpass_cert" accept=".p12" />';
}
function display_pkpass_cert_pass() {
	echo '<input type="text" name="pkpass_cert_pass" value="'. get_option('pkpass_cert_pass') . '" class="regular-text" />';
}

function sanitize_pkpass_cert() {
	$cert_path = '';
	if (isset($_FILES['pkpass_cert']) && UPLOAD_ERR_OK == $_FILES['pkpass_cert']['error']) {
		$cert_path = plugin_dir_path(__FILE__) . 'certificate.p12';
		move_uploaded_file($_FILES['pkpass_cert']['tmp_name'], $cert_path);
	}
	return $cert_path;
}

/**
* Convert a hexa decimal color code to its RGB equivalent
*
* @param string $hexStr (hexadecimal color value)
* @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
* @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
* @return array or string (depending on second parameter. Returns False if invalid hex color value)
*/                                                                                                 
function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
    $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
    $rgbArray = array();
    if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
    	$colorVal = hexdec($hexStr);
    	$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
    	$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
    	$rgbArray['blue'] = 0xFF & $colorVal;
    } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
    	$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
    	$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
    	$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
    } else {
        return false; //Invalid hex color code
    }
    return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
}

function make_strip_png($url) {

}