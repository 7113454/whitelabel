<?php
/*
Plugin Name: ICS Importer
Plugin URI: 
Description: Import events from ICS Calendar
Author: ub.fitness
Version: 1.0
Author URI: 
*/

add_action('admin_menu', 'ics_import_menu');
function ics_import_menu() {
	add_management_page('ICS Importer', 'ICS Importer', 'manage_options', 'ics-import-page.php', 'ics_import_page');
}

function ics_import_page() {
	require plugin_dir_path(__FILE__) . 'lib/ICal/EventObject.php';
	require plugin_dir_path(__FILE__) . 'lib/ICal/ICal.php';

	remove_filter('wp_insert_post', 'ub_custom_fields');

	if (isset($_FILES['ics_file'])) {
		$importCnt = 0;
		$ical = new ICal\ICal($_FILES['ics_file']['tmp_name']);

		foreach ($ical->events() as $ev) {
			ics_create_post($ev) and $importCnt++;
		}
		$msg = sprintf('Events in ICS: %d. Imported: %d', $ical->eventCount, $importCnt);
	}

	require plugin_dir_path(__FILE__) . 'ics-import-page.php';
}

function ics_create_post(ICal\EventObject $event) {
	$post = array(
		'post_title' => $event->summary,
		'post_status' => $_POST['post_status'],
		'post_author' => $_POST['user'],
		'post_category' => array($_POST['cat']),
	);


	$post_id = wp_insert_post($post);

	if ($post_id) {
		update_post_meta($post_id, 'date', strtotime($event->dtstart));
		update_post_meta($post_id, 'date_end', strtotime($event->dtend));
		
		if ( ! empty($_POST['post_tags'])) {
			wp_set_post_tags($post_id, $_POST['post_tags']);
		}

		$l = 'TBC' == $event->location ? false : ub_geocode($event->location);
		if ($l) {
			update_post_meta($post_id, 'location', array('address' => $event->location, 'lat' => $l['lat'], 'lng' => $l['lng']));
		}
	}

	return $post_id;
}