<div class="wrap">
	<h2>ICS Importer</h2>

	<?php if ( ! empty($msg)): ?>
		<div class="notice notice-success is-dismissible"><p><?= $msg ?></p></div>
	<?php endif ?>

	<form method="post" action="" enctype="multipart/form-data">
		<table class="form-table">
			<tr>
				<th>Post Owner</th>
				<td><?php wp_dropdown_users() ?></td>
			</tr>
			<tr>
				<th>Post Status</th>
				<td>
					<select name="post_status">
					<?php
						foreach (get_post_statuses() as $k => $v) {
							printf('<option value="%s" %s>%s</option>', $k, ('publish' == $k ? 'selected' : ''), $v);
						}
					?>
					</select>
				</td>
			</tr>
			<tr>
				<th>Post Category</th>
				<td><?php wp_dropdown_categories(array('hierarchical' => 1, 'orderby' => 'name')) ?></td>
			</tr>
			<tr>
				<th>Post Tags</th>
				<td>
					<input type="text" class="regular-text" name="post_tags">
					<p class="description">Comma separated</p>
				</td>
			</tr>
			<tr>
				<th>ICS File</th>
				<td>
					<input type="file" name="ics_file">
					<p class="description"><?php printf( __( 'Maximum upload file size: %s.' ), esc_html(size_format(wp_max_upload_size()))) ?></p>
				</td>
			</tr>
		</table>
		<p class="submit"><button type="submit" class="button button-primary">Import</button></p>
	</form>
</div>