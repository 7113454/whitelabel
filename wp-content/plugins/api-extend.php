<?php
/*
Plugin Name: API Extend
Plugin URI: 
Description: Customise functionality of the "WP REST API" plugin for ub.fitness
Author: ub.fitness
Version: 1.0
Author URI: 
*/

add_action('rest_api_init', 'api_additional_fields');
function api_additional_fields() {
	register_rest_field('post', 'image', array('get_callback' => 'api_post_image'));
	register_rest_field('post', 'author_info', array('get_callback' => 'api_post_author_info'));
	register_rest_field('post', 'meta', array('get_callback' => 'api_post_meta'));
	register_rest_field('post', 'comments', array('get_callback' => 'api_last_comment'));

	register_rest_field('user', 'meta', array('get_callback' => 'api_user_meta'));

	register_rest_route('wp/v2', '/signup/', array('methods' => 'POST', 'callback' => 'api_signup'));
	register_rest_route('wp/v2', '/lostpassword', array('methods' => 'POST', 'callback' => 'api_lostpassword'));
	register_rest_route('wp/v2', '/favorites', array(
		array('methods' => 'GET', 'callback' => 'api_favorites'),
		array('methods' => 'POST', 'callback' => 'api_favorites_add')
	));
	register_rest_route('wp/v2', '/favorites/(?P<id>[\d]+)', array('methods' => 'DELETE', 'callback' => 'api_favorites_delete'));
	register_rest_route('wp/v2', '/favorites/(?P<id>[\d]+)', array('methods' => 'GET', 'callback' => 'api_favorites_get'));

	register_rest_route('wp/v2', '/subscribe', array('methods' => 'POST', 'callback' => 'api_subscribe'));
	register_rest_route('wp/v2', '/report', array('methods' => 'POST', 'callback' => 'api_report'));

	// register_rest_route('wp/v2', '/active', array('methods' => 'GET', 'callback' => 'api_active_activities'));
	register_rest_route('wp/v2', '/usp-categories', array('methods' => 'GET', 'callback' => 'api_usp_categories'));
	register_rest_route('wp/v2', '/counter', array('methods' => 'GET', 'callback' => 'api_counter'));
	register_rest_route('wp/v2', '/around', array('methods' => 'GET', 'callback' => 'api_around'));
	register_rest_route('wp/v2', '/purchase', array('methods' => 'POST', 'callback' => 'api_purchase'));

	register_rest_route('wp/v2', '/share2followers', array('methods' => 'POST', 'callback' => 'share_with_followers'));
	register_rest_route('wp/v2', '/new', array(
		array('methods' => 'GET', 'callback' => 'rest_get_new'),
		array('methods' => 'DELETE', 'callback' => 'rest_flush_new'),
	));

	register_rest_route('wp/v2', '/attending/(?P<id>[\d]+)', array(
		array('methods' => 'GET', 'callback' => 'api_attending_members'),
		array('methods' => 'POST', 'callback' => 'api_attending'),
	));
}

function api_post_image($object) {
//	return wp_get_attachment_image_src($object['featured_media'], array('thumbnail', 'medium'));
	return get_the_post_thumbnail_url($object['id'], 'large');
}

$users_cache = array();

function api_post_author_info($object) {
	global $users_cache;
	$out = isset($users_cache[$object['author']]) ? $users_cache[$object['author']] : array();
	$user_id = get_current_user_id();
	$wpsamodel = new Wpsa_Model();

	$user = $out ? false : get_user_by('ID', $object['author']);
	if ($user) {
		$out = array(
			'user_login' => $user->user_login,
			'user_nicename' => $user->user_nicename,
			'user_registered' => $user->user_registered,
			'display_name' => $user->display_name,
			'avatar' => rest_get_avatar_urls($user->ID),
			'bg_image' => ub_user_background($user),
			'following' => $wpsamodel->is_user_subscribed($user->ID, $user_id),
		);

		$users_cache[$object['author']] = $out;
	}

	return $out;
}

function api_user_meta($obj) {
	global $wpdb;
	$user = get_user_by('ID', $obj['id']);

	$sql = "SELECT author_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE subscriber_id = {$obj['id']} ORDER BY created_at DESC, id";
	$following = $wpdb->get_col($sql);

 	$sql = "SELECT subscriber_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE author_id = {$obj['id']} ORDER BY created_at DESC, id";
 	$followers = $wpdb->get_col($sql);

	$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}attend
		JOIN {$wpdb->posts} ON post_id = ID
		WHERE user_id = {$obj['id']} AND post_status IN ('publish', 'featured')";
	$attend_cnt = $wpdb->get_var($sql);

	$favorites = get_user_likes($obj['id']);
	$sql = "SELECT COUNT(*) FROM {$wpdb->posts} WHERE ID IN(" . implode(',', $favorites) . ") AND post_status IN ('publish', 'featured')";
	$like_cnt = $favorites ? $wpdb->get_var($sql) : 0;

	$out = array(
		'bg_image' => ub_user_background($user),
		'following' => $following,
		'followers' => $followers,
		'attend_cnt' => $attend_cnt,
		'like_cnt' => $like_cnt,
		'topics_cnt' => ub_count_post($obj['id']),
	);
	return $out;
}

add_filter('rest_avatar_sizes', 'api_avatar_sizes');
function api_avatar_sizes($sizes) {
	return array(150, 40);
}

function api_signup(WP_REST_Request $request) {
	$errors = new WP_Error();
	$access_token = $request->get_param('access_token');

	if ($access_token) { // facebook connect
		return api_facebook_signup($access_token);
	}
	else {
		if ( ! filter_var($request->get_param('email'), FILTER_VALIDATE_EMAIL)) {
			$errors->add('invalid_email', "The email address isn't correct.");
		}
		if (strlen($request->get_param('password')) < 6) {
			$errors->add('short_password', "Password should be at least 6 symbols.");
		}
		if ($errors->get_error_code()) {
			return $errors;
		}

		$user_id = wp_create_user($request->get_param('login'), $request->get_param('password'), $request->get_param('email'));
	}

	return $user_id;
}

function api_lostpassword() {
	ob_start();
	require ABSPATH . 'wp-login.php';
	ob_clean();
	return retrieve_password();
}

/**
 * @param array           $args    Key value array of query var to query value.
 * @param WP_REST_Request $request The request used.
 */
add_filter('rest_post_query', 'api_include_status', 10, 2);
function api_include_status($args, $request) {
	global $wpdb;
	$args['post_type'] = $request->get_param('post_type') ? $request->get_param('post_type') : array('post', 'status');

	// plugin forces status=published and we cannot see the featured,
	// so remove it from query if the status is not set in URL
	if (empty($_GET['status'])) {
		unset($args['post_status']);
	}

	if ($request->get_param('hot')) {
//		$ids = get_top_posts_id($request['page'], $request['per_page']);
		$ids = get_top_posts_id(1, 60);
		$args['post__in'] = $ids;
		$args['orderby'] = 'post__in';
	}

	if ($request->get_param('meta_value_num')) {
		// show only posts when ordering by start/end date
		if (in_array($request->get_param('meta_value_num'), array('date', 'date_end'))) {
			$args['post_type'] = 'post';
		}

		$args['orderby'] = 'meta_value_num';
		$args['meta_key'] = $request->get_param('meta_value_num');
	}

	if ( ! empty($request['ub_latest'])) {
		$user = wp_get_current_user();
		$sql = "SELECT author_id FROM {$wpdb->prefix}wpsa_subscribe_author WHERE subscriber_id = {$user->ID}";
		$following = $wpdb->get_col($sql);
		$args['author__in'] = $following ? $following : array(0);
	}

	if ($request->get_param('like_by')) {
		$ids = get_user_likes($request->get_param('like_by'));
		$args['post__in'] = $ids ? $ids : array(0);
	}

	if ($request->get_param('attend_by')) {
		$sql = $wpdb->prepare("SELECT post_id FROM {$wpdb->prefix}attend WHERE user_id = %d", $request->get_param('attend_by'));
		$ids = $wpdb->get_col($sql);
		$args['post__in'] = $ids ? $ids : array(0);
	}

	return $args;
}

// we cannot sort by meta key, fixing it
add_filter('rest_query_vars', 'api_allow_orberby_meta');
function api_allow_orberby_meta($vars) {
	$vars[] = 'meta_key';
	$vars[] = 'post_type';
	return $vars;
}

/**
 * Get the value of the "starship" field
 *
 * @param array $obj Details of current post.
 * @param string $fname Name of field.
 * @param WP_REST_Request $request Current request
 *
 * @return mixed
 */
function api_post_meta($obj, $fname, $request) {
	global $wpdb;

	$allowed = array('location', 'date', 'date_end', 'views', 'repeat', 'info_url', 'youtube', 'rrule');
	$meta = array();
	$user_id = get_current_user_id();
	$all = get_post_meta($obj['id']);

	foreach ($all as $k => $m) {
		if (in_array($k, $allowed)) {
			if ('date' == $k || 'date_end' == $k) {
				$time = $m[0] ? $m[0] : 1;
				$meta[$k] = date('m/d/Y H:i', $time); // convert timestamp to date for compatibility
			}
			else {
				$meta[$k] = $m[0];
			}
		}
	}

	$meta['like_cnt'] = (int) get_post_meta($obj['id'], '_like', true);


	// fields for details page only
//	if ('get_item' == $request->get_attributes()['callback'][1]) {
		$sql = $wpdb->prepare("SELECT user_id FROM {$wpdb->prefix}attend WHERE post_id = %d", $obj['id']);
		$meta['attending_users'] = $wpdb->get_col($sql);

		$meta['attending'] = is_attending($obj['id'], $user_id);
//	}

	if (isset($meta['location'])) {
		$meta['location'] = unserialize($meta['location']);
	}

	$meta['favorite'] = do_i_like($obj['id']);
	$meta['featured'] = 'featured' == $obj['status'];

	$meta['tags'] = array();
	if ($posttags = get_the_tags($obj['id'])) {
		foreach ($posttags as $t) {
			$meta['tags'][] = array('id' => $t->term_id, 'name' => $t->name);
		}
	}

	$meta['repeat'] = isset($meta['repeat']) ? $meta['repeat'] : 'no';

	return $meta;
}

function api_favorites(WP_REST_Request $request) {
	$fav = get_user_likes();
	$page = isset($request['page']) ? intval($request['page']) : 1;
	$request['page'] = 1;

	if ($fav) {
		$rest = new WP_REST_Posts_Controller('post');
		$params = array();
		
		foreach ($rest->get_collection_params() as $k => $p) {
			if (empty($p['default'])) continue;
			$params[$k] = isset($request[$k]) ? $request[$k] : $p['default'];
		}

		$start = ($page - 1) * $params['per_page'];
		$ids = array_slice($fav, $start, $params['per_page']);
		$params['include'] = $ids;

		$request->set_default_params($params);

		$response = $rest->get_items($request);
		// var_dump($response);exit;

		$total_posts = count($fav);
		$max_pages = ceil($total_posts / $params['per_page']);
		$response->header('X-WP-Total', $total_posts);
		$response->header('X-WP-TotalPages', $max_pages);

		$request_params = $request->get_query_params();
		$base = add_query_arg( $request_params, rest_url( 'wp/v2/favorites' ));
		if ( $page > 1 ) {
			$prev_page = $page - 1;

			if ( $prev_page > $max_pages ) {
				$prev_page = $max_pages;
			}

			$prev_link = add_query_arg( 'page', $prev_page, $base );
			$response->link_header( 'prev', $prev_link );
		}
		if ( $max_pages > $page ) {
			$next_page = $page + 1;
			$next_link = add_query_arg( 'page', $next_page, $base );

			$response->link_header( 'next', $next_link );
		}

		return $response;
	}
	else {
		return array();
	}
}

function api_favorites_add(WP_REST_Request $request) {
	ub_toggle_like($request->get_param('post_id'));
	return 'OK';
}

function api_favorites_delete(WP_REST_Request $request) {
	$id = (int) $request['id'];
	ub_toggle_like($id);
	return 'OK';
}

function api_favorites_get(WP_REST_Request $request) {
	global $wpdb;
	$sql = $wpdb->prepare("SELECT user_id FROM {$wpdb->prefix}feed WHERE post_id = %d AND action = 'like'", $request['id']);
	return $wpdb->get_col($sql);
}

// additional fields for account OWNER
add_filter('rest_prepare_user', 'api_add_user_fields', 10, 3);
function api_add_user_fields($response, $user, $request) {
	// count mobile views
	if (is_object($request) && 'get_item' == $request->get_attributes()['callback'][1]) {
		ub_count_user($user->ID);
	}

	if (get_current_user_id() == $user->ID) {
		$notif = new VU_Notifications();
		$response->data['username'] = $user->user_login;
		$response->data['email'] = $user->user_email;
		$response->data['first_name'] = $user->first_name;
		$response->data['last_name'] = $user->last_name;
		$response->data['nickname'] = $user->nickname;
		$response->data['notifications'] = $notif->toArray();
	}
	return $response;
}

function api_subscribe(WP_REST_Request $request) {
	if (get_current_user_id() != $request['subscriber_id']) {
		return new WP_Error( 'cheater', 'Cannot follow.', array( 'status' => 403 ) );
	}

	if ($request['author_id'] == $request['subscriber_id']) {
		return new WP_Error( 'cannot_follow', 'Cannot follow.', array( 'status' => 400 ) );
	}
	return wpsa_subscribe_author_handle();
}

add_action('rest_insert_user', 'api_insert_user', 10, 3);
function api_insert_user(WP_User $user, WP_REST_Request $request, $creating) {
	// update user background image
	if ( ! empty($request['bg'])) {
		$img_id = ub_create_image($request['bg']);
		ub_delete_bg($user); // delete old image
		update_user_meta($user->ID, 'ub_background', $img_id);
	}
	elseif (isset($request['bg'])) { // remove background image
		ub_delete_bg($user);
	}

	if ( ! empty($request['avatar'])) {
		$media_id = ub_create_image($request['avatar']);
		if ($media_id) {
			avatar_delete(); // delete old image
			update_user_meta($user->ID, 'wp_user_avatar', $media_id);
		}
	}
	elseif (isset($request['avatar'])) {
		avatar_delete();
	}

	if ($request['notifications']) {
		$notif = new VU_Notifications($user->ID);
		$notif->subscribeApi($request['notifications']);
	}
}

/**
 * Filter user data before inserting user via the REST API.
 *
 * @param object          $prepared_user User object.
 * @param WP_REST_Request $request       Request object.
 */
add_filter('rest_pre_insert_user', 'api_pre_insert_user', 10, 2);
function api_pre_insert_user($prepared_user, $request) {
	if (isset($request['action']) && 'close' == $request['action']) {
		$prepared_user->role = 'closed';
	}
	return $prepared_user;
}

/**
 * Filter a post before it is inserted via the REST API.
 *
 * @param stdClass        $prepared_post An object representing a single post prepared
 * @param WP_REST_Request $request       Request object.
 */
add_filter('rest_pre_insert_post', 'api_pre_insert_post', 10, 2);
function api_pre_insert_post($prepared_post, $request) {
	$date_start =  strtotime($_POST['date_start']);
	$date_end = strtotime($_POST['date_end']);

	if ( ! $date_start) {
		return new WP_Error( 'rest_invalid_date_start', __( 'Invalid start date' ), array( 'status' => 400 ) );
	}

	if ( ! $date_end || $date_end < $date_start) {
		return new WP_Error( 'rest_invalid_date_end', __( 'Invalid end date' ), array( 'status' => 400 ) );
	}

	if (isset($prepared_post->ID)) {
		unset($prepared_post->post_status); // don't allow status change
	}
	else {
		$prepared_post->post_status = 'publish';
	}
	return $prepared_post;
}

function api_report(WP_REST_Request $request) {
	$errors = new WP_Error();
	$user = wp_get_current_user();
	$item = in_array($request['item'], array('user', 'post')) ? $request['item'] : 'post';

	if ( empty($user->ID)) {
		$errors->add('invalid_login', "You are not logged in.");
	}

	if ( ! $errors->get_error_code()) {
		$attr = array(
			'user_id' => $user->ID,
			'item_type' => $item,
			'item_id' => $request['id'],
			'subject' => $request['subject'],
			'message' => $request['message'],
			'agent' => $_SERVER['HTTP_USER_AGENT'],
			'date' => date('Y-m-d H:i:s'),
			'ip' => $_SERVER['REMOTE_ADDR'],
		);
		ub_create_complaint($attr);
		return array('message' => 'Success.');
	}

	return $errors;
}

// function api_active_activities() {
// 	global $wpdb;

// 	$sql = "SELECT p.ID, p.post_title, pm.meta_value AS 'date', loc.meta_value AS 'location', IF(STRCMP(p.post_status, 'featured'), 0, 1) AS 'featured' "
// 		. "FROM {$wpdb->postmeta} AS pm "
// 		. "JOIN {$wpdb->posts} as p ON pm.post_id = p.ID AND p.post_type = 'post' "
// 		. "JOIN {$wpdb->postmeta} AS loc ON pm.post_id = loc.post_id AND loc.meta_key = 'location' "
// 		. "WHERE pm.meta_key = 'date' AND STR_TO_DATE(pm.meta_value, '%m/%d/%Y %H:%i') > NOW() ";
// 	$out = $wpdb->get_results($sql, ARRAY_A);

// 	foreach ($out as &$o) {
// 		$o['location'] = unserialize($o['location']);
// 	}

// 	return $out;
// }

function api_usp_categories() {
	global $wpdb;
	$usp_options = get_option('usp_options');

	$sql = "SELECT t.term_id, t.name "
		. "FROM {$wpdb->term_taxonomy} AS tt "
		. "JOIN {$wpdb->terms} AS t ON tt.term_id = t.term_id "
		. "WHERE tt.taxonomy = 'category' "
		. "ORDER BY t.name ";
	return $wpdb->get_results($sql);
}

add_action('rest_insert_post', 'api_insert_post', 10, 3);
/**
 * Fires after a single post is created or updated via the REST API.
 *
 * @param object          $post      Inserted Post object (not a WP_Post object).
 * @param WP_REST_Request $request   Request object.
 * @param boolean         $creating  True when creating post, false when updating.
 */
function api_insert_post($post, $request, $creating) {
	if (isset($request['tags_string'])) {
		wp_set_post_tags($post->ID, $request['tags_string']);
	}
}

function api_counter(WP_REST_Request $request) {
	switch ($request['item']) {
		case 'post':
			ub_count($request['item_id']);
			break;
	}
	return array('status' => 0);
}

function api_around(WP_REST_Request $request) {
	global $wpdb;
	$order = array();
	$out = array();

	$sql = "SELECT p.ID, p.post_title, pm.meta_value AS 'date', loc.meta_value AS 'location', IF(STRCMP(p.post_status, 'featured'), 0, 1) AS 'featured' "
		. "FROM {$wpdb->postmeta} AS pm "
		. "JOIN {$wpdb->posts} as p ON pm.post_id = p.ID AND p.post_type = 'post' "
		. "JOIN {$wpdb->postmeta} AS loc ON pm.post_id = loc.post_id AND loc.meta_key = 'location' "
		. "WHERE pm.meta_key = 'date_end' AND FROM_UNIXTIME(pm.meta_value) > NOW() ";
	$posts = $wpdb->get_results($sql, ARRAY_A);

	foreach ($posts as $k => $p) {
		$location = unserialize($p['location']);
		$posts[$k]['date'] = date('m/d/Y H:i', $p['date']);
		$posts[$k]['location'] = $location;

		if (isset($request['radius']) && $request['radius'] > 0) {
			$dist = calculateDistance($request['lat'], $request['lng'], $location['lat'], $location['lng'], 'M');
			if ($dist > $request['radius']) {
				unset($posts[$k]);
			}
			else {
				$order[$k] = $dist;
			}
		}
	}

	if (isset($request['radius']) && $request['radius'] > 0) {
		// sort by distance, closest first
		asort($order);
		foreach ($order as $k => $v) {
			$posts[$k]['image'] = get_the_post_thumbnail_url($posts[$k]['ID'], 'large');
			$out[] = $posts[$k];
		}
	}
	else {
		$out = array_values($posts);
	}

	return $out;
}

function api_purchase(WP_REST_Request $request) {
	$rcpt = 'serge@enrappture.com, dan@enrappture.com';

	$user = wp_get_current_user();
	$post = get_post($request['post_id']);

	if ($post) {
		wp_update_post(array('ID' => $post->ID, 'post_status' => 'featured'));
	}

	$subj = '[IMPORTANT] ub.fitness mobile payment';
	$msg = "The member [{$user->user_login}] paid for the activity [{$post->post_title}]\r\n\r\n";
	$msg .= "INFO:\r\npost_id: {$request['post_id']}\r\ndata: {$request['data']}\r\n";

	wp_mail($rcpt, $subj, $msg);
	return array();
}

function api_facebook_signup($access_token) {
	global $wpdb;
	require(get_template_directory() . '/inc/Facebook/autoload.php');
	$errors = new WP_Error();

	$settings = maybe_unserialize(get_option('nextend_fb_connect'));
	$fb = new Facebook\Facebook(array(
		'app_id' => $settings['fb_appid'],
		'app_secret' => $settings['fb_secret'],
	));

	try {
		$response = $fb->get('/me?fields=id,name,email,first_name,last_name', $access_token);
		$fb_user = $response->getGraphUser();
	}
	catch (Exception $e) {
		$errors->add('fb_error', $e->getMessage());
		return $errors;
	}

	$sql = $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}social_users WHERE type = 'fb' AND identifier = '%d'", $fb_user['id']);
	$ID = $wpdb->get_var($sql);

	if ( ! get_user_by('id', $ID)) {
		$wpdb->query($wpdb->prepare('DELETE FROM ' . $wpdb->prefix . 'social_users WHERE ID = "%d"', $ID));
		$ID = null;
	}

	if (null == $ID) {
		if (!isset($fb_user['email'])) $fb_user['email'] = $fb_user['id'] . '@facebook.com';
		$ID = email_exists($fb_user['email']);

		if ($ID == false) { // Real register
			$pass = wp_generate_password();
			$username = sanitize_user(strtolower($fb_user['first_name'] . $fb_user['last_name']));
			if ( ! validate_username($username)) {
				$username = sanitize_user($fb_user['id']);
			}

			$ID = wp_create_user($username, $pass, $fb_user['email']);
			if ( ! is_wp_error($ID)) {
				wp_new_user_notification($ID, $pass);
				wp_update_user(array(
					'ID' => $ID,
					'display_name' => $fb_user['name'],
					'first_name' => $fb_user['first_name'],
					'last_name' => $fb_user['last_name']
				));
			}
			else {
				return;
			}

		}

		if ($ID) {
			$wpdb->insert($wpdb->prefix . 'social_users', array('ID' => $ID, 'type' => 'fb', 'identifier' => $fb_user['id']) , array('%d', '%s', '%s'));
		}
	}

	if ($ID) {
		$user = get_userdata($ID);
		update_user_meta($ID, 'fb_user_access_token', $access_token);

		// authenticate user
		wp_set_current_user($user->ID);

		$wp_rest = new WP_REST_Users_Controller();
		$user = $wp_rest->prepare_item_for_response($user, false);
		$response = rest_ensure_response($user);

		return $response;
	}
	else {
		$errors->add('unhandled', 'Unexpected error.');
		return $errors;
	}
}

// login with facebook access token instead of password
add_filter('determine_current_user', 'api_auth_fb');
function api_auth_fb($user) {
	if ( ! empty($user) ) return $user;

	$username = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : false;
	$token = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : false;

	$u = get_user_by('login', $username);
	$stored_token = $u ? get_user_meta($u->ID, 'fb_user_access_token', true) : false;

	if ($u && $token && $stored_token == $token) {
		return $u->ID;
	}
	else {
		return false;
	}
}

function share_with_followers(WP_REST_Request $request) {
	global $wpdb;

	$user = wp_get_current_user();
	if (empty($user->ID)) {
		return new WP_Error( 'rest_not_logged_in', __( 'You are not currently logged in.' ), array( 'status' => 401 ) );
	}

	$post = get_post($request['post_id']);
	if ( ! $post) {
		return new WP_Error( 'rest_post_invalid_id', __( 'Invalid post id.' ), array( 'status' => 404 ) );
	}

	$sql = "SELECT u.ID, display_name, user_email
		FROM {$wpdb->prefix}wpsa_subscribe_author AS sa
		JOIN {$wpdb->users} AS u ON sa.subscriber_id = u.ID
		WHERE author_id = {$user->ID}";
	$followers = $wpdb->get_results($sql);

	$search = ['%rcpt-name%', '%post-title%', '%post-url%', '%from-name%'];
	$replace = ['', $post->post_title, get_permalink($post) . '?sb=' . $user->user_nicename, $user->display_name];

	$subj = str_replace($search, $replace, get_option('share_subj'));
	$msg = get_option('share_body');

	foreach ($followers as $f) {
		$replace[0] = $f->display_name;
		wp_mail($f->user_email, $subj, str_replace($search, $replace, $msg));
	}

	return array('msg' => 'Success');
}

function rest_get_new() {
	$user = wp_get_current_user();
	if (empty($user->ID)) {
		return new WP_Error( 'rest_not_logged_in', __( 'You are not currently logged in.' ), array( 'status' => 401 ) );
	}

	$new = get_user_meta($user->ID, 'unread', true);
	$new = is_array($new) ? $new : array();
	return $new;
}

function rest_flush_new(WP_REST_Request $request) {
	$user = wp_get_current_user();
	if (empty($user->ID)) {
		return new WP_Error( 'rest_not_logged_in', __( 'You are not currently logged in.' ), array( 'status' => 401 ) );
	}

	$new = array();

	// parse post_id parameter and remove appropriate values from $new
	parse_str(file_get_contents('php://input'), $delete_args);
	if (isset($delete_args['post_id'])) {
		$post_id = is_array($delete_args['post_id']) ? $delete_args['post_id'] : explode(',', $delete_args['post_id']);
		$new = get_user_meta($user->ID, 'unread', true);

		foreach ($post_id as $id) {
			if (false !== ($k = array_search($id, $new))) {
				unset($new[$k]);
			}
		}
		$new = array_values($new);
	}

	update_user_meta($user->ID, 'unread', $new);

	return $new;
}

function api_last_comment($object) {
	$ret = array();
	$args = array('post_id' => $object['id'], 'status' => 'approve', 'number' => 1);
	$q = new WP_Comment_Query();
	$comments = $q->query($args);
	foreach ($comments as $comment) {
		$ret[] = array(
			'id'                 => (int) $comment->comment_ID,
			'post'               => (int) $comment->comment_post_ID,
			'parent'             => (int) $comment->comment_parent,
			'author'             => (int) $comment->user_id,
			'author_name'        => $comment->comment_author,
			'author_url'         => $comment->comment_author_url,
			'date'               => mysql_to_rfc3339( $comment->comment_date ),
			'date_gmt'           => mysql_to_rfc3339( $comment->comment_date_gmt ),
			'content'            => array(
				/** This filter is documented in wp-includes/comment-template.php */
				'rendered' => apply_filters( 'comment_text', $comment->comment_content, $comment ),
				'raw'      => $comment->comment_content,
			),
			'link'               => get_comment_link( $comment ),
			'status'             => 'approved',
			'type'               => get_comment_type( $comment->comment_ID ),
			'author_avatar_urls' => rest_get_avatar_urls( $comment->comment_author_email )
		);
	}
	return $ret;
}
