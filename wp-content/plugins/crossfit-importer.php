<?php
/*
Plugin Name: Import CrossFit 1971
Plugin URI: 
Description: Import events for http://crossfit1971.com/
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'cf_activation');
function cf_activation() {
	if ( ! wp_next_scheduled ('cf_cronjob')) {
		wp_schedule_event(time(), 'weekly', 'cf_cronjob');
	}
}

register_deactivation_hook(__FILE__, 'cf_deactivation');
function cf_deactivation() {
	wp_clear_scheduled_hook('cf_cronjob');
}

add_action('cf_cronjob', 'cf_run');
function cf_run() {
	global $wpdb;

	remove_filter('wp_insert_post', 'ub_custom_fields');

	$week = date('Y-W', strtotime('+1 week'));
	if ($week <= get_option('_cf_last_update')) return;

	$source = 'https://goteamup.com/w256766/event_feed/?feed_type=provider&feed_type_id=256766&keys='.$week;
	$response = wp_remote_get($source);
	if ( ! is_array($response) || 200 != $response['response']['code']) {
		trigger_error('Import: cannot fetch data.', E_USER_WARNING);
		return false;
	}

	$data = json_decode($response['body']);

	foreach ($data->events as $e) {
		$id = cf_import($e);
	}

	update_option('_cf_last_update', $week, 'no');
}

function cf_import($event) {
	$location = array(
		'address' => 'Unit 3 Hutchins Close, Stratford E15 2HU, UK',
		'lat' => '51.538537',
		'lng' => '-0.006146'
	);

	$post = array(
		'post_author' => 802,
		'post_category' => [210],
		'tags_input' => ['Crossfit', 'crossfit1971', 'crossfit 1971'],

		'post_title' => $event->name,
		'post_content' => $event->description,
		'post_status' => 'publish',
		'meta_input' => array(
			'date' => strtotime($event->start),
			'date_end' => strtotime($event->end),
			'location' => $location,
			'info_url' => 'http://crossfit1971.com/',
			'_thumbnail_id' => 15731
		),
	);

	$id = wp_insert_post($post);

	return $id;
}
