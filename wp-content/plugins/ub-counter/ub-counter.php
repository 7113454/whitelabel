<?php
/*
Plugin Name: UB-Counter
Plugin URI: 
Description: Post view counter for ub.fitness. Shortcodes [ub_top_posts limit="10"]
Author: ub.fitness
Version: 1.0
Author URI: 
*/

define('UB_COUNT_FOR', '60 DAY');

// ACTIVATION
register_activation_hook(__FILE__, 'ub_activation');
function ub_activation() {
	global $wpdb;

	wp_schedule_event(time(), 'daily', 'ub_cron_daily');

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE {$wpdb->prefix}post_visits (
			`post_id` bigint unsigned NOT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`ip` varchar(15),
			INDEX post_id (post_id,date)
			) " . $charset_collate;

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

	$sql = "CREATE TABLE {$wpdb->prefix}counter_users (
			`user_id` bigint unsigned NOT NULL,
			`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			`ip` varchar(15),
			INDEX user_id (user_id,date)
			) " . $charset_collate;
	dbDelta( $sql );
}

// DEACTIVATION
register_deactivation_hook(__FILE__, 'ub_deactivation');
function ub_deactivation() {
	wp_clear_scheduled_hook('ub_cron_daily');
}

function ub_count($post_id = false) {
	global $wpdb;

	// don't count search crawlers
	if (ub_is_bot()) return false;

	$post_id = $post_id ? $post_id : get_the_ID();
	$ip = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];
	$data = array('post_id' => $post_id, 'ip' => $ip, 'date' => current_time('mysql', 1), 'agent' => $_SERVER['HTTP_USER_AGENT']);

//	$sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}post_visits WHERE post_id = %d AND ip = %s AND DATE(date) = DATE(%s)", $data['post_id'], $data['ip'], $data['date']);
//	if ($wpdb->get_row($sql)) return false; // count only one view per IP a day

	$wpdb->insert($wpdb->prefix . 'post_visits', $data);

	$cnt = intval(get_post_meta($post_id, 'views', true));

	update_post_meta($post_id, 'views', $cnt + 1);
}

function ub_count_user($user_id) {
	global $wpdb;

	// don't count search crawlers
	if (ub_is_bot()) return false;

	// do not count self views
	if ($user_id == get_current_user_id()) return false;
	$ip = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];
	$data = array('user_id' => $user_id, 'ip' => $ip, 'date' => current_time('mysql', 1), 'agent' => $_SERVER['HTTP_USER_AGENT']);

	// $sql = $wpdb->prepare("SELECT * FROM {$wpdb->prefix}counter_users WHERE user_id = %d AND ip = %s AND DATE(date) = DATE(%s)", $data['user_id'], $data['ip'], $data['date']);
	// if ($wpdb->get_row($sql)) return false; // count only one view per IP a day

	$wpdb->insert($wpdb->prefix . 'counter_users', $data);

	$cnt = intval(get_user_meta($user_id, 'views', true));

	update_user_meta($user_id, 'views', $cnt + 1);
}

function get_user_views($user_id) {
	global $wpdb;

	$out = array(array('Date', 'Views'));
	$user_id = intval($user_id);
	$sql = "SELECT DATE_FORMAT(date, '%Y-%m-%d') AS mdate, COUNT(*) AS cnt FROM {$wpdb->prefix}counter_users WHERE user_id = $user_id GROUP BY mdate";
	$views = $wpdb->get_results($sql, OBJECT_K);
//	foreach ($wpdb->get_results($sql) as $i) {
//		$out[] = array($i->mdate, $i->cnt);
//	}

	$now = time();
	$time = strtotime('-1 month');
	while ($time < $now) {
		$key = date('Y-m-d', $time);
		$value = isset($views[$key]) ? $views[$key]->cnt : 0;
		$out[] = array(date('M, j', $time), $value);
		$time = strtotime('+1 days', $time);
	}

	return $out;
}

add_shortcode('ub_top_posts', 'ub_top_posts');
function ub_top_posts($atts) {
	global $wpdb;

	$page = get_query_var('paged') ? intval(get_query_var('paged')) : 1;
	$limit = empty($atts['limit']) ? 10 : $atts['limit'];
	$url = get_permalink();

	$ids = get_top_posts_id($page, $limit);

	$args = array(
		'ignore_sticky_posts' => true,
		'post__in' => $ids,
		'orderby' => 'post__in',
		'posts_per_page' => -1,
	);
	$q = new WP_Query($args);

	ob_start();
	echo '<div class="row">';
	while ($q->have_posts()) {
		$q->the_post();
		echo '<div class="col-md-4">';
		get_template_part('content', 'post');
		echo '</div>';
	}
	echo '</div>';
	wp_reset_postdata();

	if ($ids) {
		echo '<span class="pager next"><a href="'.get_permalink().'/page/'.($page+1).'">Next</a></span>';
	}

	return ob_get_clean();
}

function get_top_posts_id($page, $limit) {
	global $wpdb;

	$start = ($page - 1) * $limit;

	$ids = get_transient('hot_posts');
	if (false === $ids) {
		// $sql = "SELECT SQL_CALC_FOUND_ROWS post.ID
		// 	FROM {$wpdb->postmeta} AS date
		// 	JOIN {$wpdb->postmeta} AS view ON date.post_id = view.post_id AND view.meta_key = 'views'
		// 	LEFT JOIN {$wpdb->postmeta} AS not_hot ON date.post_id = not_hot.post_id AND not_hot.meta_key = '_not_hot'
		// 	JOIN {$wpdb->posts} AS post ON date.post_id = post.ID AND post_status IN('publish', 'featured')
		// 	WHERE date.meta_key = 'date_end' AND CAST(date.meta_value AS UNSIGNED) > UNIX_TIMESTAMP() AND not_hot.meta_value IS NULL
		// 	ORDER BY CAST(view.meta_value AS UNSIGNED) DESC
		// 	LIMIT $start,$limit ";

		$days = intval(get_option('hot_days'));
		$sql = "SELECT post.ID
			FROM {$wpdb->posts} AS post
			LEFT JOIN {$wpdb->prefix}post_visits AS pv ON post.ID = pv.post_id AND pv.date > DATE_SUB(CURDATE(), INTERVAL $days DAY)
			LEFT JOIN {$wpdb->postmeta} AS view ON post.ID = view.post_id AND view.meta_key = 'views'
			LEFT JOIN {$wpdb->postmeta} AS not_hot ON post.ID = not_hot.post_id AND not_hot.meta_key = '_not_hot'
			LEFT JOIN {$wpdb->postmeta} AS date ON post.ID = date.post_id AND date.meta_key = 'date_end'
			WHERE post_type='post' AND post_status IN('publish', 'featured')
				AND CAST(date.meta_value AS UNSIGNED) > UNIX_TIMESTAMP()
				AND (not_hot.meta_value IS NULL OR not_hot.meta_value = 0)
			GROUP BY post.ID
			ORDER BY COUNT(post.ID) DESC, MAX(view.meta_value) DESC
			LIMIT 250";
		$ids = $wpdb->get_col($sql);
		set_transient('hot_posts', $ids, 3 * HOUR_IN_SECONDS);
	}

	return array_slice($ids, $start, $limit);
}

// add_action('ub_cron_daily', 'ub_cron_daily');
// function ub_cron_daily() {
// 	// clear log table
// 	global $wpdb;

// 	$sql = "DELETE FROM {$wpdb->prefix}post_visits WHERE date < DATE_ADD(NOW(), INTERVAL -" . UB_COUNT_FOR . ")";
// 	$wpdb->query($sql);

// 	$sql = "DELETE FROM {$wpdb->prefix}counter_users WHERE date < DATE_ADD(NOW(), INTERVAL -" . UB_COUNT_FOR . ")";
// 	$wpdb->query($sql);
// }

function ub_is_bot($user_agent = '') {
	$user_agent = $user_agent ? $user_agent : $_SERVER['HTTP_USER_AGENT'];
	$bot = false;

	if (stripos($user_agent, 'bot') || stripos($user_agent, 'slurp') || false !== stripos($user_agent, 'facebook')) {
		$bot = true;
	}
	return $bot;
}

add_shortcode('ub-analytics-2', 'ub_analytics2');
function ub_analytics2() {
	global $wpdb;
	$user = wp_get_current_user();
	if ( ! $user->ID) return;

	// get data for specified post only
	$post = empty($_GET['post']) ? false : get_post($_GET['post']);
	if (NULL === $post || ($post && $post->post_author != $user->ID)) return 'Not Found';

	if (isset($_GET['paid']) && 1 == $_GET['paid']) $post_status = "post_status = 'publish'";
	elseif (isset($_GET['paid']) && 2 == $_GET['paid']) $post_status = "post_status = 'featured'";
	else $post_status = "post_status IN('publish', 'featured')";

	$sql = "SELECT ID, post_title FROM {$wpdb->posts}
		WHERE post_author = {$user->ID} AND post_type = 'post' AND $post_status
		ORDER BY post_title";
	$my_posts = $wpdb->get_results($sql, OBJECT_K);

//	if (empty($my_posts)) return 'No activities.';

	if (isset($_GET['date'])) {
		$d = explode('-', $_GET['date']);
		$startDate = new DateTime($d[0]);
		$endDate = new DateTime($d[1]);
	}
	else {
		$startDate = new DateTime('-29 days');
		$endDate = new DateTime();
	}
	$endDate->setTime(23, 59);

	$show = []; // which elements show on the graph
	$graphData = [['Date']];
	$id_string = $post ? $post->ID : implode(',', array_keys($my_posts));

	if (empty($_GET) || isset($_GET['views'])) {
		$show[] = 'views';
		$graphData[0][] = 'Views';
		$sql = "SELECT DATE(date) AS date, COUNT(*) AS cnt FROM {$wpdb->prefix}post_visits WHERE post_id IN ($id_string) AND (DATE(date) BETWEEN %s AND %s) GROUP BY DATE(date)";
		$sql = $wpdb->prepare($sql, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
		$views = $wpdb->get_results($sql, OBJECT_K);
	}

	if (empty($_GET) || isset($_GET['likes'])) {
		$show[] = 'likes';
		$graphData[0][] = 'Likes';
		$sql = "SELECT DATE(date) AS date, COUNT(*) AS cnt FROM {$wpdb->prefix}like WHERE post_id IN ($id_string) AND (DATE(date) BETWEEN %s AND %s) GROUP BY DATE(date)";
		$sql = $wpdb->prepare($sql, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
		$likes = $wpdb->get_results($sql, OBJECT_K);
	}

	if (empty($_GET) || isset($_GET['attends'])) {
		$show[] = 'attends';
		$graphData[0][] = 'Attend';
		$sql = "SELECT DATE(date) AS date, COUNT(*) AS cnt FROM {$wpdb->prefix}attend WHERE post_id IN ($id_string) AND (DATE(date) BETWEEN %s AND %s) GROUP BY DATE(date)";
		$sql = $wpdb->prepare($sql, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
		$attend = $wpdb->get_results($sql, OBJECT_K);
	}

	if (empty($_GET) || isset($_GET['reach'])) {
		$show[] = 'reach';
		$graphData[0][] = 'Reach';
		$sql = "SELECT DATE(date) AS date, COUNT(*) AS cnt FROM {$wpdb->prefix}post_seen WHERE post_id IN ($id_string) AND (DATE(date) BETWEEN %s AND %s) GROUP BY DATE(date)";
		$sql = $wpdb->prepare($sql, $startDate->format('Y-m-d'), $endDate->format('Y-m-d'));
		$reach = $wpdb->get_results($sql, OBJECT_K);
	}

	$interval = new DateInterval('P1D');
	$daterange = new DatePeriod($startDate, $interval ,$endDate);
	foreach ($daterange as $d) {
		$buf = [$d->format('M j')];
		$key = $d->format('Y-m-d');
		if (in_array('views', $show)) $buf[] = array_key_exists($key, $views) ? intval($views[$key]->cnt) : 0;
		if (in_array('likes', $show)) $buf[] = array_key_exists($key, $likes) ? intval($likes[$key]->cnt) : 0;
		if (in_array('attends', $show)) $buf[] = array_key_exists($key, $attend) ? intval($attend[$key]->cnt) : 0;
		if (in_array('reach', $show)) $buf[] = array_key_exists($key, $reach) ? intval($reach[$key]->cnt) : 0;
		$graphData[] = $buf;
	}

	ob_start();
	require plugin_dir_path(__FILE__) . 'analytics.php';
	return ob_get_clean();
}

function get_all_posts_views($user_id = 0) {
	global $wpdb;
	$sql = "SELECT SUM(meta_value)
		FROM {$wpdb->posts} AS p
		JOIN {$wpdb->postmeta} AS pm ON ID = post_id AND meta_key='views'
		WHERE post_author = " . intval($user_id);
	return (int)$wpdb->get_var($sql);
}

add_action('wp_ajax_not_hot', 'ajax_not_hot');
function ajax_not_hot() {
	if ( ! is_super_admin()) wp_die('ERR');

	update_post_meta($_POST['post_id'], '_not_hot', 1);
	delete_transient('hot_posts');
	wp_die('OK');
}

add_action('ub_settings_page', 'hot_settings');
function hot_settings() {
	if (isset($_POST['hot_days'])) delete_transient('hot_posts');
?>
<form method="post" action="options.php">
	<?php settings_fields('ub-counter') ?>
	<?php do_settings_sections('ub-counter') ?>
	<table class="form-table">
		<tr>
			<th scope="row">Hot days</th>
			<td><input type="text" name="hot_days" class="regular-text" value="<?= esc_attr(get_option('hot_days')) ?>"></td>
		</tr>
	</table>
	<?php submit_button() ?>
</form>
<?php
}
