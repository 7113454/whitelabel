<div id="analytics">
	<form method="get" action="" class="form-inline">

		<div class="text-center">
			<div class="form-group">
				<select name="post" class="form-control" onchange="this.form.submit()">
					<option value="0">Overall</option>
					<?php foreach($my_posts as $p): ?>
						<?php $selected = $post && $post->ID == $p->ID ? 'selected' : '' ?>
						<?php printf('<option value="%d" %s>%s</option>', $p->ID, $selected, $p->post_title) ?>
					<?php endforeach ?>
				</select>
			</div>

			<?php if ($post): ?>
				<div class="form-group">
					<a href="<?= get_permalink($post) ?>" class="btn btn-default" title="View"><i class="fa fa-link"></i></a>
				</div>
			<?php endif ?>
		</div>

		<div class="chart-wrapper">
			<div id="chart"></div>
		</div>

		<div class="well">
			<div class="checkbox">
				<label><input type="checkbox" name="views" <?= in_array('views', $show) ? "checked" : '' ?>> Views</label>
			</div>
			<div class="checkbox">
				<label><input type="checkbox" name="likes" <?= in_array('likes', $show) ? "checked" : '' ?>> Likes</label>
			</div>
			<div class="checkbox">
				<label><input type="checkbox" name="attends" <?= in_array('attends', $show) ? "checked" : '' ?>> Attends</label>
			</div>
			<div class="checkbox">
				<label><input type="checkbox" name="reach" <?= in_array('reach', $show) ? "checked" : '' ?>> Reach</label>
			</div>
			<!-- <input type="text" id="daterange" name="date" class="form-control"> -->
			<div id="daterange" class="">
				<i class="fa fa-calendar"></i>
				<span>&nbsp;</span>
				<i class="fa fa-caret-down"></i>
				<input type="hidden" name="date">
			</div>
			<div class="form-group">
			<select name="paid" class="form-control">
				<option value="3">All</option>
				<option value="1" <?php isset($_GET['paid']) && 1 == $_GET['paid'] && print "selected" ?>>Free</option>
				<option value="2" <?php isset($_GET['paid']) && 2 == $_GET['paid'] && print "selected" ?>>Paid</option>
			</select>
			</div>
			<button type="submit" class="btn btn-primary">Filter</button>
		</div>
	</form>

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table id="stats" class="table table-condensed"></table>
		</div>
	</div>
</div>

<script src="https://www.gstatic.com/charts/loader.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script>
	google.charts.load('current', {'packages':['corechart']});
	google.charts.setOnLoadCallback(ubDrawChart);
	var stats_data = <?= json_encode($graphData) ?>;

	jQuery(function($) {
		buildTable();

		var start = moment.utc(<?= $startDate->getTimestamp() ?> * 1000);
		var end = moment.utc(<?= $endDate->getTimestamp() ?> * 1000);

		$.getScript("//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js", function(){
			$('#daterange').daterangepicker({
				"startDate": start,
				"endDate": end,
				"locale": {
					"format": "MMMM D, YYYY"
				},
				ranges: {
					'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				}
			}, cb);
		});

		cb(start, end);
	});

	function cb(start, end) {
        jQuery('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        jQuery('#daterange input').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

	function ubDrawChart() {
		var data = google.visualization.arrayToDataTable(stats_data);

		var options = {
//			animation: {startup: true}
//			isStacked: true,
//			legend: {position: 'none'},
			vAxis: {viewWindow: {min:0}}
		};
		var chart = new google.visualization.AreaChart(document.getElementById('chart'));
		chart.draw(data, options);
	}

	function buildTable() {
		var tbody = '';
		var thead = '<tr><th>' + stats_data[0].join('</th><th>') + '</th></tr>';

		jQuery.each(stats_data, function(k, arr) {
			if (k == 0) return;
			tbody += '<tr><td>' + arr.join('</td><td>') + '</td></tr>';
		});

		jQuery('#stats').append('<thead>' + thead + '</thead>');
		jQuery('#stats').append('<tbody>' + tbody + '</tbody>');
	}
</script>
