<?php

class __Mustache_37c0fafc2e7d7a764fafc0538383dd88 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';
        $newContext = array();

        $buffer .= $indent . '<div class="wrapper">
';
        $buffer .= $indent . '    <h1>';
        $value = $this->resolveValue($context->find('pageTitle'), $context);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '</h1>
';
        $buffer .= $indent . '    <p>';
        $value = $this->resolveValue($context->find('description'), $context);
        $buffer .= htmlspecialchars($value, 2, 'UTF-8');
        $buffer .= '</p>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '    <form method="post">
';
        // 'fields' section
        $value = $context->find('fields');
        $buffer .= $this->sectionF8c1c7ac2b2a686d39d39b558308a341($context, $indent, $value);
        $buffer .= $indent . '        <p>
';
        $buffer .= $indent . '            <input type="submit" value="Save" />
';
        $buffer .= $indent . '        </p>
';
        $buffer .= $indent . '    </form>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '</div>';

        return $buffer;
    }

    private function sectionA4d03a85fb077061392d02bd05bea552(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
                    <label for="{{properties.id}}">{{properties.label}}</label> <br />
                ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '                    <label for="';
                $value = $this->resolveValue($context->findDot('properties.id'), $context);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '">';
                $value = $this->resolveValue($context->findDot('properties.label'), $context);
                $buffer .= htmlspecialchars($value, 2, 'UTF-8');
                $buffer .= '</label> <br />
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionF8c1c7ac2b2a686d39d39b558308a341(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
        if (!is_string($value) && is_callable($value)) {
            $source = '
            <p>
                {{#properties.label}}
                    <label for="{{properties.id}}">{{properties.label}}</label> <br />
                {{/properties.label}}
                {{{ html }}}
            </p>
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '            <p>
';
                // 'properties.label' section
                $value = $context->findDot('properties.label');
                $buffer .= $this->sectionA4d03a85fb077061392d02bd05bea552($context, $indent, $value);
                $buffer .= $indent . '                ';
                $value = $this->resolveValue($context->find('html'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '            </p>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
