<?php

class ER_Twitter
{
	private $token = false;
	
	function __construct()
	{
		$this->token = get_option('twitter_bearer');

		if ( ! $this->test_token()) {
			$this->request_token();
		}
	}

	private function request_token() {
		$url = 'https://api.twitter.com/oauth2/token';
		$args = array(
			'body' => array('grant_type' => 'client_credentials'),
			'headers' => array('Authorization' => 'Basic ' . base64_encode(TWITTER_KEY . ':' . TWITTER_SECRET)),
		);
		$data = wp_remote_post($url, $args);
		if (is_wp_error($data)) {
			throw new Exception('Twitter error.');
		}
		$body = json_decode($data['body']);

		if ( ! empty($body->access_token)) {
			$this->token = $body->access_token;
			update_option('twitter_bearer', $body->access_token);
		}
	}

	public function test_token() {
		return ! empty($this->token);
	}

	public function search($query) {
		if ( ! $this->test_token()) return;

		$url = 'https://api.twitter.com/1.1/search/tweets.json?result_type=popular&q=' . urlencode($query);
		$args = array(
			'headers' => array('Authorization' => 'Bearer ' . $this->token),
		);
		$data = wp_remote_get($url, $args);

		if (is_wp_error($data)) {
			throw new Exception('Twitter error.');
		}

		return json_decode($data['body']);
	}
}
