<?php
/*
Plugin Name: ER Hashtag
Plugin URI: 
Description: Add post meta "hashtag" and put following in template: <?= eh_tweets() ?>
Author: 
Version: 1.0
Author URI: 
*/

defined('TWITTER_KEY') or define('TWITTER_KEY', '0VFjNCV6MxKxv40w2OtANJzob');
defined('TWITTER_SECRET') or define('TWITTER_SECRET', '9pNoX6TrUQM7qJ7Sc0n3sobm1gwnkKnfGHTN4xQrw8J1TjLdeR');

function eh_tweets($post = null) {
	$post = get_post($post);
	$store_time = DAY_IN_SECONDS;

	if (empty($post->hashtag)) return;

	// update twitter data
	if (time() > $post->_tw_update + $store_time) {
		require plugin_dir_path(__FILE__) . 'er_twitter.php';
		$tw = new ER_Twitter();
		$search = $tw->search(eh_clean_hastag($post->hashtag));

		$ids = array();
		foreach ($search->statuses as $i => $s) {
			if ($i > 3) break; // 4 tweets max
			$ids[] = $s->id;
		}
		update_post_meta($post->ID, '_tw_ids', $ids);
		update_post_meta($post->ID, '_tw_update', time());
	}

	$out = '';
	if ($post->_tw_ids) {
		$out .= '<script src="//platform.twitter.com/widgets.js"></script>' . PHP_EOL;
		$out .= '<div class="row"><div class="col-md-6">'; // just a Bootstrap layout
		$last_in_col = floor(count($post->_tw_ids) / 2);
		foreach ($post->_tw_ids as $i => $id) {
			if ($last_in_col == $i) $out .= '</div><div class="col-md-6">'; // layout, can be safely removed
			$out .= sprintf('<div class="tweet" id="%s"></div>'.PHP_EOL, $id);
		}
		$out .= '</div></div>'; // layout
		$out .= '<script>jQuery(".tweet").each(function(e,t){var r=jQuery(this).attr("id");twttr.widgets.createTweet(r,t,{conversation:"none",cards:"none",linkColor:"",theme:"light"})});</script>'.PHP_EOL;
	}
	return $out;
}

function eh_clean_hastag($hastag) {
	$words = explode(' ', $hastag);
	return '#' . ltrim($words[0], '#');
}

add_action('save_post', 'eh_clear_twitter_cache');
function eh_clear_twitter_cache($post_id) {
	delete_post_meta($post_id, '_tw_ids');
	delete_post_meta($post_id, '_tw_update');
}
