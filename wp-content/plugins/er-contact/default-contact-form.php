<form method="post" action="<?= admin_url("admin-ajax.php") ?>" class="er-contact-form">
	<div class="form-group">
	 	<label for="er-name">Your Name <span class="required">*</span></label>
	 	<input type="text" class="form-control" id="er-name" name="name" placeholder="John Doe" required="">
	 </div>
	 <div class="form-group">
	 	<label for="er-email">Your Email <span class="required">*</span></label>
	 	<input type="email" class="form-control" id="er-email" name="email" placeholder="your@email.com" required="">
	 </div>
	 <div class="form-group">
	 	<label for="er-msg">Message</label>
	 	<textarea id="er-msg" name="message" class="form-control" rows="3" placeholder="Message"></textarea>
	 </div>
	 <input type="hidden" name="action" value="er_contact">
	 <button type="submit" class="btn btn-primary">Send</button>
</form>
