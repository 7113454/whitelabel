<?php
/*
Plugin Name: Contact Form
Description: Create default form with shortcode [er-contact-form] or build your own HTML form with class="er-contact-form". The request will be sent to admin email address.
Author: 
Version: 1.0
*/

// include inline JS if no caching plugin enabled
add_action('wp_footer', 'er_script');
function er_script() {
	if ( ! defined('WP_CACHE') || ! WP_CACHE) {
		echo '<script>';
		readfile (plugin_dir_path(__FILE__) . 'js-script.min.js');
		echo '</script>'.PHP_EOL;
	}
}

// include external JS file if caching plugin enabled
add_action('wp_enqueue_scripts', 'er_contact_script');
function er_contact_script() {
	if (defined('WP_CACHE') && WP_CACHE) {
		wp_enqueue_script('er-contact', plugin_dir_url( __FILE__ ) . 'js-script.min.js', array('jquery'), '1.0', true);
	}
}

add_action('wp_ajax_er_contact', 'er_contact');
add_action('wp_ajax_nopriv_er_contact', 'er_contact');
function er_contact() {
	$out = array('error' => false, 'msg' => '');
	if (empty($_POST)) $out = array('error' => true, 'msg' => 'Form is empty');

	if ( ! $out['error']) {
		$subj = sprintf('Contact Request [%s]', get_bloginfo('name'));
		$sent = wp_mail(get_bloginfo('admin_email'), $subj, er_email_content());

		if ($sent) {
			$out['msg'] = 'Message sent.';
		}
		else {
			$out = array('error' => true, 'msg' => 'Message does not sent.');
		}
	}
	wp_send_json($out);
}

add_shortcode('er-contact-form', 'er_contact_form');
function er_contact_form() {
	ob_start();
	include plugin_dir_path(__FILE__) . 'default-contact-form.php';
	return ob_get_clean();
}

function er_email_content() {
	$out = '';
	foreach ($_POST as $k => $v) {
		if ('action' == $k) continue;
		$out .= "$k: $v\n";
	}

	$out .= PHP_EOL;
	$out .= 'IP: ' . $_SERVER['REMOTE_ADDR'] . PHP_EOL;
	$out .= 'Browser: ' . $_SERVER['HTTP_USER_AGENT'] . PHP_EOL;
	return $out;
}