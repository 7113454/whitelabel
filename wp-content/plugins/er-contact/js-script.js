jQuery(function() {
	jQuery('form.er-contact-form').on('submit', er_submit_form);
});

function er_submit_form(e) {
	e.preventDefault();
	var form = jQuery(this);
	var data = form.serializeArray();
	data.push({name: 'action', value: 'er_contact'});
	jQuery.ajax(form.attr('action'), {
		method: 'POST',
		data: data,
		beforeSend: function() {
			form.find('.alert').remove();
			form.find('.btn.btn-primary').prop('disabled', true);
		},
		success: function(r) {
			var cls = r.error ? 'alert-danger' : 'alert-success';
			form.prepend('<div class="alert '+cls+'">'+r.msg+'</div>');
			form.find('.btn.btn-primary').prop('disabled', false);
			if (false == r.error) {
				form.trigger('reset');
			}
		},
		error: function() {
			form.find('.btn.btn-primary').prop('disabled', false);
			form.prepend('<div class="alert alert-danger">The form does not work, sorry.</div>');
		}
	});
	return false;
}
