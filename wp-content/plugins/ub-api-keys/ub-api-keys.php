<?php
/*
Plugin Name: API Keys
Plugin URI: 
Description: Manage API keys for third-party apps
Author: 
Version: 1.0
Author URI: 
*/


// create Options page
add_action('admin_menu', 'ak_menu');
function ak_menu() {
	add_options_page('API Keys', 'API Keys', 'manage_options', 'api-keys', 'api_keys_html');
}

function api_keys_html() {
	?>
	<div class="wrap">
		<form method="post" action="options.php" enctype="multipart/form-data">
			<?php 
			do_settings_sections('api-keys');
			settings_fields('api_keys');
			submit_button();
			?>
		</form>
	</div>
	<?php
}

add_action('admin_init', 'ak_settings_init');
function ak_settings_init() {
	add_settings_section("api_keys", "API Keys", false, "api-keys");

	add_settings_field("google_api_key", "Google API Key", "display_google_api_key", "api-keys", "api_keys");
	add_settings_field("fb_app", "Facebook App ID", "display_fb_app", "api-keys", "api_keys");
	add_settings_field("fb_app_secret", "Facebook App Secret", "display_fb_app_secret", "api-keys", "api_keys");

	register_setting('api_keys', 'google_api_key');
	register_setting('api_keys', 'fb_app', array('sanitize_callback' => 'sanitize_fb_app'));
	register_setting('api_keys', 'fb_app_secret', array('sanitize_callback' => 'sanitize_fb_app_secret'));
}

function display_google_api_key() {
	echo '<input type="text" name="google_api_key" value="'. get_option('google_api_key') . '" class="regular-text" />';
}
function display_fb_app() {
	echo '<input type="text" name="fb_app" value="'. get_option('fb_app') . '" class="regular-text" />';
}
function display_fb_app_secret() {
	echo '<input type="text" name="fb_app_secret" value="'. get_option('fb_app_secret') . '" class="regular-text" />';
}

// update settings for "Nextend Facebook Connect" plugin
function sanitize_fb_app($key) {
	$opt = maybe_unserialize(get_option('nextend_fb_connect'));
	if ($opt) {
		$opt['fb_appid'] = $key;
		update_option('nextend_fb_connect', maybe_serialize($opt));
	}
	return $key;
}
// update settings for "Nextend Facebook Connect" plugin
function sanitize_fb_app_secret($key) {
	$opt = maybe_unserialize(get_option('nextend_fb_connect'));
	if ($opt) {
		$opt['fb_secret'] = $key;
		update_option('nextend_fb_connect', maybe_serialize($opt));
	}
	return $key;
}

add_action('init', 'ak_define_constants');
function ak_define_constants() {
	if ( ! defined('GOOGLE_API_KEY')) define('GOOGLE_API_KEY', get_option('google_api_key'));
}

add_action('admin_notices', 'ak_empty_key_error');
function ak_empty_key_error() {
	$screen = get_current_screen();
	if ($screen->id == 'dashboard' && (empty(get_option('google_api_key')) || empty(get_option('fb_app')) || empty(get_option('fb_app_secret')))) {
		echo '<div class="error notice">';
		echo '<p><a href="'.admin_url('options-general.php?page=api-keys').'">API Keys</a> are missing! Some functionality will not work.</p>';
		echo '</div>';
	}
}
