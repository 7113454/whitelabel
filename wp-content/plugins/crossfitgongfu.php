<?php
/*
Plugin Name: Import CrossFit Gong Fu
Plugin URI: 
Description: Import events for http://www.crossfitgongfu.com/
Author: ub.fitness
Version: 1.0
Author URI: 
*/

register_activation_hook(__FILE__, 'gf_activation');
function gf_activation() {
	if ( ! wp_next_scheduled ('gf_cronjob')) {
		wp_schedule_event(time(), 'weekly', 'gf_cronjob');
	}
}

register_deactivation_hook(__FILE__, 'gf_deactivation');
function gf_deactivation() {
	wp_clear_scheduled_hook('gf_cronjob');
}

add_action('gf_cronjob', 'gf_run');
function gf_run() {
	wp_set_current_user(1);
	remove_filter('wp_insert_post', 'ub_custom_fields');

	$week = date('Y-W');
	if ($week <= get_option('_gf_last_update')) return;

	$source = 'https://widgets.healcode.com/widgets/mb/schedules/2914515ece9.json?options[start_date]=' . date('Y-m-d');
	$response = wp_remote_get($source, ['timeout' => 15]);
	if ( ! is_array($response) || 200 != $response['response']['code']) {
		trigger_error('Import: cannot fetch data.', E_USER_WARNING);
		return false;
	}

	$json = json_decode($response['body']);

	libxml_use_internal_errors(true);
	$doc = new DOMDocument();
	$doc->loadHTML($json->contents);
	// $doc->loadHTMLFile('/home/misc/www/test.html');

	$xpath = new DOMXpath($doc);
	$rows = $xpath->query('//table[contains(@class, "schedule")]/tr');

	foreach ($rows as $tr) {
		$class = $tr->getAttribute('class');

		if ('schedule_header' == $class) { // this is a date
			$date = $xpath->query('.//span[@class="hc_date"]', $tr)->item(0)->nodeValue;
		}
		elseif (strpos($class, 'hc_class')) { // this is an event
			$class = $xpath->query('.//span[contains(@class, "classname")]/a', $tr)->item(0);
			$data = [
				'title' => $class->nodeValue,
				'content' => gf_class_description($class->getAttribute('data-url')),
				'start' => strtotime($date . ' ' . $xpath->query('.//span[@class="hc_starttime"]', $tr)->item(0)->nodeValue),
				'end' => strtotime($date . ' ' . trim($xpath->query('.//span[@class="hc_endtime"]', $tr)->item(0)->nodeValue, ' -')),
			];

			gf_import($data);
		}
	}

	update_option('_gf_last_update', $week, 'no');
}

// get class description
function gf_class_description($url) {
	$response = wp_remote_get($url, ['timeout' => 15]);

	if ( ! is_array($response) || 200 != $response['response']['code']) {
		trigger_error('Import: cannot fetch data.', E_USER_WARNING);
		return false;
	}

	$doc = new DOMDocument();
	$doc->loadHTML($response['body']);

	$xpath = new DOMXpath($doc);
	$node = $xpath->query('//*[@class="class_description"]')->item(0);
	return $node->ownerDocument->saveHTML($node);
}

function gf_import($event) {
	$location = array(
		'address' => '26 St Albans Ln, London NW11 7QE, UK',
		'lat' => '51.571283',
		'lng' => '-0.194074'
	);

	$post = array(
		'post_author' => 849,
		'post_category' => [210],
		'tags_input' => ['CrossFit', 'exercise', 'gong fu'],

		'post_title' => $event['title'],
		'post_content' => $event['content'],
		'post_status' => 'publish',
		'meta_input' => array(
			'date' => $event['start'],
			'date_end' => $event['end'],
			'location' => $location,
			'info_url' => 'http://www.crossfitgongfu.com',
			'_thumbnail_id' => 17537
		),
	);

	$id = wp_insert_post($post);

	return $id;
}