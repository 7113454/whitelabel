<?php
/*
Plugin Name: Facebook Bot
Plugin URI: 
Description: Bot for the Messenger
Author: ub.fitness
Version: 1.0
Author URI: 
*/

add_action('admin_menu', 'bot_wp_menu');
function bot_wp_menu() {
	add_options_page('Facebook Bot', 'Facebook Bot', 'manage_options', 'bot-settings.php', 'bot_settings_page');
}

add_action('admin_init', 'bot_register_settings');
function bot_register_settings() {
	register_setting('bot-settings', 'page_access_token');
	register_setting('bot-settings', 'bot_per_page');
}

function bot_settings_page() {
	if (isset($_POST['action']) && 'register_menu' == $_POST['action']) bot_register_menu();
	require plugin_dir_path(__FILE__) . 'settings-page.php';
}

add_action('wp_ajax_nopriv_fb_bot', 'fb_webhook');
function fb_webhook() {
	global $fb_request, $sender;
	bot_challenge();

	$fb_request = json_decode(file_get_contents('php://input'), true);
	$payload = isset($fb_request['entry'][0]['messaging'][0]['postback']) ? $fb_request['entry'][0]['messaging'][0]['postback']['payload'] : '';
	$message = isset($fb_request['entry'][0]['messaging'][0]['message']) ? $fb_request['entry'][0]['messaging'][0]['message']['text'] : '';
	$sender = $fb_request['entry'][0]['messaging'][0]['sender']['id'];

	if ('HELP' == $payload || 'GET_STARTED' == $payload) {
		payload_help();
	}
	elseif (preg_match('/^HOT_EVENTS_(\d+)$/', $payload, $matches)) {
		payload_hot_events($matches[1]);
	}
	elseif (preg_match('/^SEARCH_([A-Za-z0-9+\/=]+)_(\d+)$/', $payload, $matches)) {
		bot_search(base64_decode($matches[1]), $matches[2]);
	}
	elseif (preg_match('/^search (.+)$/i', $message, $matches)) {
		bot_search($matches[1]);
	}
	elseif (preg_match('/^hello|help/i', $message)) {
		payload_help();
	}
	elseif (preg_match('/^WHATSON_([A-Za-z0-9+\/=]+)_(\d+)$/', $payload, $matches)) {
		bot_whatson(base64_decode($matches[1]), $matches[2]);
	}
	elseif (preg_match("/^what(?:s|'s| is) on(?: in)? (.+)$/i", $message, $matches)) {
		// the regex above handles queries like this:
		// Whats on
		// Whats on in
		// What's on
		// What is on
		bot_whatson($matches[1]);
	}

	wp_die();
}

function bot_challenge() {
	$verify_token = "fb_bot";

	if(isset($_REQUEST['hub_verify_token']) && $_REQUEST['hub_verify_token'] == $verify_token) {
		echo $_REQUEST['hub_challenge'];
		wp_die();
	}
}

function payload_help() {
	global $sender;

	$msg = 'Hi, thanks for chatting with UB Fitness.

Find out what\'s going on by searching for activity, location or date.

To find ACTIVITIES simply type "search gymnastics" to see what gymnastics events are listed.
(Tip: replace "gymnastics" with the kind of activity you are interested in. i.e. tennis, kickboxing, golf etc.)';
	bot_say(['recipient' => ['id' => $sender], 'message' => ['text' => $msg]]);

	$msg = 'To find activities by LOCATION type "What\'s on in London?" to see what is happening in London.
(Tip: replace "London" with the city you\'re in i.e. New York, Melbourne etc.)

You can also find out WHEN activities are happening. Try typing "What\'s on in October?”
(Tip: replace "October" with the month of your choice.)

Thanks for taking the time to get in touch, we appreciate your support. Did you know that we\'ve just launched the UB Fitness App http://ub.fitness/download/';
	bot_say(['recipient' => ['id' => $sender], 'message' => ['text' => $msg]]);

	// $out = [
	// 	'recipient' => ['id' => $sender],
	// 	'message' => ['text' => $msg],
	// ];
	// bot_say($out);
}

function payload_hot_events($page) {
	global $wpdb;

	$limit = get_option('bot_per_page', 9);
	$ids = get_top_posts_id($page, $limit);
	$total = $wpdb->get_var('SELECT FOUND_ROWS()');

	$next_page = '';

	if ($page < ceil($total/$limit)) {
		$next_page = 'HOT_EVENTS_' . ($page+1);
	}

	$args = array(
		'ignore_sticky_posts' => true,
		'post__in' => $ids,
		'orderby' => 'post__in',
		'posts_per_page' => -1,
	);
	$q = new WP_Query($args);

	bot_say(posts2json($q, $next_page));
}

/*
 *
 * @param $q WP_Query object
 * @param $next page string payload
 */
function posts2json(WP_Query $q, $next_page = '') {
	global $sender;

	$out = [
		'recipient' => ['id' => $sender],
		'message' => [
			'attachment' => [
				'type' => 'template',
				'payload' => [
					'template_type' => 'generic',
					'elements' => [],
				],
			],
		],
	];

	while ($q->have_posts()) {
		$q->the_post();

		date_default_timezone_set(get_event_timezone());

		$out['message']['attachment']['payload']['elements'][] = [
			'title' => html_entity_decode($q->post->post_title),
			'image_url' => get_the_post_thumbnail_url(false, 'medium'),
			'subtitle' => 'Start: ' . date('j M Y H:i (T)', $q->post->date),
			'item_url' => get_permalink(),
		];
	}

	if ($next_page) {
		$out['message']['attachment']['payload']['elements'][] = [
			'title' => 'Would you like to see more?',
			'image_url' => get_template_directory_uri() . '/images/pagination-9.png',
			'buttons' => [
				['title' => 'View More', 'type' => 'postback', 'payload' => $next_page]
			],
		];
	}

	return $out;
}

function bot_say($data) {
	$url = 'https://graph.facebook.com/v2.6/me/messages?access_token=' . get_option('page_access_token');

	$response = wp_remote_post($url, array(
		'headers' => array('Content-Type' => 'application/json'),
		'body' => json_encode($data),
	));

	// log errrors
	if (is_wp_error($response)) {
		trigger_error('BOT: ' . $response->get_error_message(), E_USER_WARNING);
	}
	elseif (isset($response['code']) && 200 != $response['code']) {
		trigger_error('BOT: ' . $response['body'], E_USER_WARNING);
	}

	// $f = fopen(ABSPATH . '/bot.log', 'a');
	// fwrite($f, "\n-------------------\n");
	// fwrite($f, json_encode($data));
	// fwrite($f, "\ndebug:" . json_encode($debug));
	// fclose($f);
}

function bot_register_menu() {
	// register "Get Started" button
	$url = "https://graph.facebook.com/v2.6/me/thread_settings?access_token=" . get_option('page_access_token');
	$data = array(
		'setting_type' => 'call_to_actions',
		'thread_state' => 'new_thread',
		'call_to_actions' => array(
			array('payload' => 'GET_STARTED')
		),
	);
	wp_remote_post($url, array(
		'headers' => array('Content-Type' => 'application/json'),
		'body' => json_encode($data),
	));

//////////////////////
	$data = array(
		'setting_type' => 'call_to_actions',
		'thread_state' => 'existing_thread',
		'call_to_actions' => array(
			array('title' => 'Hot Activities', 'type' => 'postback', 'payload' => 'HOT_EVENTS_1'),
			array('title' => 'Help', 'type' => 'postback', 'payload' => 'HELP'),
		),
	);
	$debug = wp_remote_post($url, array(
		'headers' => array('Content-Type' => 'application/json'),
		'body' => json_encode($data),
	));
//	var_dump(json_encode($data), $debug);
}

function bot_search($needle, $page = 1) {
	global $sender;

	$args = array(
		'ignore_sticky_posts' => true,
		's' => $needle,
		'posts_per_page' => get_option('bot_per_page', 9),
		'paged' => $page,
		'meta_query' => array(
			array('key' => 'date_end', 'compare' => '>', 'value' => current_time('timestamp'), 'type' => 'NUMERIC')
		)
	);

	if (preg_match('/(.+) in (.+)/i', $needle, $matches)) {
		$addr = ub_geocode($matches[2]);
		$name = isset($addr['city']) ? $addr['city'] : (isset($addr['country']) ? $addr['country'] : false);
		if ($name) {
			$args['s'] = $matches[1];
			$args['tax_query'] = [['taxonomy' => 'city', 'field' => 'name', 'terms' => $name]];
		}
		else {
			$args['post__in'] = array(0);
		}
	}

	$q = new WP_Query($args);

	if ($q->post_count) {
		$next_page = ($page < $q->max_num_pages) ? 'SEARCH_' . base64_encode($needle) . '_' . ($page+1) : '';
		bot_say(posts2json($q, $next_page));
	}
	else {
		$out = [
			'recipient' => ['id' => $sender],
			'message' => ['text' => "Sorry, but I couldn't find anything for $needle, please try another search."],
		];
		bot_say($out);
	}
}

function bot_whatson($phrase, $page = 1) {
	global $sender;
	$parts = explode(' in ', $phrase); // phrase could be "London in January"

	$args = array(
		'ignore_sticky_posts' => true,
		'posts_per_page' => get_option('bot_per_page', 9),
		'paged' => $page,
		'order' => 'ASC',
		'orderby'   => 'meta_value_num',
		'meta_key'  => 'date',
		'meta_query' => array(
			array('key' => 'date_end', 'compare' => '>', 'value' => current_time('timestamp'), 'type' => 'NUMERIC')
		)
	);

	foreach ($parts as $p) {
		$p = trim($p);

		try { // is it a date?
			$start = new DateTime('1 ' . $p);
		} catch (Exception $e) {
			$start = false;
		}

		if ($start) { // search by month
			$this_month = new DateTime('first day of this month 00:00');
			if ($start < $this_month) {
				$start->modify('+1 year');
			}
			$end = clone $start;
			$end->modify('+1 month');

			$args['meta_query'] = array(
				array('key' => 'date', 'compare' => '>', 'value' => $start->getTimestamp(), 'type' => 'NUMERIC'),
				array('key' => 'date', 'compare' => '<', 'value' => $end->getTimestamp(), 'type' => 'NUMERIC')
			);
		}
		else { // search by location
			$addr = ub_geocode($p);

			if (isset($addr['country'])) {
				// get the country taxonomy first
				$targs = ['taxonomy' => 'city', 'name' => $addr['country'], 'hide_empty' => 0];
				$tmp = get_terms($targs);
				$country = array_shift($tmp);

				if ($country && isset($addr['city'])) {
					$targs = ['taxonomy' => 'city', 'name' => $addr['city'], 'hide_empty' => 0, 'child_of' => $country->term_id];
					$tmp = get_terms($targs);
					$city = array_shift($tmp);
				}

				if (isset($city) && $city) {
					$args['tax_query'] = [['taxonomy' => 'city', 'field' => 'term_id', 'terms' => $city->term_id]];
				}
				elseif (empty($addr['city']) && $country) {
					$args['tax_query'] = [['taxonomy' => 'city', 'field' => 'term_id', 'terms' => $country->term_id]];
				}
				else {
					$args['post__in'] = array(0);
				}

			}
			else {
				$args['post__in'] = array(0);
			}
		}
	}

	$q = new WP_Query($args);

	if ($q->post_count) {
		$next_page = ($page < $q->max_num_pages) ? 'WHATSON_' . base64_encode($phrase) . '_' . ($page+1) : '';
		bot_say(posts2json($q, $next_page));
	}
	else {
		$out = [
			'recipient' => ['id' => $sender],
			'message' => ['text' => "Sorry, but I couldn't find anything, please try another search."],
		];
		bot_say($out);
	}
}