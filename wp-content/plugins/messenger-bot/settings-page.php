<div class="wrap">
	<h1>Facebook Bot</h1>

	<form method="post" action="options.php">
		<?php settings_fields('bot-settings') ?>
		<?php do_settings_sections('bot-settings') ?>
		<table class="form-table">
			<tr>
				<th scope="row">Page Access Token</th>
				<td><input type="text" name="page_access_token" value="<?= esc_attr(get_option('page_access_token')) ?>" class="regular-text"></td>
			</tr>
			<tr>
				<th scope="row">Number of listings</th>
				<td><input type="number" name="bot_per_page" min="2" max="9" value="<?= esc_attr(get_option('bot_per_page', 9)) ?>"></td>
			</tr>
		</table>
		<?php submit_button() ?>
	</form>

	<form method="post" class="hidden">
		<input type="hidden" name="action" value="register_menu">
		<button type="submit" class="button">Register Menu</button>
	</form>
</div>