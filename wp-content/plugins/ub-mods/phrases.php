<?php
class UB_Phrase {
	private static $locale = 'en_US';
	private static $phrases = array();

	public static function get_phrase($key, $locale = '') {
		$locale = 'en_US';
		if ( ! array_key_exists($locale, self::$phrases)) {
			self::loadPhrases($locale);
		}

		return array_key_exists($key, self::$phrases[$locale]) ? self::$phrases[$locale][$key] : '';
	}

	/**
	 * Get several phrases
	 * @param mixed $key array of keys or string separated by comma
	 *
	 * @return array associative arrays of phrases
	 */
	public static function get_phrases($keys = array(), $locale = '') {
		$locale = 'en_US';
		$out = array();
		if ( ! is_array($keys) && strlen($keys)) {
			$keys = explode(',', $keys);
		}

		// load phrases
		if ( ! array_key_exists($locale, self::$phrases)) {
			self::loadPhrases($locale);
		}

		if ( ! empty($keys)) {
			foreach ($keys as $k) {
				$out[$k] = array_key_exists($k, self::$phrases[$locale]) ? self::$phrases[$locale][$k] : '';
			}
		}
		else {
			$out = self::$phrases[$locale];
		}
		return $out;
	}

	private static function loadPhrases($locale) {
		global $wpdb;
		self::$phrases[$locale] = array();

		$sql = "SELECT `key`, `value` FROM {$wpdb->prefix}phrases WHERE locale = %s";
		$result = $wpdb->get_results($wpdb->prepare($sql, $locale));

		foreach ($result as $r) {
			self::$phrases[$locale][$r->key] = $r->value;
		}
	}
}

// START REST API
add_action('rest_api_init', 'api_phrases_init');
function api_phrases_init() {
	register_rest_route('wp/v2', '/phrases', array('methods' => 'GET', 'callback' => 'api_phrases'));
}

function api_phrases($request) {
	return UB_Phrase::get_phrases($request['key']);
}
// END REST API

add_shortcode('phrase', 'shortcode_phrase');
function shortcode_phrase($atts) {
	if (empty($atts['key'])) return;
	return UB_Phrase::get_phrase($atts['key']);
}
