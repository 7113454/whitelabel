<style>
	button.button { vertical-align: middle !important; }
</style>

<div class="wrap">
	<h2><button type="button" class="button" data-for="avatar">Change...</button> Default Avatar</h2>
	<?= wp_get_attachment_image(get_option('default_avatar_id'), 'thumbnail') ?>

	<h2><button type="button" class="button" data-for="background">Change...</button> Default Background</h2>
	<?= wp_get_attachment_image(get_option('default_bg_id'), 'thumbnail') ?>

	<h2><button type="button" class="button" data-for="thumbnail">Change...</button> Event Image</h2>
	<?= wp_get_attachment_image(get_option('default_thumb_id'), 'thumbnail') ?>
</div>

<form id="imageForm" style="display:none" method="post">
	<input type="hidden" name="media_id">
	<input type="hidden" name="media_for">
</form>

<script>
	var frame;
	jQuery(function($) {

		$('.button').on('click', function() {
			var btn = $(this);
			if ( ! frame ) {
				// Create a new media frame
				frame = wp.media({
					title: 'Select Image',
					// button: {
					// 	text: 'Use this media'
					// },
					multiple: false  // Set to true to allow multiple files to be selected

				});

				frame.on('select', function() {
					var attachment = frame.state().get('selection').first().toJSON();
					var form = $('#imageForm');
					form.find('input[name=media_id]').val(attachment.id);
					form.find('input[name=media_for]').val(btn.data('for'));
					form.trigger('submit');
				});
			}

			frame.open();
		});
	});
</script>
