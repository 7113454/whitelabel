<?php

/*
CREATE TABLE IF NOT EXISTS `wp_post_seen` (
`post_id` bigint(20) unsigned NOT NULL,
`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
`ip` varchar(15) NOT NULL,
`agent` varchar(255) NOT NULL,
`client` enum('web','ios','android') NOT NULL DEFAULT 'web',
`page` varchar(50) NULL,
KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci
*/

add_action('rest_api_init', 'rest_post_seen');
function rest_post_seen() {
	register_rest_route('ub', '/post-seen', array('methods' => 'POST', 'callback' => 'api_post_seen'));
}

add_action('ub_cron_weekly', 'clean_post_seen');
function clean_post_seen() {
	global $wpdb;
	$sql = "DELETE FROM {$wpdb->prefix}post_seen WHERE date < DATE_ADD(NOW(), INTERVAL -" . UB_COUNT_FOR . ")";
	$wpdb->query($sql);
}

function api_post_seen($request) {
	global $wpdb;
	$json = json_decode(file_get_contents("php://input"));

	$client = ub_client();
	$date = current_time('mysql');
	$sql = "INSERT INTO {$wpdb->prefix}post_seen (post_id, `date`, ip, agent, client, page) VALUES ";
	$sql2 = "";
	$cnt = 0;

	foreach ($json as $i) {
		if (empty($i->post_id)) continue;

		$cnt++;
		$page = isset($i->page) ? esc_html($i->page) : null;
		$sql2 .= $wpdb->prepare("(%d, %s, %s, %s, %s, %s),", $i->post_id, $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $client, $page);
	}

	if ($sql2) {
		$wpdb->query($sql . substr($sql2, 0, -1));
	}

	return $cnt;
}

function post_seen($post_id) {
	global $wpdb;

	$date = current_time('mysql');
	$client = ub_client();
	$page = isset($_POST['page']) ? esc_html($_POST['page']) : null;

	$sql = "INSERT INTO {$wpdb->prefix}post_seen (post_id, `date`, ip, agent, client, page) VALUES ";
	$sql2 = "";

	if (is_array($post_id)) {
		foreach ($post_id as $id) {
			$sql2 .= $wpdb->prepare("(%d, %s, %s, %s, %s, %s),", $id, $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $client, $page);
		}
		$sql2 = substr($sql2, 0, -1);
	}
	else {
		$sql2 .= $wpdb->prepare("(%d, %s, %s, %s, %s, %s)", $post_id, $date, $_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $client, $page);
	}

	$wpdb->query($sql . $sql2);
}