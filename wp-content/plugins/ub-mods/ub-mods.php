<?php
/*
Plugin Name: UB Mods
Plugin URI: 
Description: Modifications those cannot be in theme code
Author: ub.fitness
Version: 1.0
Author URI: 
*/

include_once plugin_dir_path(__FILE__) . 'post-seen.php';
include_once plugin_dir_path(__FILE__) . 'phrases.php';

register_activation_hook(__FILE__, 'ub_mods_activation');
function ub_mods_activation() {
	if ( ! wp_next_scheduled ('ub_cron_twicedaily')) {
		wp_schedule_event(time(), 'twicedaily', 'ub_cron_twicedaily');
	}
	if ( ! wp_next_scheduled ('ub_cron_hourly')) {
		wp_schedule_event(time(), 'hourly', 'ub_cron_hourly');
	}
	if ( ! wp_next_scheduled ('ub_cron_weekly')) {
		wp_schedule_event(time(), 'weekly', 'ub_cron_weekly');
	}
}

register_deactivation_hook(__FILE__, 'ub_mods_deactivation');
function ub_mods_deactivation() {
	wp_clear_scheduled_hook('ub_cron_twicedaily');
	wp_clear_scheduled_hook('ub_cron_hourly');
	wp_clear_scheduled_hook('ub_cron_weekly');
}

add_action('admin_menu', 'ub_images_menu');
function ub_images_menu() {
	add_options_page('Default Images', 'Default Images', 'manage_options', 'ub-images.php', 'ub_images_page');
	add_options_page('UBF Settings', 'UBF Settings', 'manage_options', 'ub_settings', 'ub_settings');
	add_options_page('Email Templates', 'Email Templates', 'manage_options', 'email-template.php', 'templates_page');
}


function ub_images_page() {
	if (isset($_POST['media_id'])) save_default_image();

	wp_enqueue_media();
	require plugin_dir_path(__FILE__) . 'ub-images.php';
}

function ub_settings() {
	echo '<div class="wrap"><h1>UBF Settings</h1>';
	do_action('ub_settings_page');
	echo '</div>';
}

function templates_page() {
	echo '<div class="wrap"><h1>Email Templates</h1>';
	do_action('ub_templates_page');
	echo '</div>';
}

function save_default_image() {
	switch ($_POST['media_for']) {
		case 'avatar':
			update_option('default_avatar_id', $_POST['media_id']);
			break;
		case 'background':
			update_option('default_bg_id', $_POST['media_id']);
			break;
		case 'thumbnail':
			update_option('default_thumb_id', $_POST['media_id']);
			break;
	}
}

add_action('admin_init', 'register_share_settings');
function register_share_settings() {
	register_setting('email-template', 'share_subj');
	register_setting('email-template', 'share_body');

	register_setting('new-post-template', 'new_post_subj');
	register_setting('new-post-template', 'new_post_body');

	register_setting('status-submission-rules', 'min_posts');
	register_setting('ub-counter', 'hot_days');
}

add_action('ub_templates_page', 'share_template');
function share_template() {
?>
<form method="post" action="options.php">
	<?php settings_fields('email-template') ?>
	<?php do_settings_sections('email-template') ?>
	<h2 class="title">Share with followers</h2>
	<table class="form-table">
		<tr>
			<th scope="row">Subject</th>
			<td><input type="text" name="share_subj" class="regular-text" value="<?= esc_attr(get_option('share_subj')) ?>"></td>
		</tr>
		<tr>
			<th scope="row">Body</th>
			<td><textarea name="share_body" class="large-text" cols="50" rows="6"><?= esc_attr(get_option('share_body')) ?></textarea></td>
		</tr>
	</table>
	<?php submit_button() ?>
</form>
<?php
}

add_action('ub_templates_page', 'new_activity');
function new_activity() {
?>
<form method="post" action="options.php">
	<?php settings_fields('new-post-template') ?>
	<?php do_settings_sections('new-post-template') ?>
	<h2 class="title">New activity</h2>
	<table class="form-table">
		<tr>
			<th scope="row">Subject</th>
			<td><input type="text" name="new_post_subj" class="regular-text" value="<?= esc_attr(get_option('new_post_subj')) ?>"></td>
		</tr>
		<tr>
			<th scope="row">Body</th>
			<td><textarea name="new_post_body" class="large-text" cols="50" rows="6"><?= esc_attr(get_option('new_post_body')) ?></textarea></td>
		</tr>
	</table>
	<?php submit_button() ?>
</form>
<?php
}

// add_action('ub_push', 'new_post_email_notify', 10, 2);
function new_post_email_notify($post, $subscribers) {
	global $wpdb;

	$author = get_user_by('id', $post->post_author);
	$ids = implode(',', $subscribers);
	$sql = "SELECT * FROM {$wpdb->users} WHERE ID IN ($ids)";
	$users = $wpdb->get_results($sql);

	$search = ['%rcpt-name%', '%unsubscribe-url%', '%post-title%', '%post-content%', '%post-url%', '%from-name%'];
	$replace = ['', '', $post->post_title, $post->post_content, get_permalink($post), $author->display_name];

	$subj = str_replace($search, $replace, get_option('new_post_subj'));
	$msg = get_option('new_post_body');

	foreach ($users as $u) {
		$replace[0] = $u->display_name;
		$replace[1] = site_url().'?wpsa_unsubscribe='.md5($u->user_email).'&author='.$author->ID;
		wp_mail($u->user_email, $subj, str_replace($search, $replace, $msg));
	}
}


add_action('ub_settings_page', 'status_submit_settings');
function status_submit_settings() {
?>
<form method="post" action="options.php">
	<?php settings_fields('status-submission-rules') ?>
	<?php do_settings_sections('status-submission-rules') ?>
	<h2 class="title">Allow status submission if</h2>
	<table class="form-table">
		<tr>
			<th scope="row">Min activities</th>
			<td><input type="text" name="min_posts" class="regular-text" value="<?= esc_attr(get_option('min_posts')) ?>"></td>
		</tr>
	</table>
	<?php submit_button() ?>
</form>
<?php
}
